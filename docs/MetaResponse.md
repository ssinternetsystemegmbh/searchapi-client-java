

# MetaResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request** | [**MetaRequestResponse**](MetaRequestResponse.md) |  |  [optional]
**build** | [**MetaBuildResponse**](MetaBuildResponse.md) |  |  [optional]



