

# MetaRequestResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**duration** | **Integer** |  |  [optional]
**hostIp** | **String** |  |  [optional]
**requestId** | **String** |  |  [optional]
**timestamp** | **String** |  |  [optional]



