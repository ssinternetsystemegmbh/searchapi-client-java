

# DealerAuthRoleDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** | The role ID. |  [optional]
**name** | **String** | The role name. |  [optional]



