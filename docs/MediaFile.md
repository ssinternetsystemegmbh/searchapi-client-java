

# MediaFile

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filename** | **String** |  |  [optional]
**fileType** | **String** |  |  [optional]
**position** | **Integer** |  |  [optional]
**isBranded** | **Boolean** |  |  [optional]
**isCensored** | **Boolean** |  |  [optional]



