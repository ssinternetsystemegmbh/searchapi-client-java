

# DealerAuthUserDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** | The user ID. |  [optional]
**username** | **String** | The login username of the user. |  [optional]
**email** | **String** | The unique email address of the user. |  [optional]
**dealerId** | **Long** | The dealer ID to which the user belongs. |  [optional]
**roles** | **List&lt;String&gt;** | The roles of the user. |  [optional]
**lastaccess** | **String** | The last access timestamp of the user. |  [optional]
**created** | **String** | The dataset created timestamp of the user. |  [optional]
**modified** | **String** | The dataset modified timestamp of the user. |  [optional]
**enabled** | **Boolean** | The access of the user. |  [optional]



