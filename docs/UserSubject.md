

# UserSubject

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**login** | **String** |  |  [optional]
**id** | **String** |  |  [optional]
**roles** | **List&lt;String&gt;** |  |  [optional]
**properties** | **Map&lt;String, String&gt;** |  |  [optional]
**authenticationMethod** | [**AuthenticationMethodEnum**](#AuthenticationMethodEnum) |  |  [optional]



## Enum: AuthenticationMethodEnum

Name | Value
---- | -----
PASSWORD | &quot;PASSWORD&quot;
TWO_FACTOR | &quot;TWO_FACTOR&quot;



