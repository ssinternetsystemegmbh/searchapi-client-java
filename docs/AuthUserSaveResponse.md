

# AuthUserSaveResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** | The user ID. |  [optional]
**dealerId** | **Long** | The dealer ID to which the user belongs. |  [optional]
**messages** | **List&lt;String&gt;** | A save message. |  [optional]
**changes** | [**List&lt;Change&gt;**](Change.md) | The following changes were made for data consistency. |  [optional]
**errors** | [**List&lt;Error&gt;**](Error.md) | The following errors occurred during processing. |  [optional]



