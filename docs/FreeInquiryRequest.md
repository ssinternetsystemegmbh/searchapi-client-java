

# FreeInquiryRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customerData** | [**CustomerInquiryData**](CustomerInquiryData.md) |  |  [optional]
**freeInquiryVehicleData** | [**FreeInquiryVehicleData**](FreeInquiryVehicleData.md) |  |  [optional]



