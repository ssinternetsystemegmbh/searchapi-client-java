

# DealerAuthRoleListResponseWithMeta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**MetaResponse**](MetaResponse.md) |  |  [optional]
**response** | [**DealerAuthRoleListResponse**](DealerAuthRoleListResponse.md) |  |  [optional]



