

# GeodataCityResponseWithMeta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**MetaResponse**](MetaResponse.md) |  |  [optional]
**response** | [**GeodataCityResponse**](GeodataCityResponse.md) |  |  [optional]



