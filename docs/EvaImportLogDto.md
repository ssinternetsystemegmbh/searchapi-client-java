

# EvaImportLogDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**tbtId** | **Integer** |  |  [optional]
**errortype** | **Integer** |  |  [optional]
**errortext** | **String** |  |  [optional]
**comment** | **String** |  |  [optional]



