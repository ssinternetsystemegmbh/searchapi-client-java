

# DealerOwnFieldsInternResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | **Boolean** |  |  [optional]
**message** | **String** |  |  [optional]
**dealerOwnFieldsInternResultItem** | [**DealerOwnFieldsInternResultItem**](DealerOwnFieldsInternResultItem.md) |  |  [optional]



