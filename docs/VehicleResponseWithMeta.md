

# VehicleResponseWithMeta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**MetaResponse**](MetaResponse.md) |  |  [optional]
**response** | [**VehicleResponse**](VehicleResponse.md) |  |  [optional]



