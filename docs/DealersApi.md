# DealersApi

All URIs are relative to *https://develop-api.ssis.de:8443/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getDealerDetails**](DealersApi.md#getDealerDetails) | **POST** /dealers/details | Get dealer details
[**searchDealer**](DealersApi.md#searchDealer) | **POST** /dealers/search | search dealers


<a name="getDealerDetails"></a>
# **getDealerDetails**
> DealerResponseWithMeta getDealerDetails(dealerDetailsRequest)

Get dealer details

Get list of dealer details

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.DealersApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    DealersApi apiInstance = new DealersApi(defaultClient);
    DealerDetailsRequest dealerDetailsRequest = new DealerDetailsRequest(); // DealerDetailsRequest | dealerIds
    try {
      DealerResponseWithMeta result = apiInstance.getDealerDetails(dealerDetailsRequest);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DealersApi#getDealerDetails");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dealerDetailsRequest** | [**DealerDetailsRequest**](DealerDetailsRequest.md)| dealerIds |

### Return type

[**DealerResponseWithMeta**](DealerResponseWithMeta.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json;charset=utf-8
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | An array of dealerDetails |  -  |
**400** | Bad Request |  -  |
**401** | Unauthorized |  -  |
**404** | Not Found |  -  |
**0** | an array of dealerDetails |  -  |

<a name="searchDealer"></a>
# **searchDealer**
> DealerSearchResultWithMeta searchDealer(dealerSearch)

search dealers

search list of dealers

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.DealersApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    DealersApi apiInstance = new DealersApi(defaultClient);
    DealerSearch dealerSearch = new DealerSearch(); // DealerSearch | dealerIds
    try {
      DealerSearchResultWithMeta result = apiInstance.searchDealer(dealerSearch);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DealersApi#searchDealer");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dealerSearch** | [**DealerSearch**](DealerSearch.md)| dealerIds |

### Return type

[**DealerSearchResultWithMeta**](DealerSearchResultWithMeta.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json;charset=utf-8
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | An array of dealerIds |  -  |
**400** | Bad Request |  -  |
**401** | Unauthorized |  -  |
**404** | Not Found |  -  |
**0** | an array of dealerIds |  -  |

