

# DataProvider

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  |  [optional]
**vehicleId** | **String** |  |  [optional]
**vehicleLink** | **String** |  |  [optional]



