

# Dealer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealerId** | **Integer** |  |  [optional]
**headquarterId** | **Integer** |  |  [optional]
**branchIds** | **List&lt;Integer&gt;** |  |  [optional]
**products** | [**List&lt;Product&gt;**](Product.md) |  |  [optional]
**cooperations** | [**List&lt;Cooperation&gt;**](Cooperation.md) |  |  [optional]
**businesscardImages** | [**List&lt;BusinesscardImage&gt;**](BusinesscardImage.md) |  |  [optional]
**customContent** | [**List&lt;CustomContent&gt;**](CustomContent.md) |  |  [optional]
**company** | **String** |  |  [optional]
**companyAdditional** | **String** |  |  [optional]
**street** | **String** |  |  [optional]
**zip** | **String** |  |  [optional]
**city** | **String** |  |  [optional]
**countryIso** | **String** |  |  [optional]
**name** | **List&lt;String&gt;** |  |  [optional]
**telephone** | **List&lt;String&gt;** |  |  [optional]
**mobile** | **String** |  |  [optional]
**fax** | **String** |  |  [optional]
**email** | **List&lt;String&gt;** |  |  [optional]
**emailTechnics** | **String** |  |  [optional]
**emailBilling** | **String** |  |  [optional]
**homepage** | **String** |  |  [optional]
**businessOrganisation** | **String** |  |  [optional]
**managingDirector** | **String** |  |  [optional]
**euIdentNumber** | **String** |  |  [optional]
**ustId** | **String** |  |  [optional]
**commercialRegister** | **String** |  |  [optional]
**commercialRegisterNumber** | **String** |  |  [optional]
**commercialRegisterCourt** | **String** |  |  [optional]
**arbitrationOffice** | **String** |  |  [optional]
**insuranceBrokerNumber** | **String** | The dealer&#39;s insurance broker number. |  [optional]
**geolocation** | [**Geolocation**](Geolocation.md) |  |  [optional]
**dateCreated** | **String** |  |  [optional]
**dateModified** | **String** |  |  [optional]
**bic** | **String** |  |  [optional]
**iban** | **String** |  |  [optional]
**bank** | **String** |  |  [optional]
**hasOwnVehicles** | **Boolean** |  |  [optional]
**numOwnVehicles** | **Long** |  |  [optional]
**vat** | **Float** | The vat rate of the dealer&#39;s country. |  [optional]
**dealerSettings** | **Map&lt;String, String&gt;** |  |  [optional]
**dealerSettingsFromDefault** | **List&lt;String&gt;** |  |  [optional]



