

# BusinesscardImage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**orderNr** | **Integer** |  |  [optional]
**imageURL** | **String** |  |  [optional]



