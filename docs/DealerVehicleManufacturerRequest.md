

# DealerVehicleManufacturerRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealerIds** | **List&lt;Integer&gt;** | A list of dealer IDs. |  [optional]



