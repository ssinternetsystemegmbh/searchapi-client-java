

# InquirySearchListResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **Integer** | Total hits. |  [optional]
**inquirySearchResponseList** | [**List&lt;InquirySearchResponse&gt;**](InquirySearchResponse.md) | List of inquiries. |  [optional]



