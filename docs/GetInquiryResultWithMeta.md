

# GetInquiryResultWithMeta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**MetaResponse**](MetaResponse.md) |  |  [optional]
**response** | [**GetInquiryResult**](GetInquiryResult.md) |  |  [optional]



