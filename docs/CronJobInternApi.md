# CronJobInternApi

All URIs are relative to *https://develop-api.ssis.de:8443/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**execCronJobIntern**](CronJobInternApi.md#execCronJobIntern) | **POST** /admin/cronjobintern/exec/{cronjobname} | Execute a cron job by given cronJobName (for internal only)
[**listCronJobIntern**](CronJobInternApi.md#listCronJobIntern) | **GET** /admin/cronjobintern/list | List all searchAPI internally crontab jobs (for internal)


<a name="execCronJobIntern"></a>
# **execCronJobIntern**
> DefaultResponse execCronJobIntern(cronjobname)

Execute a cron job by given cronJobName (for internal only)

Execute a cron job by given case sensitive cronjobname.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.CronJobInternApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    CronJobInternApi apiInstance = new CronJobInternApi(defaultClient);
    String cronjobname = "cronjobname_example"; // String | 
    try {
      DefaultResponse result = apiInstance.execCronJobIntern(cronjobname);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling CronJobInternApi#execCronJobIntern");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cronjobname** | **String**|  |

### Return type

[**DefaultResponse**](DefaultResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | Execute a cron job by given cronJobName (for internal only) |  -  |

<a name="listCronJobIntern"></a>
# **listCronJobIntern**
> CronJobInternListResultWithMeta listCronJobIntern()

List all searchAPI internally crontab jobs (for internal)

List all searchAPI internally crontab jobs (for internal).

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.CronJobInternApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    CronJobInternApi apiInstance = new CronJobInternApi(defaultClient);
    try {
      CronJobInternListResultWithMeta result = apiInstance.listCronJobIntern();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling CronJobInternApi#listCronJobIntern");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**CronJobInternListResultWithMeta**](CronJobInternListResultWithMeta.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | List all searchAPI internally crontab jobs (for internal). |  -  |

