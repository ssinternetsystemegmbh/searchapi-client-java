

# DealerSearchHit

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealerId** | **Integer** |  |  [optional]
**distance** | **String** |  |  [optional]
**dateModified** | **String** |  |  [optional]



