

# DealerResponseWithMeta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**MetaResponse**](MetaResponse.md) |  |  [optional]
**response** | [**DealerResponse**](DealerResponse.md) |  |  [optional]



