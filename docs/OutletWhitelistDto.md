

# OutletWhitelistDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | **String** | Email-Address |  [optional]
**dealerId** | **Long** | Dealer-ID |  [optional]
**company** | **String** | Company |  [optional]
**homepage** | **String** | Homepage |  [optional]
**active** | **Boolean** | active |  [optional]
**dateModified** | [**DateTime**](DateTime.md) | Date of modification. |  [optional]
**dateCreated** | [**DateTime**](DateTime.md) | Date of creation. |  [optional]



