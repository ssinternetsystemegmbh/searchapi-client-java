

# Consumption

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**consumptionCity** | [**BigDecimal**](BigDecimal.md) | HINWEIS: das Feld ist veraltet. |  [optional]
**consumptionOutskirts** | [**BigDecimal**](BigDecimal.md) | HINWEIS: das Feld ist veraltet. |  [optional]
**consumptionTotal** | [**BigDecimal**](BigDecimal.md) | HINWEIS: das Feld ist veraltet. |  [optional]
**consumptionElectricity** | [**BigDecimal**](BigDecimal.md) | HINWEIS: das Feld ist veraltet. |  [optional]
**co2Emission** | [**BigDecimal**](BigDecimal.md) | HINWEIS: das Feld ist veraltet. |  [optional]
**co2Efficiency** | **String** | HINWEIS: das Feld ist veraltet. |  [optional]
**energyCarrierCost** | [**BigDecimal**](BigDecimal.md) | HINWEIS: das Feld ist veraltet. |  [optional]
**fuelTypes** | [**List&lt;FuelType&gt;**](FuelType.md) |  |  [optional]
**wltpSlow** | [**BigDecimal**](BigDecimal.md) | HINWEIS: das Feld ist veraltet. Benutzen Sie bitte wltpFuelConsumptionCity. [WLTP Verbrauch langsam. in l/100km] |  [optional]
**wltpMiddle** | [**BigDecimal**](BigDecimal.md) | HINWEIS: das Feld ist veraltet. Benutzen Sie bitte wltpFuelConsumptionSuburban. [WLTP Verbrauch mittel. in l/100km] |  [optional]
**wltpFast** | [**BigDecimal**](BigDecimal.md) | HINWEIS: das Feld ist veraltet. Benutzen Sie bitte wltpFuelConsumptionRural. [WLTP Verbrauch schnell. in l/100km] |  [optional]
**wltpVeryFast** | [**BigDecimal**](BigDecimal.md) | HINWEIS: das Feld ist veraltet. Benutzen Sie bitte wltpFuelConsumptionHighway. [WLTP Verbrauch sehr schnell. in l/100km] |  [optional]
**wltpCombined** | [**BigDecimal**](BigDecimal.md) | HINWEIS: das Feld ist veraltet. Benutzen Sie bitte wltpFuelConsumptionCombined. [WLTP Verbrauch kombiniert. in l/100km (bei Erdgas in kg/100km, bei Elektro in kWh/100km)] |  [optional]
**wltpEmission** | **Integer** | HINWEIS: das Feld ist veraltet. Benutzen Sie bitte wltpCombinedEmission. [WLTP Emission. Ganzzahl in g/km (wird auf 0 gesetzt bei Elektro).] |  [optional]
**wltpEfficiency** | **String** | HINWEIS: das Feld ist veraltet. Benutzen Sie bitte wltpCombinedCO2Class. [WLTP CO2 efficiency class (from A to G)] |  [optional]
**wltpCombinedEmission** | **Integer** | WLTP CO2 Emission kombiniert. Ganzzahl in g/km (wird auf 0 gesetzt bei Elektro). Im Fall von Plugin Hybrid ist es die CO2 Emission bei entladener Batterie |  [optional]
**wltpFuelConsumptionCombined** | [**BigDecimal**](BigDecimal.md) | WLTP Kraftstoffverbrauch kombiniert. in l/100km (bei Erdgas in kg/100km). Im Fall von Plugin Hybrid den Kraftstoffverbrauch kombiniert bei entladener Batterie |  [optional]
**wltpFuelConsumptionCity** | [**BigDecimal**](BigDecimal.md) | WLTP Kraftstoffverbrauch Stadt. in l/100km (bei Erdgas in kg/100km). Im Fall von Plugin Hybrid den Kraftstoffverbrauch Stadt bei entladener Batterie |  [optional]
**wltpFuelConsumptionSuburban** | [**BigDecimal**](BigDecimal.md) | WLTP Kraftstoffverbrauch Stadtrand. in l/100km (bei Erdgas in kg/100km). Im Fall von Plugin Hybrid den Kraftstoffverbrauch Stadtrand bei entladener Batterie |  [optional]
**wltpFuelConsumptionRural** | [**BigDecimal**](BigDecimal.md) | WLTP Kraftstoffverbrauch Landstrasse. in l/100km (bei Erdgas in kg/100km). Im Fall von Plugin Hybrid den Kraftstoffverbrauch Landstrasse bei entladener Batterie |  [optional]
**wltpFuelConsumptionHighway** | [**BigDecimal**](BigDecimal.md) | WLTP Kraftstoffverbrauch Autobahn. in l/100km (bei Erdgas in kg/100km). Im Fall von Plugin Hybrid den Kraftstoffverbrauch Autobahn bei entladener Batterie |  [optional]
**wltpCombinedCO2Class** | **String** | WLTP CO2 Klasse kombiniert.  (A, B, C, D, E, F oder G) im Fall von Plugin Hybrid ist es die CO2-Klasse bei entladener Batterie |  [optional]
**wltpFueltype** | [**WltpFueltypeEnum**](#WltpFueltypeEnum) | WLTP Motorantriebsart: mögliche Werte &#39;Verbrenner&#39;,&#39;Elektro&#39;,&#39;Brennstoff&#39; oder &#39;Plug_in_Hybrid&#39; |  [optional]
**wltpFueltypeLabel** | **String** | WLTP Motorantriebsart: Bezeichnung der Motorantriebsart. |  [optional]
**wltpPowerConsumptionCombined** | [**BigDecimal**](BigDecimal.md) | WLTP Stromverbrauch kombiniert: in kWh/100km im Fall von Plugin Hybrid den Stromverbrauch kombiniert bei rein elektrischem Betrieb |  [optional]
**wltpPowerConsumptionCity** | [**BigDecimal**](BigDecimal.md) | WLTP Stromverbrauch Innenstadt: in kWh/100km im Fall von Plugin Hybrid den Stromverbrauch Innenstadt bei rein elektrischem Betrieb |  [optional]
**wltpPowerConsumptionSuburban** | [**BigDecimal**](BigDecimal.md) | WLTP Stromverbrauch Stadtrand: in kWh/100km im Fall von Plugin Hybrid den Stromverbrauch Stadtrand bei rein elektrischem Betrieb |  [optional]
**wltpPowerConsumptionRural** | [**BigDecimal**](BigDecimal.md) | WLTP Stromverbrauch Landstrasse: in kWh/100km im Fall von Plugin Hybrid den Stromverbrauch Landstrasse bei rein elektrischem Betrieb |  [optional]
**wltpPowerConsumptionHighway** | [**BigDecimal**](BigDecimal.md) | WLTP Stromverbrauch Autobahn: in kWh/100km im Fall von Plugin Hybrid den Stromverbrauch Autobahn bei rein elektrischem Betrieb |  [optional]
**wltpAllElectricRange** | **Integer** | WLTP Elektronische Reichweite: in Kilometer im Fall von Plugin Hybrid die elektronische Reichweite EAER |  [optional]
**wltpWeightedCombinedEmission** | **Integer** | WLTP CO2-Emission gewichtet kombiniert: Ganzzahl in g/km |  [optional]
**wltpWeightedCombinedPower** | [**BigDecimal**](BigDecimal.md) | WLTP Energieverbrauch Strom gewichtet kombiniert: in kWh/100km |  [optional]
**wltpWeightedCombinedFuel** | [**BigDecimal**](BigDecimal.md) | WLTP Energieverbrauch Kraftstoff gewichtet kombiniert: in l/100km (bei Erdgas in kg/100km) Als Dezimaltrennzeichen ist der Punkt zu verwenden |  [optional]
**wltpWeightedCombinedCO2Class** | **String** | WLTP CO2-Klasse gewichtet kombiniert: (A, B, C, D, E, F oder G) |  [optional]
**wltpConsumptionCosts** | [**BigDecimal**](BigDecimal.md) | WLTP Energiekosten: in EUR, bei 15.000 km Jahresfahrleistung |  [optional]
**fuelpriceAvgYear** | [**BigDecimal**](BigDecimal.md) | Kraftstoffpreis: EUR/l oder kg im Jahresdurchschnitt |  [optional]
**powerpriceAvgYear** | [**BigDecimal**](BigDecimal.md) | Strompreis: cent/kWh im Jahresdurchschnitt |  [optional]
**consumptionPriceYear** | **Integer** | Jahresangabe der Felder Kraftstoff- und Strompreis: im Format YYYY |  [optional]
**co2CostMiddleBaseprice** | [**BigDecimal**](BigDecimal.md) | CO2 Kosten Mittel pro Tonne: EUR/t, über die nächsten 10 Jahre (15.000 km/Jahr) |  [optional]
**co2CostMiddleAccumulatedPrice** | [**BigDecimal**](BigDecimal.md) | CO2 Kosten Mittel hochgerechnet: EUR, über die nächsten 10 Jahre (15.000 km/Jahr) |  [optional]
**co2CostLowBaseprice** | [**BigDecimal**](BigDecimal.md) | CO2 Kosten Niedrig pro Tonne: EUR/t, über die nächsten 10 Jahre (15.000 km/Jahr) |  [optional]
**co2CostLowAccumulatedPrice** | [**BigDecimal**](BigDecimal.md) | CO2 Kosten Niedrig hochgerechnet: EUR, über die nächsten 10 Jahre (15.000 km/Jahr) |  [optional]
**co2CostHighBaseprice** | [**BigDecimal**](BigDecimal.md) | CO2 Kosten Hoch pro Tonne: EUR/t, über die nächsten 10 Jahre (15.000 km/Jahr) |  [optional]
**co2CostHighAccumulatedPrice** | [**BigDecimal**](BigDecimal.md) | CO2 Kosten Hoch hochgerechnet: EUR, über die nächsten 10 Jahre (15.000 km/Jahr) |  [optional]
**co2CostTimeFrameFrom** | **Integer** | CO2 Preise Zeitraum von: im Format YYYY |  [optional]
**co2CostTimeFrameTill** | **Integer** | CO2 Preise Zeitraum bis: im Format YYYY |  [optional]



## Enum: WltpFueltypeEnum

Name | Value
---- | -----
VERBRENNER | &quot;Verbrenner&quot;
ELEKTRO | &quot;Elektro&quot;
BRENNSTOFF | &quot;Brennstoff&quot;
PLUG_IN_HYBRID | &quot;Plug_in_Hybrid&quot;



