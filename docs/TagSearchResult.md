

# TagSearchResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**aggregation** | [**Map&lt;String, Map&lt;String, TagObject&gt;&gt;**](Map.md) |  |  [optional]



