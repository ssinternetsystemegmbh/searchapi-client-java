

# VehicleSelectionListSaveRequestDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** | Unique identifier for the vehicle selection list. Null for new lists. |  [optional]
**dealerId** | **Long** | The unique identifier of the dealer associated with this selection list. | 
**name** | **String** | The name of the vehicle selection list. | 
**description** | **String** | A brief description of the vehicle selection list. |  [optional]
**searchJson** | **String** | Search parameters in JSON format. |  [optional]
**vehicles** | [**List&lt;VehicleSelectionListVehicleSaveDto&gt;**](VehicleSelectionListVehicleSaveDto.md) | The list of vehicles included in the selection. |  [optional]



