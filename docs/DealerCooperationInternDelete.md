

# DealerCooperationInternDelete

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;DealerCooperationInternDeleteItem&gt;**](DealerCooperationInternDeleteItem.md) | A list of settings_keys |  [optional]



