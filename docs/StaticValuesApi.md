# StaticValuesApi

All URIs are relative to *https://develop-api.ssis.de:8443/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getMakes**](StaticValuesApi.md#getMakes) | **GET** /value/makes | Get makes.
[**getModelsByMake**](StaticValuesApi.md#getModelsByMake) | **GET** /value/models/{make} | Get models of the passed make.


<a name="getMakes"></a>
# **getMakes**
> MakeResponseWithMeta getMakes()

Get makes.

Get a list of vehicle makes.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.StaticValuesApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    StaticValuesApi apiInstance = new StaticValuesApi(defaultClient);
    try {
      MakeResponseWithMeta result = apiInstance.getMakes();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling StaticValuesApi#getMakes");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**MakeResponseWithMeta**](MakeResponseWithMeta.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | Returns a list of vehicle makes. |  -  |

<a name="getModelsByMake"></a>
# **getModelsByMake**
> ModelResponseWithMeta getModelsByMake(make)

Get models of the passed make.

Get models of the passed make.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.StaticValuesApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    StaticValuesApi apiInstance = new StaticValuesApi(defaultClient);
    String make = "make_example"; // String | 
    try {
      ModelResponseWithMeta result = apiInstance.getModelsByMake(make);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling StaticValuesApi#getModelsByMake");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **make** | **String**|  |

### Return type

[**ModelResponseWithMeta**](ModelResponseWithMeta.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | Returns a list of vehicle models. |  -  |

