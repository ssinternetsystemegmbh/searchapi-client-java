# VehiclesApi

All URIs are relative to *https://develop-api.ssis.de:8443/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getVehicleAggregationTags**](VehiclesApi.md#getVehicleAggregationTags) | **POST** /dealers/{dealerid}/vehicles/aggregationtags | 
[**getVehicleDetails**](VehiclesApi.md#getVehicleDetails) | **POST** /dealers/{dealerid}/vehicles/details | Get Vehicledetails
[**searchDealerVehicles**](VehiclesApi.md#searchDealerVehicles) | **POST** /dealers/{dealerid}/vehicles/search | search Vehicles


<a name="getVehicleAggregationTags"></a>
# **getVehicleAggregationTags**
> TagSearchResultWithMeta getVehicleAggregationTags(dealerid, tagSearch)



### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.VehiclesApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    VehiclesApi apiInstance = new VehiclesApi(defaultClient);
    Integer dealerid = 56; // Integer | 
    TagSearch tagSearch = new TagSearch(); // TagSearch | tags
    try {
      TagSearchResultWithMeta result = apiInstance.getVehicleAggregationTags(dealerid, tagSearch);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling VehiclesApi#getVehicleAggregationTags");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dealerid** | **Integer**|  |
 **tagSearch** | [**TagSearch**](TagSearch.md)| tags |

### Return type

[**TagSearchResultWithMeta**](TagSearchResultWithMeta.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json;charset=utf-8
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | an array of translated tags |  -  |

<a name="getVehicleDetails"></a>
# **getVehicleDetails**
> VehicleResponseWithMeta getVehicleDetails(dealerid, vehicleDetailsRequest)

Get Vehicledetails

Get list of vehicledetails

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.VehiclesApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    VehiclesApi apiInstance = new VehiclesApi(defaultClient);
    Integer dealerid = 56; // Integer | 
    VehicleDetailsRequest vehicleDetailsRequest = new VehicleDetailsRequest(); // VehicleDetailsRequest | vehicleIds
    try {
      VehicleResponseWithMeta result = apiInstance.getVehicleDetails(dealerid, vehicleDetailsRequest);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling VehiclesApi#getVehicleDetails");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dealerid** | **Integer**|  |
 **vehicleDetailsRequest** | [**VehicleDetailsRequest**](VehicleDetailsRequest.md)| vehicleIds |

### Return type

[**VehicleResponseWithMeta**](VehicleResponseWithMeta.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json;charset=utf-8
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | an array of vehicledetails |  -  |

<a name="searchDealerVehicles"></a>
# **searchDealerVehicles**
> VehicleSearchResultWithMeta searchDealerVehicles(dealerid, vehicleSearch)

search Vehicles

search list of vehicles

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.VehiclesApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    VehiclesApi apiInstance = new VehiclesApi(defaultClient);
    Integer dealerid = 56; // Integer | 
    VehicleSearch vehicleSearch = new VehicleSearch(); // VehicleSearch | search list of vehicles
    try {
      VehicleSearchResultWithMeta result = apiInstance.searchDealerVehicles(dealerid, vehicleSearch);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling VehiclesApi#searchDealerVehicles");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dealerid** | **Integer**|  |
 **vehicleSearch** | [**VehicleSearch**](VehicleSearch.md)| search list of vehicles |

### Return type

[**VehicleSearchResultWithMeta**](VehicleSearchResultWithMeta.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json;charset=utf-8
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | an array of vehicleIds |  -  |

