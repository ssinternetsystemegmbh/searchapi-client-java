# VehicleSelectionApi

All URIs are relative to *https://develop-api.ssis.de:8443/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addVehiclesToVehicleSelectionList**](VehicleSelectionApi.md#addVehiclesToVehicleSelectionList) | **POST** /vehicleselection/addvehiclestolist | Adds a list of vehicles to the vehicle selection list.
[**deleteVehicleSelectionList**](VehicleSelectionApi.md#deleteVehicleSelectionList) | **DELETE** /vehicleselection/delete/{dealerid} | Deletes a vehicle selection list for a specified dealer by ID.
[**getVehicleSelectionLists**](VehicleSelectionApi.md#getVehicleSelectionLists) | **GET** /vehicleselection/all/{dealerid} | Retrieves a list of vehicle selection lists associated with a specific dealer.
[**removeVehiclesFromVehicleSelectionList**](VehicleSelectionApi.md#removeVehiclesFromVehicleSelectionList) | **POST** /vehicleselection/removevehiclesfromlist | Removes vehicles from a specified vehicle selection list.
[**saveVehicleSelectionList**](VehicleSelectionApi.md#saveVehicleSelectionList) | **POST** /vehicleselection/savelist | Saves a vehicle selection list.


<a name="addVehiclesToVehicleSelectionList"></a>
# **addVehiclesToVehicleSelectionList**
> CommonResponse addVehiclesToVehicleSelectionList(vehicleSelectionListVehicleAddDto)

Adds a list of vehicles to the vehicle selection list.

Adds a list of vehicles to the vehicle selection list.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.VehicleSelectionApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    VehicleSelectionApi apiInstance = new VehicleSelectionApi(defaultClient);
    VehicleSelectionListVehicleAddDto vehicleSelectionListVehicleAddDto = new VehicleSelectionListVehicleAddDto(); // VehicleSelectionListVehicleAddDto | List of vehicle susuuids.
    try {
      CommonResponse result = apiInstance.addVehiclesToVehicleSelectionList(vehicleSelectionListVehicleAddDto);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling VehicleSelectionApi#addVehiclesToVehicleSelectionList");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vehicleSelectionListVehicleAddDto** | [**VehicleSelectionListVehicleAddDto**](VehicleSelectionListVehicleAddDto.md)| List of vehicle susuuids. |

### Return type

[**CommonResponse**](CommonResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json;charset=utf-8
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | Response &#x60;true&#x60; when vehicles successfully added. |  -  |

<a name="deleteVehicleSelectionList"></a>
# **deleteVehicleSelectionList**
> CommonResponse deleteVehicleSelectionList(dealerid, listid)

Deletes a vehicle selection list for a specified dealer by ID.

Deletes a vehicle selection list for a specified dealer by ID.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.VehicleSelectionApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    VehicleSelectionApi apiInstance = new VehicleSelectionApi(defaultClient);
    Integer dealerid = 56; // Integer | 
    Integer listid = 56; // Integer | 
    try {
      CommonResponse result = apiInstance.deleteVehicleSelectionList(dealerid, listid);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling VehicleSelectionApi#deleteVehicleSelectionList");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dealerid** | **Integer**|  |
 **listid** | **Integer**|  |

### Return type

[**CommonResponse**](CommonResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | Response is true when deleting was successful. |  -  |

<a name="getVehicleSelectionLists"></a>
# **getVehicleSelectionLists**
> VehicleSelectionListDtoListResponse getVehicleSelectionLists(dealerid)

Retrieves a list of vehicle selection lists associated with a specific dealer.

Retrieves a list of vehicle selection lists associated with a specific dealer.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.VehicleSelectionApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    VehicleSelectionApi apiInstance = new VehicleSelectionApi(defaultClient);
    Integer dealerid = 56; // Integer | 
    try {
      VehicleSelectionListDtoListResponse result = apiInstance.getVehicleSelectionLists(dealerid);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling VehicleSelectionApi#getVehicleSelectionLists");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dealerid** | **Integer**|  |

### Return type

[**VehicleSelectionListDtoListResponse**](VehicleSelectionListDtoListResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | List data. |  -  |

<a name="removeVehiclesFromVehicleSelectionList"></a>
# **removeVehiclesFromVehicleSelectionList**
> CommonResponse removeVehiclesFromVehicleSelectionList(vehicleSelectionListVehicleRemoveDto)

Removes vehicles from a specified vehicle selection list.

Removes vehicles from a specified vehicle selection list.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.VehicleSelectionApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    VehicleSelectionApi apiInstance = new VehicleSelectionApi(defaultClient);
    VehicleSelectionListVehicleRemoveDto vehicleSelectionListVehicleRemoveDto = new VehicleSelectionListVehicleRemoveDto(); // VehicleSelectionListVehicleRemoveDto | List of vehicle susuuids.
    try {
      CommonResponse result = apiInstance.removeVehiclesFromVehicleSelectionList(vehicleSelectionListVehicleRemoveDto);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling VehicleSelectionApi#removeVehiclesFromVehicleSelectionList");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vehicleSelectionListVehicleRemoveDto** | [**VehicleSelectionListVehicleRemoveDto**](VehicleSelectionListVehicleRemoveDto.md)| List of vehicle susuuids. |

### Return type

[**CommonResponse**](CommonResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json;charset=utf-8
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | Response &#x60;true&#x60; when vehicles successfully removed. |  -  |

<a name="saveVehicleSelectionList"></a>
# **saveVehicleSelectionList**
> CommonResponse saveVehicleSelectionList(vehicleSelectionListSaveRequestDto)

Saves a vehicle selection list.

Saves a vehicle selection list.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.VehicleSelectionApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    VehicleSelectionApi apiInstance = new VehicleSelectionApi(defaultClient);
    VehicleSelectionListSaveRequestDto vehicleSelectionListSaveRequestDto = new VehicleSelectionListSaveRequestDto(); // VehicleSelectionListSaveRequestDto | Data of selection list.
    try {
      CommonResponse result = apiInstance.saveVehicleSelectionList(vehicleSelectionListSaveRequestDto);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling VehicleSelectionApi#saveVehicleSelectionList");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vehicleSelectionListSaveRequestDto** | [**VehicleSelectionListSaveRequestDto**](VehicleSelectionListSaveRequestDto.md)| Data of selection list. |

### Return type

[**CommonResponse**](CommonResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json;charset=utf-8
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | Response ist true when saving was successful. |  -  |

