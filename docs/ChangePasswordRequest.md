

# ChangePasswordRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **String** | The username. | 
**oldpassword** | **String** | The new password. | 
**newpassword** | **String** | The new password. |  [optional]



