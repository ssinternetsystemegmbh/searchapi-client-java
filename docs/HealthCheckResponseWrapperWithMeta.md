

# HealthCheckResponseWrapperWithMeta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**MetaResponse**](MetaResponse.md) |  |  [optional]
**response** | [**HealthCheckResponseWrapper**](HealthCheckResponseWrapper.md) |  |  [optional]



