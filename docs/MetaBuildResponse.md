

# MetaBuildResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**version** | **String** |  |  [optional]
**timestamp** | **String** |  |  [optional]



