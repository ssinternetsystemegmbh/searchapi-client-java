

# SendCarkparkEmail

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sendername** | **String** | Name of the sender. |  [optional]
**senderemail** | **String** | Email of the sender. |  [optional]
**recipientname** | **String** | Name of the recipient. |  [optional]
**recipientemail** | **String** | Email of the recipient. |  [optional]
**subject** | **String** | Email subject. |  [optional]
**body** | **String** | Email body. |  [optional]
**susuuids** | **List&lt;String&gt;** | List of vehicle susuuids from the carpark. |  [optional]



