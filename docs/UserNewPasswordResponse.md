

# UserNewPasswordResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**emailAddress** | **String** | Email Address the reset token was sent. | 
**dateCreated** | **String** | Timestamp of the entry. | 
**createdBy** | **String** | Principal of the entry (eg. SusAdmin, PWForgotSite) | 
**newToken** | **Boolean** | Further information. |  [optional]
**message** | **String** | Further information. |  [optional]
**tokenLifetime** | **Integer** | Further information. |  [optional]



