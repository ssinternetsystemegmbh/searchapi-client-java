

# PwkQueueIndexingStatisticsDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numEntries** | **Integer** | The total number of entries in the queue. |  [optional]
**toOldestEntrySeconds** | **Long** | The duration in seconds to the oldest entry in the queue. |  [optional]
**oldestQueueIndexing** | [**PwkQueueIndexingDto**](PwkQueueIndexingDto.md) |  |  [optional]



