

# VehicleSearchHitIntern

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vehicleId** | **String** |  |  [optional]
**dateModified** | **String** |  |  [optional]
**vehicleType** | **String** |  |  [optional]
**vehicleOwnerId** | **Integer** |  |  [optional]
**offerNumber** | **String** |  |  [optional]
**consumerPrice** | **Double** |  |  [optional]



