

# DealerDataInternRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tblHaendlerdaten** | [**DealerDataInternFieldsOfRequest**](DealerDataInternFieldsOfRequest.md) |  |  [optional]



