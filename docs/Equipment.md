

# Equipment

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**text** | **String** |  |  [optional]
**categoryTag** | **String** |  |  [optional]
**categoryText** | **String** |  |  [optional]
**prices** | [**Map&lt;String, Price&gt;**](Price.md) |  |  [optional]
**optional** | **Boolean** |  |  [optional]
**notForResellers** | **Boolean** |  |  [optional]
**optionalForInStock** | **Boolean** |  |  [optional]



