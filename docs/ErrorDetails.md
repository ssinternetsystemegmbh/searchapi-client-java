

# ErrorDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | [**DateTime**](DateTime.md) | The timestamp of the thrown error. | 
**message** | **String** | The error message. | 
**details** | **String** | Some error details. | 



