

# OutletSearch

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**outletAttachment** | **String** |  |  [optional]
**outletMailBody** | **String** |  |  [optional]
**outletSenderEmail** | **String** |  |  [optional]
**outletSenderName** | **String** |  |  [optional]
**vehicleType** | **String** |  |  [optional]
**outletSubject** | **String** |  |  [optional]
**vehicleId** | **String** |  |  [optional]
**outletQuery** | **List&lt;String&gt;** |  |  [optional]
**dateIndexed** | **List&lt;String&gt;** |  |  [optional]
**dateModified** | **List&lt;String&gt;** |  |  [optional]



