

# DealerAuthUserListResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**users** | [**List&lt;DealerAuthUserDto&gt;**](DealerAuthUserDto.md) |  |  [optional]



