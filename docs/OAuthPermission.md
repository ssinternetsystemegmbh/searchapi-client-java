

# OAuthPermission

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**permission** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**invisibleToClient** | **Boolean** |  |  [optional]
**httpVerbs** | **List&lt;String&gt;** |  |  [optional]
**uris** | **List&lt;String&gt;** |  |  [optional]
**defaultPermission** | **Boolean** |  |  [optional]
**_default** | **Boolean** |  |  [optional]



