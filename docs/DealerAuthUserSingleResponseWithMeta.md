

# DealerAuthUserSingleResponseWithMeta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**MetaResponse**](MetaResponse.md) |  |  [optional]
**response** | [**DealerAuthUserSingleResponse**](DealerAuthUserSingleResponse.md) |  |  [optional]



