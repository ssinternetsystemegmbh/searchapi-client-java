

# CronJobInternListResultWithMeta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**MetaResponse**](MetaResponse.md) |  |  [optional]
**response** | [**CronJobInternListResult**](CronJobInternListResult.md) |  |  [optional]



