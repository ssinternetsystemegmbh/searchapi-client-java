

# ProductSearch

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tag** | **String** |  |  [optional]
**isBlocked** | **Boolean** |  |  [optional]
**isTestphase** | **Boolean** |  |  [optional]
**productOptions** | [**List&lt;ProductOptionSearch&gt;**](ProductOptionSearch.md) |  |  [optional]



