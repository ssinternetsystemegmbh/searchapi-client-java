

# ModelSearch

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**manufacturerTag** | **String** |  |  [optional]
**seriesTag** | **String** |  |  [optional]
**modelQuery** | **List&lt;String&gt;** |  |  [optional]
**exclude** | **Boolean** |  |  [optional]



