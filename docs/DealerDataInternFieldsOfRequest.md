

# DealerDataInternFieldsOfRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**company** | **String** | Firma wird nur übernommen, wenn eine Händerneuanlage(INSERT) erfolgt. Zurzeit ist nur das UPDATE erlaubt und daher wird dieses Feld ignoriert. |  [optional]
**companyAdditional** | **String** |  |  [optional]
**street** | **String** |  |  [optional]
**lhdId** | **Integer** | ID des Herkunftsland |  [optional]
**countryIso** | **String** | Country ISO2 origin |  [optional]
**zip** | **String** |  |  [optional]
**city** | **String** |  |  [optional]
**longitude** | **String** | laengengrad |  [optional]
**latitude** | **String** | breitengrad |  [optional]
**contactPerson1** | **String** | ansprechpartner1 |  [optional]
**contactPerson2** | **String** | ansprechpartner2 |  [optional]
**telefone1** | **String** |  |  [optional]
**telefone2** | **String** |  |  [optional]
**fax** | **String** |  |  [optional]
**mobil** | **String** |  |  [optional]
**email1** | **String** |  |  [optional]
**email2** | **String** |  |  [optional]
**emailTechnic** | **String** | Technik E-Mail |  [optional]
**emailBilling** | **String** | Rechnungs E-Mail |  [optional]
**homepage** | **String** |  |  [optional]
**ustId** | **String** |  |  [optional]
**commercialRegister** | **String** | HRB |  [optional]
**businessOrganisation** | **String** | Gesellschaftsform |  [optional]
**managingDirector** | **String** | Geschaeftsfuehrer |  [optional]
**commercialRegisterCourt** | **String** | Registergericht |  [optional]
**commercialRegisterNumber** | **String** | Registernummer |  [optional]
**euIdentNumber** | **String** | euidentnummer |  [optional]
**insuranceBrokerNumber** | **String** | Versicherungsvermittler Register (vvregister) |  [optional]
**arbitrationOffice** | **String** | schiedsstelle |  [optional]
**bank** | **String** |  |  [optional]
**iban** | **String** |  |  [optional]
**bic** | **String** |  |  [optional]



