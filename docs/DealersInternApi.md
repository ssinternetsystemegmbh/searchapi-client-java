# DealersInternApi

All URIs are relative to *https://develop-api.ssis.de:8443/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addDealerCarparkIntern**](DealersInternApi.md#addDealerCarparkIntern) | **POST** /admin/dealersintern/{dealerId}/carpark | Add one vehicle susuuid in carpark for internal.
[**addDealerCooperationIntern**](DealersInternApi.md#addDealerCooperationIntern) | **POST** /admin/dealersintern/{dealerId}/cooperation | Add one dealer_cooperation entry in dealer_cooperation for internal.
[**addDealerInformationIntern**](DealersInternApi.md#addDealerInformationIntern) | **POST** /admin/dealersintern/{dealerId}/customContent | Add one customContent entry in dealer_information for internal.
[**addDealerOwnFieldsIntern**](DealersInternApi.md#addDealerOwnFieldsIntern) | **POST** /admin/dealersintern/{dealerId}/ownFields | Add one ownFields entry in tblhaendlerEigeneFelder for internal.
[**addDealerSettingsIntern**](DealersInternApi.md#addDealerSettingsIntern) | **POST** /admin/dealersintern/{dealerId}/settings | Add one dealer_settings entry in dealer_settings for internal.
[**deleteDealerCarparkIntern**](DealersInternApi.md#deleteDealerCarparkIntern) | **DELETE** /admin/dealersintern/{dealerId}/carpark | Delete one vehicle susuuid in carpark for internal.
[**deleteDealerCooperationIntern**](DealersInternApi.md#deleteDealerCooperationIntern) | **DELETE** /admin/dealersintern/{dealerId}/cooperation | Delete one or more dealer_cooperation in dealer_cooperation for internal.
[**deleteDealerInformationIntern**](DealersInternApi.md#deleteDealerInformationIntern) | **DELETE** /admin/dealersintern/{dealerId}/customContent | Delete one customContent in dealer_information for internal.
[**deleteDealerSettingsIntern**](DealersInternApi.md#deleteDealerSettingsIntern) | **DELETE** /admin/dealersintern/{dealerId}/settings | Delete one or more dealer_settings in dealer_settings for internal.
[**detailsDealerBillsIntern**](DealersInternApi.md#detailsDealerBillsIntern) | **POST** /admin/dealersintern/{dealerId}/bills/details | Get bills details for internal.
[**detailsDealerCarparkIntern**](DealersInternApi.md#detailsDealerCarparkIntern) | **GET** /admin/dealersintern/{dealerId}/carpark | Get all vehicle from dealers carpark for internal.
[**getDealerVehicleManufacturer**](DealersInternApi.md#getDealerVehicleManufacturer) | **POST** /admin/dealersintern/{dealerId}/manufacturer | Get a list of manufacturer of vehicles send by dealer.
[**putDealerData**](DealersInternApi.md#putDealerData) | **PUT** /admin/dealersintern/{dealerId}/data | Insert or update dealer data
[**searchDealerBillsIntern**](DealersInternApi.md#searchDealerBillsIntern) | **POST** /admin/dealersintern/{dealerId}/bills/search | Search bills for internal.


<a name="addDealerCarparkIntern"></a>
# **addDealerCarparkIntern**
> DefaultResponse addDealerCarparkIntern(dealerId, dealerCarparkInternChange)

Add one vehicle susuuid in carpark for internal.

Add one vehicle susuuid in carpark for internal.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.DealersInternApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    DealersInternApi apiInstance = new DealersInternApi(defaultClient);
    Integer dealerId = 56; // Integer | 
    DealerCarparkInternChange dealerCarparkInternChange = new DealerCarparkInternChange(); // DealerCarparkInternChange | Dealer carpark insert.
    try {
      DefaultResponse result = apiInstance.addDealerCarparkIntern(dealerId, dealerCarparkInternChange);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DealersInternApi#addDealerCarparkIntern");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dealerId** | **Integer**|  |
 **dealerCarparkInternChange** | [**DealerCarparkInternChange**](DealerCarparkInternChange.md)| Dealer carpark insert. | [optional]

### Return type

[**DefaultResponse**](DefaultResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json;charset=utf-8
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | Add one vehicle susuuid in carpark for internal. |  -  |

<a name="addDealerCooperationIntern"></a>
# **addDealerCooperationIntern**
> DefaultResponse addDealerCooperationIntern(dealerId, dealerCooperationInternAdd)

Add one dealer_cooperation entry in dealer_cooperation for internal.

Add one dealer_cooperation entry in dealer_cooperation for internal.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.DealersInternApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    DealersInternApi apiInstance = new DealersInternApi(defaultClient);
    Integer dealerId = 56; // Integer | 
    DealerCooperationInternAdd dealerCooperationInternAdd = new DealerCooperationInternAdd(); // DealerCooperationInternAdd | Dealer cooperation insert.
    try {
      DefaultResponse result = apiInstance.addDealerCooperationIntern(dealerId, dealerCooperationInternAdd);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DealersInternApi#addDealerCooperationIntern");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dealerId** | **Integer**|  |
 **dealerCooperationInternAdd** | [**DealerCooperationInternAdd**](DealerCooperationInternAdd.md)| Dealer cooperation insert. | [optional]

### Return type

[**DefaultResponse**](DefaultResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json;charset=utf-8
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | Add one dealer_cooperation entry in dealer_cooperation for internal. |  -  |

<a name="addDealerInformationIntern"></a>
# **addDealerInformationIntern**
> DefaultResponse addDealerInformationIntern(dealerId, dealerInformationInternAdd)

Add one customContent entry in dealer_information for internal.

Add one customContent entry in dealer_information for internal.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.DealersInternApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    DealersInternApi apiInstance = new DealersInternApi(defaultClient);
    Integer dealerId = 56; // Integer | 
    DealerInformationInternAdd dealerInformationInternAdd = new DealerInformationInternAdd(); // DealerInformationInternAdd | Dealer CustomContent insert.
    try {
      DefaultResponse result = apiInstance.addDealerInformationIntern(dealerId, dealerInformationInternAdd);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DealersInternApi#addDealerInformationIntern");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dealerId** | **Integer**|  |
 **dealerInformationInternAdd** | [**DealerInformationInternAdd**](DealerInformationInternAdd.md)| Dealer CustomContent insert. | [optional]

### Return type

[**DefaultResponse**](DefaultResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json;charset=utf-8
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | Add one dealer_customContent entry in dealer_customContent for internal. |  -  |

<a name="addDealerOwnFieldsIntern"></a>
# **addDealerOwnFieldsIntern**
> DealerOwnFieldsInternResultWithMeta addDealerOwnFieldsIntern(dealerId, dealerOwnFieldsInternAdd)

Add one ownFields entry in tblhaendlerEigeneFelder for internal.

Add one ownFields entry in tblhaendlerEigeneFelder for internal.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.DealersInternApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    DealersInternApi apiInstance = new DealersInternApi(defaultClient);
    Integer dealerId = 56; // Integer | 
    DealerOwnFieldsInternAdd dealerOwnFieldsInternAdd = new DealerOwnFieldsInternAdd(); // DealerOwnFieldsInternAdd | Dealer OwnFields insert/update.
    try {
      DealerOwnFieldsInternResultWithMeta result = apiInstance.addDealerOwnFieldsIntern(dealerId, dealerOwnFieldsInternAdd);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DealersInternApi#addDealerOwnFieldsIntern");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dealerId** | **Integer**|  |
 **dealerOwnFieldsInternAdd** | [**DealerOwnFieldsInternAdd**](DealerOwnFieldsInternAdd.md)| Dealer OwnFields insert/update. | [optional]

### Return type

[**DealerOwnFieldsInternResultWithMeta**](DealerOwnFieldsInternResultWithMeta.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json;charset=utf-8
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | Add or update one dealer_ownFields entry in tblhaendlerEigeneFelder for internal. |  -  |

<a name="addDealerSettingsIntern"></a>
# **addDealerSettingsIntern**
> DefaultResponse addDealerSettingsIntern(dealerId, dealerSettingsInternAdd)

Add one dealer_settings entry in dealer_settings for internal.

Add one dealer_settings entry in dealer_settings for internal.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.DealersInternApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    DealersInternApi apiInstance = new DealersInternApi(defaultClient);
    Integer dealerId = 56; // Integer | 
    DealerSettingsInternAdd dealerSettingsInternAdd = new DealerSettingsInternAdd(); // DealerSettingsInternAdd | Dealer settings insert.
    try {
      DefaultResponse result = apiInstance.addDealerSettingsIntern(dealerId, dealerSettingsInternAdd);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DealersInternApi#addDealerSettingsIntern");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dealerId** | **Integer**|  |
 **dealerSettingsInternAdd** | [**DealerSettingsInternAdd**](DealerSettingsInternAdd.md)| Dealer settings insert. | [optional]

### Return type

[**DefaultResponse**](DefaultResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json;charset=utf-8
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | Add one dealer_settings entry in dealer_settings for internal. |  -  |

<a name="deleteDealerCarparkIntern"></a>
# **deleteDealerCarparkIntern**
> DefaultResponse deleteDealerCarparkIntern(dealerId, dealerCarparkInternChange)

Delete one vehicle susuuid in carpark for internal.

Delete one vehicle susuuid in carpark for internal.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.DealersInternApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    DealersInternApi apiInstance = new DealersInternApi(defaultClient);
    Integer dealerId = 56; // Integer | 
    DealerCarparkInternChange dealerCarparkInternChange = new DealerCarparkInternChange(); // DealerCarparkInternChange | Dealer carpark delete.
    try {
      DefaultResponse result = apiInstance.deleteDealerCarparkIntern(dealerId, dealerCarparkInternChange);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DealersInternApi#deleteDealerCarparkIntern");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dealerId** | **Integer**|  |
 **dealerCarparkInternChange** | [**DealerCarparkInternChange**](DealerCarparkInternChange.md)| Dealer carpark delete. |

### Return type

[**DefaultResponse**](DefaultResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json;charset=utf-8
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | Delete one vehicle susuuid in carpark for internal. |  -  |

<a name="deleteDealerCooperationIntern"></a>
# **deleteDealerCooperationIntern**
> DefaultResponse deleteDealerCooperationIntern(dealerId, dealerCooperationInternDelete)

Delete one or more dealer_cooperation in dealer_cooperation for internal.

Delete one more dealer_cooperation in dealer_cooperation for internal.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.DealersInternApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    DealersInternApi apiInstance = new DealersInternApi(defaultClient);
    Integer dealerId = 56; // Integer | 
    DealerCooperationInternDelete dealerCooperationInternDelete = new DealerCooperationInternDelete(); // DealerCooperationInternDelete | Dealer cooperation delete.
    try {
      DefaultResponse result = apiInstance.deleteDealerCooperationIntern(dealerId, dealerCooperationInternDelete);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DealersInternApi#deleteDealerCooperationIntern");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dealerId** | **Integer**|  |
 **dealerCooperationInternDelete** | [**DealerCooperationInternDelete**](DealerCooperationInternDelete.md)| Dealer cooperation delete. |

### Return type

[**DefaultResponse**](DefaultResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json;charset=utf-8
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | Delete one more dealer_cooperation in dealer_cooperation for internal. |  -  |

<a name="deleteDealerInformationIntern"></a>
# **deleteDealerInformationIntern**
> DefaultResponse deleteDealerInformationIntern(dealerId, dealerInformationInternDelete)

Delete one customContent in dealer_information for internal.

Delete one customContent in dealer_information for internal.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.DealersInternApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    DealersInternApi apiInstance = new DealersInternApi(defaultClient);
    Integer dealerId = 56; // Integer | 
    DealerInformationInternDelete dealerInformationInternDelete = new DealerInformationInternDelete(); // DealerInformationInternDelete | Dealer CustomContent delete.
    try {
      DefaultResponse result = apiInstance.deleteDealerInformationIntern(dealerId, dealerInformationInternDelete);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DealersInternApi#deleteDealerInformationIntern");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dealerId** | **Integer**|  |
 **dealerInformationInternDelete** | [**DealerInformationInternDelete**](DealerInformationInternDelete.md)| Dealer CustomContent delete. |

### Return type

[**DefaultResponse**](DefaultResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json;charset=utf-8
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | Delete only one dealer_customContent in dealer_customContent for internal. |  -  |

<a name="deleteDealerSettingsIntern"></a>
# **deleteDealerSettingsIntern**
> DefaultResponse deleteDealerSettingsIntern(dealerId, dealerSettingsInternDelete)

Delete one or more dealer_settings in dealer_settings for internal.

Delete one more dealer_settings in dealer_settings for internal.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.DealersInternApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    DealersInternApi apiInstance = new DealersInternApi(defaultClient);
    Integer dealerId = 56; // Integer | 
    DealerSettingsInternDelete dealerSettingsInternDelete = new DealerSettingsInternDelete(); // DealerSettingsInternDelete | Dealer settings delete.
    try {
      DefaultResponse result = apiInstance.deleteDealerSettingsIntern(dealerId, dealerSettingsInternDelete);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DealersInternApi#deleteDealerSettingsIntern");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dealerId** | **Integer**|  |
 **dealerSettingsInternDelete** | [**DealerSettingsInternDelete**](DealerSettingsInternDelete.md)| Dealer settings delete. |

### Return type

[**DefaultResponse**](DefaultResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json;charset=utf-8
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | Delete one more dealer_settings in dealer_settings for internal. |  -  |

<a name="detailsDealerBillsIntern"></a>
# **detailsDealerBillsIntern**
> DealerBillsInternDetailsResultWithMeta detailsDealerBillsIntern(dealerId, dealerBillsInternDetails)

Get bills details for internal.

Get bills details for internal.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.DealersInternApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    DealersInternApi apiInstance = new DealersInternApi(defaultClient);
    Integer dealerId = 56; // Integer | 
    DealerBillsInternDetails dealerBillsInternDetails = new DealerBillsInternDetails(); // DealerBillsInternDetails | A list of bill_Ids
    try {
      DealerBillsInternDetailsResultWithMeta result = apiInstance.detailsDealerBillsIntern(dealerId, dealerBillsInternDetails);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DealersInternApi#detailsDealerBillsIntern");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dealerId** | **Integer**|  |
 **dealerBillsInternDetails** | [**DealerBillsInternDetails**](DealerBillsInternDetails.md)| A list of bill_Ids |

### Return type

[**DealerBillsInternDetailsResultWithMeta**](DealerBillsInternDetailsResultWithMeta.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json;charset=utf-8
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | An array of dealer bills details. |  -  |

<a name="detailsDealerCarparkIntern"></a>
# **detailsDealerCarparkIntern**
> DealerCarparkInternSearchResultWithMeta detailsDealerCarparkIntern(dealerId)

Get all vehicle from dealers carpark for internal.

Get all vehicle from dealers carpark for internal.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.DealersInternApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    DealersInternApi apiInstance = new DealersInternApi(defaultClient);
    Integer dealerId = 56; // Integer | 
    try {
      DealerCarparkInternSearchResultWithMeta result = apiInstance.detailsDealerCarparkIntern(dealerId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DealersInternApi#detailsDealerCarparkIntern");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dealerId** | **Integer**|  |

### Return type

[**DealerCarparkInternSearchResultWithMeta**](DealerCarparkInternSearchResultWithMeta.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | An array with vehicles from dealers carpark. |  -  |

<a name="getDealerVehicleManufacturer"></a>
# **getDealerVehicleManufacturer**
> DealerVehicleManufacturerResponseWithMeta getDealerVehicleManufacturer(dealerId, dealerVehicleManufacturerRequest)

Get a list of manufacturer of vehicles send by dealer.

Get a list of manufacturer of vehicles send by dealer.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.DealersInternApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    DealersInternApi apiInstance = new DealersInternApi(defaultClient);
    Integer dealerId = 56; // Integer | 
    DealerVehicleManufacturerRequest dealerVehicleManufacturerRequest = new DealerVehicleManufacturerRequest(); // DealerVehicleManufacturerRequest | List of dealers.
    try {
      DealerVehicleManufacturerResponseWithMeta result = apiInstance.getDealerVehicleManufacturer(dealerId, dealerVehicleManufacturerRequest);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DealersInternApi#getDealerVehicleManufacturer");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dealerId** | **Integer**|  |
 **dealerVehicleManufacturerRequest** | [**DealerVehicleManufacturerRequest**](DealerVehicleManufacturerRequest.md)| List of dealers. |

### Return type

[**DealerVehicleManufacturerResponseWithMeta**](DealerVehicleManufacturerResponseWithMeta.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json;charset=utf-8
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | List of manufacturer of vehicles send by dealer. |  -  |

<a name="putDealerData"></a>
# **putDealerData**
> DefaultResponse putDealerData(dealerId, dealerDataInternRequest)

Insert or update dealer data

Insert or update dealer data

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.DealersInternApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    DealersInternApi apiInstance = new DealersInternApi(defaultClient);
    Integer dealerId = 56; // Integer | 
    DealerDataInternRequest dealerDataInternRequest = new DealerDataInternRequest(); // DealerDataInternRequest | dealer data   (currently only UPDATE is allowed!)
    try {
      DefaultResponse result = apiInstance.putDealerData(dealerId, dealerDataInternRequest);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DealersInternApi#putDealerData");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dealerId** | **Integer**|  |
 **dealerDataInternRequest** | [**DealerDataInternRequest**](DealerDataInternRequest.md)| dealer data   (currently only UPDATE is allowed!) |

### Return type

[**DefaultResponse**](DefaultResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json;charset=utf-8
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | An array of dealerDetails |  -  |
**400** | Bad Request |  -  |
**401** | Unauthorized |  -  |
**404** | Not Found |  -  |
**0** | an array of dealerDetails |  -  |

<a name="searchDealerBillsIntern"></a>
# **searchDealerBillsIntern**
> DealerBillsInternSearchResultWithMeta searchDealerBillsIntern(dealerId, dealerBillsInternSearch)

Search bills for internal.

Search bills for internal.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.DealersInternApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    DealersInternApi apiInstance = new DealersInternApi(defaultClient);
    Integer dealerId = 56; // Integer | 
    DealerBillsInternSearch dealerBillsInternSearch = new DealerBillsInternSearch(); // DealerBillsInternSearch | Dealer bills search.
    try {
      DealerBillsInternSearchResultWithMeta result = apiInstance.searchDealerBillsIntern(dealerId, dealerBillsInternSearch);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DealersInternApi#searchDealerBillsIntern");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dealerId** | **Integer**|  |
 **dealerBillsInternSearch** | [**DealerBillsInternSearch**](DealerBillsInternSearch.md)| Dealer bills search. | [optional]

### Return type

[**DealerBillsInternSearchResultWithMeta**](DealerBillsInternSearchResultWithMeta.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json;charset=utf-8
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | A dealer Id. |  -  |

