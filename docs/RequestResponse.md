

# RequestResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**requestId** | **String** |  |  [optional]
**hostIp** | **String** |  |  [optional]
**timestamp** | **String** |  |  [optional]
**duration** | **Long** |  |  [optional]



