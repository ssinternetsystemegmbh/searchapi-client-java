

# OutletWhitelistResponseWithMetadata

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**MetaResponse**](MetaResponse.md) |  |  [optional]
**response** | [**OutletWhitelistResponse**](OutletWhitelistResponse.md) |  |  [optional]



