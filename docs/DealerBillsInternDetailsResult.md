

# DealerBillsInternDetailsResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealerId** | **Integer** |  |  [optional]
**billCounter** | **Integer** |  |  [optional]
**items** | [**List&lt;DealerBillsInternDetailsResultItem&gt;**](DealerBillsInternDetailsResultItem.md) |  |  [optional]



