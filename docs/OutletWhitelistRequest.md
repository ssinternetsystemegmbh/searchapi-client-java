

# OutletWhitelistRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | **String** | Email-Address |  [optional]
**dealerId** | **Long** | Dealer-ID |  [optional]
**company** | **String** | Company |  [optional]
**homepage** | **String** | Homepage |  [optional]
**active** | **Boolean** | active |  [optional]



