

# DealerAuthUserSingleResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user** | [**DealerAuthUserDto**](DealerAuthUserDto.md) |  |  [optional]



