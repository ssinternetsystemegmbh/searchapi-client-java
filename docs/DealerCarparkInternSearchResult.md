

# DealerCarparkInternSearchResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealerId** | **Integer** |  |  [optional]
**vehicleCarparkCounter** | **Integer** |  |  [optional]
**items** | [**List&lt;DealerCarparkInternSearchResultItem&gt;**](DealerCarparkInternSearchResultItem.md) |  |  [optional]



