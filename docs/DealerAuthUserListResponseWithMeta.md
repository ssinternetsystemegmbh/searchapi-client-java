

# DealerAuthUserListResponseWithMeta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**MetaResponse**](MetaResponse.md) |  |  [optional]
**response** | [**DealerAuthUserListResponse**](DealerAuthUserListResponse.md) |  |  [optional]



