

# SearchInquiry

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customerData** | [**CustomerInquiryData**](CustomerInquiryData.md) |  |  [optional]
**searchInquiryDetail** | [**Anfrage**](Anfrage.md) |  |  [optional]



