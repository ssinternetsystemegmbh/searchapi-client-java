

# VehicleSearchTagObject

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tag** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**count** | **Long** |  |  [optional]



