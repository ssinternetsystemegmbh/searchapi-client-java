

# Tblhaendlerdaten

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**firma** | **String** | Firma wird nur übernommen, wenn eine Händerneuanlage(INSERT) erfolgt. Zurzeit ist nur das UPDATE erlaubt und daher wird dieses Feld ignoriert. |  [optional]
**firmenzusatz** | **String** |  |  [optional]
**strasse** | **String** |  |  [optional]
**lhdId** | **Integer** | ID des Herkunktsland |  [optional]
**plz** | **String** |  |  [optional]
**ort** | **String** |  |  [optional]
**ansprechpartner1** | **String** |  |  [optional]
**ansprechpartner2** | **String** |  |  [optional]
**telefon1** | **String** |  |  [optional]
**telefon2** | **String** |  |  [optional]
**fax** | **String** |  |  [optional]
**mobil** | **String** |  |  [optional]
**email1** | **String** |  |  [optional]
**email2** | **String** |  |  [optional]
**emailtechnic** | **String** | Technik E-Mail |  [optional]
**emailbilling** | **String** | Rechnungs E-Mail |  [optional]
**homepage** | **String** |  |  [optional]
**ustid** | **String** |  |  [optional]
**hrb** | **String** |  |  [optional]
**gesellschaftsform** | **String** |  |  [optional]
**geschaeftsfuehrer** | **String** |  |  [optional]
**registergericht** | **String** |  |  [optional]
**registernummer** | **String** |  |  [optional]
**euidentnummer** | **String** |  |  [optional]
**vvregister** | **String** |  |  [optional]
**schiedsstelle** | **String** |  |  [optional]
**bank** | **String** |  |  [optional]
**iban** | **String** |  |  [optional]
**bic** | **String** |  |  [optional]



