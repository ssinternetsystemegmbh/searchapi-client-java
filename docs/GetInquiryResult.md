

# GetInquiryResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**inquiryObjects** | [**List&lt;InquiryObject&gt;**](InquiryObject.md) |  |  [optional]



