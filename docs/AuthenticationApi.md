# AuthenticationApi

All URIs are relative to *https://develop-api.ssis.de:8443/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getNewWebToken**](AuthenticationApi.md#getNewWebToken) | **POST** /prevapi/public/getNewWebToken | Renew the given Web-Token.
[**webLogin**](AuthenticationApi.md#webLogin) | **POST** /prevapi/public/webLogin | Login the given Web-User and return a Token.


<a name="getNewWebToken"></a>
# **getNewWebToken**
> JwtTokenResponseWithMeta getNewWebToken(jwTTokenRequest)

Renew the given Web-Token.

Renew the given Web-Token.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.AuthenticationApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    AuthenticationApi apiInstance = new AuthenticationApi(defaultClient);
    JWTTokenRequest jwTTokenRequest = new JWTTokenRequest(); // JWTTokenRequest | 
    try {
      JwtTokenResponseWithMeta result = apiInstance.getNewWebToken(jwTTokenRequest);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling AuthenticationApi#getNewWebToken");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jwTTokenRequest** | [**JWTTokenRequest**](JWTTokenRequest.md)|  | [optional]

### Return type

[**JwtTokenResponseWithMeta**](JwtTokenResponseWithMeta.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json;charset=utf-8
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |
**401** | Username or password wrong. |  -  |
**0** | Other error responses. |  -  |

<a name="webLogin"></a>
# **webLogin**
> JwtTokenResponseWithMeta webLogin(authenticationRequest)

Login the given Web-User and return a Token.

Login the given Web-User and return a Token.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.AuthenticationApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    AuthenticationApi apiInstance = new AuthenticationApi(defaultClient);
    AuthenticationRequest authenticationRequest = new AuthenticationRequest(); // AuthenticationRequest | 
    try {
      JwtTokenResponseWithMeta result = apiInstance.webLogin(authenticationRequest);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling AuthenticationApi#webLogin");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authenticationRequest** | [**AuthenticationRequest**](AuthenticationRequest.md)|  | [optional]

### Return type

[**JwtTokenResponseWithMeta**](JwtTokenResponseWithMeta.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json;charset=utf-8
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |
**401** | Username or password wrong. |  -  |
**0** | Other error responses. |  -  |

