

# Cooperation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cooperation** | **String** |  |  [optional]
**status** | **String** |  |  [optional]
**userId** | **String** |  |  [optional]
**email** | **String** |  |  [optional]



