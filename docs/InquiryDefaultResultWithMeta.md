

# InquiryDefaultResultWithMeta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**MetaResponse**](MetaResponse.md) |  |  [optional]
**response** | [**InquiryDefaultResult**](InquiryDefaultResult.md) |  |  [optional]



