

# Outlet

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**outletAttachment** | **String** |  |  [optional]
**outletMailBody** | **String** |  |  [optional]
**outletMailId** | **String** |  |  [optional]
**outletSenderEmail** | **String** |  |  [optional]
**outletSenderName** | **String** |  |  [optional]
**vehicleType** | **String** |  |  [optional]
**dateIndexed** | **String** |  |  [optional]
**outletDateTransmitted** | **String** |  |  [optional]
**outletSubject** | **String** |  |  [optional]



