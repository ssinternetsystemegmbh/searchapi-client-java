

# Calcpath

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tax** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**distributorTopOfferDiscount** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**distributorTopOfferDiscountFactor** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**distributorDiscount** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**basePriceWithTax** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**discountedBasePrice** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**calculatedPriceNet** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**topOfferDiscount** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**vehicleIsTopOffer** | **Boolean** |  |  [optional]
**priceGroupSurchargeAbsolute** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**priceGroupSurchargeRelative** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**topOfferResellerDiscount** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**distributorSurcharge** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**finalGrossPrice** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**rounding** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**finalGrossPriceRounded** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**finalGrossPricePromotioned** | [**BigDecimal**](BigDecimal.md) |  |  [optional]



