# AdminUserPasswordmanagerApi

All URIs are relative to *https://develop-api.ssis.de:8443/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**changePassword**](AdminUserPasswordmanagerApi.md#changePassword) | **POST** /prevapi/admin/user/passwordmanager/password/change | Endpoint for change password.
[**changePasswordWithSessionId**](AdminUserPasswordmanagerApi.md#changePasswordWithSessionId) | **POST** /prevapi/admin/user/passwordmanager/password/changewithsessionid | Endpoint for change password with session id.
[**checkSessionID**](AdminUserPasswordmanagerApi.md#checkSessionID) | **GET** /prevapi/admin/user/passwordmanager/password/checksession | Endpoint to check existing a session.
[**sendNewPasswordEmail**](AdminUserPasswordmanagerApi.md#sendNewPasswordEmail) | **POST** /prevapi/admin/user/passwordmanager/password/new | Endpoint for get a session id for set a new password.


<a name="changePassword"></a>
# **changePassword**
> ChangePasswordResponseWithMeta changePassword(changePasswordRequest)

Endpoint for change password.

Endpoint for change password.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.AdminUserPasswordmanagerApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    AdminUserPasswordmanagerApi apiInstance = new AdminUserPasswordmanagerApi(defaultClient);
    ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest(); // ChangePasswordRequest | Set new password.
    try {
      ChangePasswordResponseWithMeta result = apiInstance.changePassword(changePasswordRequest);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling AdminUserPasswordmanagerApi#changePassword");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **changePasswordRequest** | [**ChangePasswordRequest**](ChangePasswordRequest.md)| Set new password. |

### Return type

[**ChangePasswordResponseWithMeta**](ChangePasswordResponseWithMeta.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json;charset=utf-8
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |
**401** | Username or password wrong. |  -  |
**0** | Other error responses. |  -  |

<a name="changePasswordWithSessionId"></a>
# **changePasswordWithSessionId**
> ChangePasswordWithSessionIdResponse changePasswordWithSessionId(changePasswordWithSessionIdRequest)

Endpoint for change password with session id.

Endpoint for change password with session id.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.AdminUserPasswordmanagerApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    AdminUserPasswordmanagerApi apiInstance = new AdminUserPasswordmanagerApi(defaultClient);
    ChangePasswordWithSessionIdRequest changePasswordWithSessionIdRequest = new ChangePasswordWithSessionIdRequest(); // ChangePasswordWithSessionIdRequest | Set new password.
    try {
      ChangePasswordWithSessionIdResponse result = apiInstance.changePasswordWithSessionId(changePasswordWithSessionIdRequest);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling AdminUserPasswordmanagerApi#changePasswordWithSessionId");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **changePasswordWithSessionIdRequest** | [**ChangePasswordWithSessionIdRequest**](ChangePasswordWithSessionIdRequest.md)| Set new password. |

### Return type

[**ChangePasswordWithSessionIdResponse**](ChangePasswordWithSessionIdResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json;charset=utf-8
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |
**401** | Username or password wrong. |  -  |
**0** | Other error responses. |  -  |

<a name="checkSessionID"></a>
# **checkSessionID**
> CommonResponseWithMeta checkSessionID(uuid)

Endpoint to check existing a session.

Endpoint to check existing a session.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.AdminUserPasswordmanagerApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    AdminUserPasswordmanagerApi apiInstance = new AdminUserPasswordmanagerApi(defaultClient);
    String uuid = "uuid_example"; // String | 
    try {
      CommonResponseWithMeta result = apiInstance.checkSessionID(uuid);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling AdminUserPasswordmanagerApi#checkSessionID");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uuid** | **String**|  |

### Return type

[**CommonResponseWithMeta**](CommonResponseWithMeta.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success. &#39;true&#39; when a valid session id exists. |  -  |
**401** | Username or password wrong. |  -  |
**0** | Other error responses. |  -  |

<a name="sendNewPasswordEmail"></a>
# **sendNewPasswordEmail**
> UserNewPasswordResponseWithMeta sendNewPasswordEmail(userNewPasswordRequest)

Endpoint for get a session id for set a new password.

Endpoint for get a session id for set a new password.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.AdminUserPasswordmanagerApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    AdminUserPasswordmanagerApi apiInstance = new AdminUserPasswordmanagerApi(defaultClient);
    UserNewPasswordRequest userNewPasswordRequest = new UserNewPasswordRequest(); // UserNewPasswordRequest | Send password change email.
    try {
      UserNewPasswordResponseWithMeta result = apiInstance.sendNewPasswordEmail(userNewPasswordRequest);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling AdminUserPasswordmanagerApi#sendNewPasswordEmail");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userNewPasswordRequest** | [**UserNewPasswordRequest**](UserNewPasswordRequest.md)| Send password change email. |

### Return type

[**UserNewPasswordResponseWithMeta**](UserNewPasswordResponseWithMeta.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json;charset=utf-8
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |
**401** | Username or password wrong. |  -  |
**0** | Other error responses. |  -  |

