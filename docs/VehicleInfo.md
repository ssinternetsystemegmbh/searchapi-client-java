

# VehicleInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vehicleId** | **Integer** |  |  [optional]
**susuuid** | **String** |  |  [optional]
**dateLastIndexed** | **String** |  |  [optional]



