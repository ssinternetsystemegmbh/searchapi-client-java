# config.properties
***
First you must rename the [config.properties.dist](../../config.properties.dist) to **config.properties**

Then you must enter here your credentials.

```java
# SSiS Open API Client Properties
host=https://api.ssis.de:8443/api
temp_folder_path=/var/tmp/php/
username=your_dealer_id
password=your_password
dealerid=your_dealerid
```