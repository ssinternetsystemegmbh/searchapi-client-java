# Client
***
This class simplifies the usage of the swagger-generated api classes.

It includes ia. the HTTP request methods from the [**DealersApi**](../DealersApi.md) and the [**VehiclesApi**](../VehiclesApi.md)

All URIs are relative to *https://api.ssis.de:8443/api*

### Constructors

> public Client()

If you are use the [config.properties](config.properties) file, you can use this constructor.

> public Client(String username, String password, Integer dealerId)

If you don't want to use the [config.properties](config.properties.md) file, you can use this constructor and set the host with the [**setHost**](#markdown-header-sethost) method.


#### [**Class Constants**](#markdown-header-constants-for-vehicle-aggregation-types)



Method | Return Type | Description
------------- | ------------- | -------------
[**getClientVersion**](#markdown-header-getclientversion) | **String** | Returns the version of the API Client
[**getHost**](#markdown-header-gethost) | **String** | Returns the API Url
[**setHost**](#markdown-header-sethost) | **[Client](Client.md)** | Sets the API Url
[**getDealerId**](#markdown-header-getdealerid) | **Integer**  | Returns the Dealer ID
[**setDealerId**](#markdown-header-setdealerid) | **[Client](Client.md)** | Sets the Dealer ID
[**getPagParams**](#markdown-header-getpagparams) | **Map<String, String>**  | Returns the Pagination Parameters
[**setPaginationParameters**](#markdown-header-setpaginationparameters) | **[Client](Client.md)** | Set the Pagination Parameters Object
[**getPaginationParameters**](#markdown-header-getpaginationparameters) | **[PaginationParameters](../Model/PaginationParameters.md)** | Returns the Pagination Parameters Object
[**setPagParamResultCount**](#markdown-header-setpagparamresultcount) | **[Client](Client.md)** | Set the number of return values
[**setPagParamResultOffset**](#markdown-header-setpagparamresultoffset) | **[Client](Client.md)** | Set the offset of the return values
[**setPagParamSortType**](#markdown-header-setpagparamsorttype) | **[Client](Client.md)** | Set the fieldname for sort (default: id)
[**setPagParamSortOrderAsc**](#markdown-header-setpagparamsortorderasc) | **[Client](Client.md)** | Set the sort direction (default: ascending)
[**getDealerDetailsRequest**](#markdown-header-getdealerdetailsrequest) | **[DealerDetailsRequest](../Model/DealerDetailsRequest.md)** | Returns a DealerDetailsRequest Object
[**getVehicleTagSearch**](#markdown-header-getvehicletagsearch) | **[TagSearch](../Model/TagSearch.md)** | Returns a TagSearch Object
[**getVehicleSearch**](#markdown-header-getvehiclesearch) | **[VehicleSearch](../Model/VehicleSearch.md)** | Returns a VehicleSearch Object
[**getVehicleDetailsRequest**](#markdown-header-getvehicledetailsrequest) | **[VehicleDetailsRequest](../Model/VehicleDetailsRequest.md)** | Returns a VehicleDetailsRequest Object
[**getDealerDetails**](#markdown-header-getdealerdetails) | **[DealerResponse](../Model/DealerResponse.md)** | Returns a DealerResponse Object with data from API
[**getVehicleAggregationTags**](#markdown-header-getvehicleaggregationtags) | **[TagSearchResult](../Model/TagSearchResult.md)** | Returns a TagSearchResult Object with data from API
[**getVehicleSearchResult**](#markdown-header-getvehiclesearchresult) | **[VehicleSearchResult](../Model/VehicleSearchResult.md)** | Returns a VehicleSearchResult Object with data from API
[**getVehicleDetails**](#markdown-header-getvehicledetails) | **[VehicleResponse](../Model/VehicleResponse.md)** | Returns a VehicleResponse Object with data from API
[**getLastMetadata**](#markdown-header-getlastmetadata) | **[MetaResponse](../Model/MetaResponse.md)** | Returns a MetaResponse Object from last request


### Constants for vehicle aggregation types

Constant | Type | Value
------------- | ------------- | -------------
**AGG_TYPE_MANUFACTURER** | **String** | MANUFACTURER
**AGG_TYPE_SERIES** | **String** | SERIES
**AGG_TYPE_ORDERSTATUS** | **String** | ORDERSTATUS
**AGG_TYPE_BODYTYPE** | **String** | BODYTYPE
**AGG_TYPE_BODYTYPEGROUP** | **String** | BODYTYPEGROUP
**AGG_TYPE_FUELTYPEPRIMARY** | **String** | FUELTYPEPRIMARY
**AGG_TYPE_DEALERID_VEHICLEOWNER** | **String** | DEALERID_VEHICLEOWNER
**AGG_TYPE_COLORTAG** | **String** | COLORTAG
**AGG_TYPE_UPHOLSTERYTYPETAG** | **String** | UPHOLSTERYTYPETAG
**AGG_TYPE_PROPULSIONTYPETAG** | **String** | PROPULSIONTYPETAG
**AGG_TYPE_POLLUTIONBADGETAG** | **String** | POLLUTIONBADGETAG
**AGG_TYPE_EURONORMTAG** | **String** | EURONORMTAG
**AGG_TYPE_TRANSMISSIONTYPE** | **String** | TRANSMISSIONTYPE


# **getClientVersion**
> string getClientVersion()

Returns the version of the API Client

### Return type
string


# **getHost**
> String getHost()

Returns the API Url

### Return type
String


# **setHost**
> Client setHost(String $host)

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 host | String | set the API Url | overwrites the host from configuration

### Return type
[Client](Client.md)


# **getDealerId**
> int getDealerId()

Returns the Dealer ID

### Return type
int


# **setDealerId**
> Client setDealerId(int dealerId)

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 dealerId | int | set the Dealer ID | overwrites the Dealer ID from configuration

### Return type
[Client](Client.md)


# **getPagParams**
> Map<String, String> getPagParams()

Returns the Pagination Parameters

### Return type
Map<String, String>

Default values:
```java
['result_count' => 10, 'result_offset' => 0, 'sort_type' => 'id', 'sort_order_asc' => true]
```


# **setPaginationParameters**
> Client setPaginationParameters(Map<String, String> pagParams)

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 pagParams | (Map<String, String> | set the Pagination Parameters Object | structure is like [getPagParams](Client.md#getpagparams)

### Return type
[Client](Client.md)


# **getPaginationParameters**
> de.ssis.api.client.swagger.model.PaginationParameters getPaginationParameters()

Returns the Pagination Parameters Object

### Return type
[de.ssis.api.client.swagger.model.PaginationParameters](../PaginationParameters.md)


# **setPagParamResultCount**
> Client setPagParamResultCount(int cnt)

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 cnt | int | Set the number of return values | value in PaginationParameters

### Return type
[Client](Client.md)



# **setPagParamResultOffset**
> Client setPagParamResultOffset(int offset)

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 offset | int | Set the offset of return values | value in PaginationParameters

### Return type
[Client](Client.md)


# **setPagParamSortType**
> Client setPagParamSortType(String sortType)

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 sortType | String | Set the fieldname for sort (default: id) | value in PaginationParameters

### Return type
[Client](Client.md)


# **setPagParamSortOrderAsc**
> Client setPagParamSortOrderAsc(boolean sortOrderAsc)

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 sortOrderAsc | boolean | Set the sort direction (default: ascending) | value in PaginationParameters

### Return type
[Client](Client.md)


# **getDealerDetailsRequest**
> de.ssis.api.client.swagger.model.DealerDetailsRequest getDealerDetailsRequest()

Returns a DealerDetailsRequest Object

used for [getDealerDetails()](Client.md#getdealerdetails)

### Return type
[de.ssis.api.client.swagger.model.DealerDetailsRequest](../DealerDetailsRequest.md)


# **getVehicleTagSearch**
> de.ssis.api.client.swagger.model.TagSearch getVehicleTagSearch()

Returns a TagSearch Object

used for [getVehicleAggregationTags()](Client.md#getvehicleaggregationtags)

### Return type
[de.ssis.api.client.swagger.model.TagSearch](../TagSearch.md)


# **getVehicleSearch**
> de.ssis.api.client.swagger.model.VehicleSearch getVehicleSearch()

Returns a VehicleSearch Object

used for [getVehicleSearchResult()](Client.md#getvehiclesearchresult)

### Return type
[de.ssis.api.client.swagger.model.VehicleSearch](../VehicleSearch.md)


# **getVehicleDetailsRequest**
> de.ssis.api.client.swagger.model.VehicleDetailsRequest getVehicleDetailsRequest()

Returns a VehicleDetailsRequest Object

used for [getVehicleDetails()](Client.md#getvehicledetails)

### Return type
[de.ssis.api.client.swagger.model.VehicleDetailsRequest](../VehicleDetailsRequest.md)


# **getDealerDetails**
> de.ssis.api.client.swagger.model.DealerResponse getDealerDetails(DealerDetailsRequest dealerDetailsRequest)

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 dealerDetailsRequest | [DealerDetailsRequest](../DealerDetailsRequest.md) | Returns a DealerResponse Object with data from API | 

### Return type
[de.ssis.api.client.swagger.model.DealerResponse](../DealerResponse.md)


# **getVehicleAggregationTags**
> de.ssis.api.client.swagger.model.TagSearchResult getVehicleAggregationTags(TagSearch vehicleTagSearch, int dealerid = null)

Returns a TagSearchResult Object with data from API

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 vehicleTagSearch | [TagSearch](../TagSearch.md) |  | **required**
 dealerid | int | default *null* (Dealer ID from configuration will be used) | *optional*

### Return type
[de.ssis.api.client.swagger.model.TagSearchResult](../TagSearchResult.md)


# **getVehicleSearchResult**
> de.ssis.api.client.swagger.model.VehicleSearchResult getVehicleSearchResult(VehicleSearch vehicleSearch, int dealerid = null)

Returns a VehicleSearchResult Object with data from API

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 vehicleSearch | [VehicleSearch](../VehicleSearch.md) |  | **required**
 dealerid | int | default *null* (Dealer ID from configuration will be used) | *optional*

### Return type
[de.ssis.api.client.swagger.model.VehicleSearchResult](../VehicleSearchResult.md)


# **getVehicleDetails**
> de.ssis.api.client.swagger.model.VehicleResponse getVehicleDetails(VehicleDetailsRequest vehicleDetailsRequest, int dealerid = null)

Returns a VehicleResponse Object with data from API

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 vehicleDetailsRequest | [VehicleDetailsRequest](../VehicleDetailsRequest.md) |  | **required**
 dealerid | int | default *null* (Dealer ID from configuration will be used) | *optional*

### Return type
[de.ssis.api.client.swagger.model.VehicleResponse](../VehicleResponse.md)


# **getLastMetadata**
> de.ssis.api.client.swagger.model.MetaResponse getLastMetadata()

Returns a MetaResponse Object with data from last request

### Return type
[de.ssis.api.client.swagger.model.MetaResponse](../MetaResponse.md)
