# InquiriesInternApi

All URIs are relative to *https://develop-api.ssis.de:8443/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**direktinquiryintern**](InquiriesInternApi.md#direktinquiryintern) | **POST** /admin/inquiriesintern/{dealerid}/direktinquiryintern | indicate interest about a specific Car to a specific Dealer
[**freesearch**](InquiriesInternApi.md#freesearch) | **POST** /admin/inquiriesintern/freesearch | indicate interest about a car with specific Attributes to all Dealer
[**triggerScheduler**](InquiriesInternApi.md#triggerScheduler) | **GET** /admin/inquiriesintern/triggerscheduler | Trigger the inquiry scheduler email.


<a name="direktinquiryintern"></a>
# **direktinquiryintern**
> InquiryDefaultResultWithMeta direktinquiryintern(dealerid, directInquiryIntern)

indicate interest about a specific Car to a specific Dealer

indicate interest about a specific Car to a specific Dealer

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.InquiriesInternApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    InquiriesInternApi apiInstance = new InquiriesInternApi(defaultClient);
    Integer dealerid = 56; // Integer | 
    DirectInquiryIntern directInquiryIntern = new DirectInquiryIntern(); // DirectInquiryIntern | vehicleIds
    try {
      InquiryDefaultResultWithMeta result = apiInstance.direktinquiryintern(dealerid, directInquiryIntern);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling InquiriesInternApi#direktinquiryintern");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dealerid** | **Integer**|  |
 **directInquiryIntern** | [**DirectInquiryIntern**](DirectInquiryIntern.md)| vehicleIds |

### Return type

[**InquiryDefaultResultWithMeta**](InquiryDefaultResultWithMeta.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json;charset=utf-8
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | indicate interest about a specific Car to a specific Dealer |  -  |
**400** | Bad Request |  -  |
**401** | Unauthorized |  -  |
**404** | Not Found |  -  |

<a name="freesearch"></a>
# **freesearch**
> InquiryDefaultResultWithMeta freesearch(freeInquiryRequest)

indicate interest about a car with specific Attributes to all Dealer

indicate interest about a car with specific Attributes to all Dealer

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.InquiriesInternApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    InquiriesInternApi apiInstance = new InquiriesInternApi(defaultClient);
    FreeInquiryRequest freeInquiryRequest = new FreeInquiryRequest(); // FreeInquiryRequest | Custom Preferences
    try {
      InquiryDefaultResultWithMeta result = apiInstance.freesearch(freeInquiryRequest);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling InquiriesInternApi#freesearch");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **freeInquiryRequest** | [**FreeInquiryRequest**](FreeInquiryRequest.md)| Custom Preferences |

### Return type

[**InquiryDefaultResultWithMeta**](InquiryDefaultResultWithMeta.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json;charset=utf-8
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | indicate interest abaout a car with specific Attributes to all Dealer |  -  |
**400** | Bad Request |  -  |
**401** | Unauthorized |  -  |
**404** | Not Found |  -  |

<a name="triggerScheduler"></a>
# **triggerScheduler**
> triggerScheduler(dealerId)

Trigger the inquiry scheduler email.

Trigger the inquiry scheduler email.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.InquiriesInternApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    InquiriesInternApi apiInstance = new InquiriesInternApi(defaultClient);
    Integer dealerId = 56; // Integer | 
    try {
      apiInstance.triggerScheduler(dealerId);
    } catch (ApiException e) {
      System.err.println("Exception when calling InquiriesInternApi#triggerScheduler");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dealerId** | **Integer**|  | [optional]

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**400** | Bad Request |  -  |
**401** | Unauthorized |  -  |
**404** | Not Found |  -  |

