

# VehicleSelectionListVehicleAddDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealerId** | **Long** | Unique identifier for the dealer. | 
**listId** | **Long** | Unique identifier for the vehicle selection list. | 
**vehicles** | [**List&lt;VehicleSelectionListVehicleSaveDto&gt;**](VehicleSelectionListVehicleSaveDto.md) | List of vehicles to be added to the vehicle selection list. | 



