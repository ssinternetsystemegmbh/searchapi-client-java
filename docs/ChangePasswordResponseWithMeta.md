

# ChangePasswordResponseWithMeta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**MetaResponse**](MetaResponse.md) |  |  [optional]
**response** | [**ChangePasswordResponse**](ChangePasswordResponse.md) |  |  [optional]



