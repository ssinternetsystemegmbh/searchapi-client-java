

# HealthCheckResponseWrapper

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**MetaResponse**](MetaResponse.md) |  |  [optional]
**elasticsearch** | [**HealthCheckResult**](HealthCheckResult.md) |  |  [optional]
**persistence** | [**HealthCheckResult**](HealthCheckResult.md) |  |  [optional]



