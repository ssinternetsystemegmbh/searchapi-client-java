

# InquiryStatus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**wasSeen** | **Boolean** | Inquiry was seen by the dealer. |  [optional]
**wasProcessed** | **Boolean** | Inquiry was processed by the dealer. |  [optional]
**wasSent** | **Boolean** | Inquiry was sent to the dealer (immediate or scheduled). |  [optional]



