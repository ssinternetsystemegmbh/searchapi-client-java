

# PwkQueueIndexingStatisticsDtoWithMeta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**MetaResponse**](MetaResponse.md) |  |  [optional]
**response** | [**PwkQueueIndexingStatisticsDto**](PwkQueueIndexingStatisticsDto.md) |  |  [optional]



