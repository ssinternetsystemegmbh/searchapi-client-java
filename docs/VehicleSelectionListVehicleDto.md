

# VehicleSelectionListVehicleDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**susuuid** | **String** | Unique identifier for the vehicle. |  [optional]
**metaData** | **String** | Metadata containing additional information about the vehicle. |  [optional]
**dateCreated** | [**DateTime**](DateTime.md) | Timestamp indicating when the vehicle was added to the selection list. |  [optional]



