

# DealerCarparkInternSearchResultWithMeta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**MetaResponse**](MetaResponse.md) |  |  [optional]
**response** | [**DealerCarparkInternSearchResult**](DealerCarparkInternSearchResult.md) |  |  [optional]



