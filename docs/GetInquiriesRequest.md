

# GetInquiriesRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**processed** | **Boolean** |  |  [optional]
**seen** | **Boolean** |  |  [optional]
**sent** | **Boolean** |  |  [optional]
**fromDate** | **String** |  |  [optional]
**toDate** | **String** |  |  [optional]



