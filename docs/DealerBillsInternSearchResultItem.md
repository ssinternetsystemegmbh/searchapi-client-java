

# DealerBillsInternSearchResultItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**billId** | **Integer** |  |  [optional]
**amountNetto** | **Double** |  |  [optional]
**amountBrutto** | **Double** |  |  [optional]
**filename** | **String** |  |  [optional]
**billingYear** | **String** |  |  [optional]
**billingMonth** | **String** |  |  [optional]
**isDebit** | **Integer** |  |  [optional]
**isStorno** | **Integer** |  |  [optional]
**dateSent** | **String** |  |  [optional]
**dateCreated** | **String** |  |  [optional]



