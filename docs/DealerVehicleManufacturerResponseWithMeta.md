

# DealerVehicleManufacturerResponseWithMeta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**MetaResponse**](MetaResponse.md) |  |  [optional]
**response** | [**DealerVehicleManufacturerResponse**](DealerVehicleManufacturerResponse.md) |  |  [optional]



