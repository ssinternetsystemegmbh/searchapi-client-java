

# DealerSearch

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealerIds** | **List&lt;Integer&gt;** |  |  [optional]
**company** | **String** |  |  [optional]
**countryIso** | **String** |  |  [optional]
**includeNonPublicDealerRange** | **Boolean** |  |  [optional]
**products** | [**List&lt;ProductSearch&gt;**](ProductSearch.md) |  |  [optional]
**geoDistanceSearch** | [**GeoDistanceSearch**](GeoDistanceSearch.md) |  |  [optional]
**paginationParameters** | [**PaginationParameters**](PaginationParameters.md) |  |  [optional]
**dateModified** | **List&lt;String&gt;** |  |  [optional]
**city** | **String** |  |  [optional]



