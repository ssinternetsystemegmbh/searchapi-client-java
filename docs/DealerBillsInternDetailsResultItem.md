

# DealerBillsInternDetailsResultItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**billId** | **Integer** |  |  [optional]
**filename** | **String** |  |  [optional]
**pdfData** | **String** | The content of this string value is displayed as hexcode.  (You may need to remove the leading two chars &#39;0x&#39; for further processing) |  [optional]



