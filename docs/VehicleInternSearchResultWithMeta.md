

# VehicleInternSearchResultWithMeta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**MetaResponse**](MetaResponse.md) |  |  [optional]
**response** | [**VehicleInternSearchResult**](VehicleInternSearchResult.md) |  |  [optional]



