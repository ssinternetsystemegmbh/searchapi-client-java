

# OutletWhitelistResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**outletWhitelistDtos** | [**List&lt;OutletWhitelistDto&gt;**](OutletWhitelistDto.md) |  |  [optional]



