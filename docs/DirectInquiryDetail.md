

# DirectInquiryDetail

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vehicleId** | **String** |  |  [optional]
**optionalEquipmentIds** | **List&lt;Long&gt;** | A list of the selected optional equipment id&#39;s from the inquiry vehicle. |  [optional]
**bemerkung** | **String** |  |  [optional]
**sonstiges** | **String** |  |  [optional]
**cargarantie** | **Integer** |  |  [optional]
**kaufzeitraum** | **String** |  |  [optional]



