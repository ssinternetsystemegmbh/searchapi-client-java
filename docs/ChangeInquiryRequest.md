

# ChangeInquiryRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**inquiryId** | **Integer** |  |  [optional]
**processed** | **Boolean** |  |  [optional]
**seen** | **Boolean** |  |  [optional]
**sent** | **Boolean** |  |  [optional]



