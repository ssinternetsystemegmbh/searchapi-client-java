
# Anfrage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**lsnId** | **Integer** |  |  [optional]
**kundennummer** | **String** |  |  [optional]
**anfragedatum** | **String** |  |  [optional]
**anftyp** | **String** |  |  [optional]
**hersteller** | **String** |  |  [optional]
**baureihe** | **String** |  |  [optional]
**modell** | **String** |  |  [optional]
**typ** | **String** |  |  [optional]
**ausfuehrung** | **String** |  |  [optional]
**motorart** | **String** |  |  [optional]
**hubraum** | **String** |  |  [optional]
**kw** | **String** |  |  [optional]
**ps** | **String** |  |  [optional]
**farbwunsch** | **String** |  |  [optional]
**gebraucht** | [**GebrauchtEnum**](#GebrauchtEnum) |  |  [optional]
**vonBaujahr** | **String** |  |  [optional]
**bisBaujahr** | **String** |  |  [optional]
**maxLaufleistung** | **String** |  |  [optional]
**schiebedach** | [**SchiebedachEnum**](#SchiebedachEnum) |  |  [optional]
**zentralverriegelung** | [**ZentralverriegelungEnum**](#ZentralverriegelungEnum) |  |  [optional]
**elfensterheber** | [**ElfensterheberEnum**](#ElfensterheberEnum) |  |  [optional]
**getriebeart** | **String** |  |  [optional]
**klimaanlage** | [**KlimaanlageEnum**](#KlimaanlageEnum) |  |  [optional]
**esp** | [**EspEnum**](#EspEnum) |  |  [optional]
**sitzheizung** | [**SitzheizungEnum**](#SitzheizungEnum) |  |  [optional]
**radiomitcd** | [**RadiomitcdEnum**](#RadiomitcdEnum) |  |  [optional]
**abgaspartikelfilter** | [**AbgaspartikelfilterEnum**](#AbgaspartikelfilterEnum) |  |  [optional]
**allradantrieb** | [**AllradantriebEnum**](#AllradantriebEnum) |  |  [optional]
**leichtmetallfelgen** | [**LeichtmetallfelgenEnum**](#LeichtmetallfelgenEnum) |  |  [optional]
**navigationssystem** | [**NavigationssystemEnum**](#NavigationssystemEnum) |  |  [optional]
**parkdistancecontrol** | [**ParkdistancecontrolEnum**](#ParkdistancecontrolEnum) |  |  [optional]
**tempomat** | [**TempomatEnum**](#TempomatEnum) |  |  [optional]
**abs** | [**AbsEnum**](#AbsEnum) |  |  [optional]
**anhaengerkupplung** | [**AnhaengerkupplungEnum**](#AnhaengerkupplungEnum) |  |  [optional]
**klimaautomatik** | [**KlimaautomatikEnum**](#KlimaautomatikEnum) |  |  [optional]
**lederausstattung** | [**LederausstattungEnum**](#LederausstattungEnum) |  |  [optional]
**seitenairbags** | [**SeitenairbagsEnum**](#SeitenairbagsEnum) |  |  [optional]
**unfallfahrzeug** | [**UnfallfahrzeugEnum**](#UnfallfahrzeugEnum) |  |  [optional]
**xenonscheinwerfer** | [**XenonscheinwerferEnum**](#XenonscheinwerferEnum) |  |  [optional]
**kaufzeitraum** | **String** |  |  [optional]
**verbrauchStadt** | **Double** |  |  [optional]
**verbrauchAussen** | **Double** |  |  [optional]
**verbrauchGesamt** | **Double** |  |  [optional]
**verbrauchStrom** | **Double** |  |  [optional]
**co2Emission** | **Double** |  |  [optional]
**co2Effizienz** | **String** |  |  [optional]
**zombi** | [**ZombiEnum**](#ZombiEnum) |  |  [optional]
**angebotsnr** | **String** |  |  [optional]
**serienzubehoer** | **String** |  |  [optional]
**aufpreiszubehoer** | **String** |  |  [optional]
**preis** | **Double** |  |  [optional]
**preisZubehoer** | **Double** |  |  [optional]
**preisobergrenze** | **Double** |  |  [optional]
**versandStatus** | **String** |  |  [optional]
**bemerkung** | **String** |  |  [optional]
**sonstiges** | **String** |  |  [optional]
**kopiert** | [**KopiertEnum**](#KopiertEnum) |  |  [optional]
**quelle** | [**QuelleEnum**](#QuelleEnum) |  |  [optional]
**waehrung** | **String** |  |  [optional]
**mwstfaktor** | **Double** |  |  [optional]
**cargarantieselection** | **Integer** |  |  [optional]
**datenschutzOk** | [**DatenschutzOkEnum**](#DatenschutzOkEnum) |  |  [optional]
**dateCreated** | **String** |  |  [optional]
**dateModified** | **String** |  |  [optional]


<a name="GebrauchtEnum"></a>
## Enum: GebrauchtEnum
Name | Value
---- | -----
Y | &quot;Y&quot;
N | &quot;N&quot;


<a name="SchiebedachEnum"></a>
## Enum: SchiebedachEnum
Name | Value
---- | -----
Y | &quot;Y&quot;
N | &quot;N&quot;


<a name="ZentralverriegelungEnum"></a>
## Enum: ZentralverriegelungEnum
Name | Value
---- | -----
Y | &quot;Y&quot;
N | &quot;N&quot;


<a name="ElfensterheberEnum"></a>
## Enum: ElfensterheberEnum
Name | Value
---- | -----
Y | &quot;Y&quot;
N | &quot;N&quot;


<a name="KlimaanlageEnum"></a>
## Enum: KlimaanlageEnum
Name | Value
---- | -----
Y | &quot;Y&quot;
N | &quot;N&quot;


<a name="EspEnum"></a>
## Enum: EspEnum
Name | Value
---- | -----
Y | &quot;Y&quot;
N | &quot;N&quot;


<a name="SitzheizungEnum"></a>
## Enum: SitzheizungEnum
Name | Value
---- | -----
Y | &quot;Y&quot;
N | &quot;N&quot;


<a name="RadiomitcdEnum"></a>
## Enum: RadiomitcdEnum
Name | Value
---- | -----
Y | &quot;Y&quot;
N | &quot;N&quot;


<a name="AbgaspartikelfilterEnum"></a>
## Enum: AbgaspartikelfilterEnum
Name | Value
---- | -----
Y | &quot;Y&quot;
N | &quot;N&quot;


<a name="AllradantriebEnum"></a>
## Enum: AllradantriebEnum
Name | Value
---- | -----
Y | &quot;Y&quot;
N | &quot;N&quot;


<a name="LeichtmetallfelgenEnum"></a>
## Enum: LeichtmetallfelgenEnum
Name | Value
---- | -----
Y | &quot;Y&quot;
N | &quot;N&quot;


<a name="NavigationssystemEnum"></a>
## Enum: NavigationssystemEnum
Name | Value
---- | -----
Y | &quot;Y&quot;
N | &quot;N&quot;


<a name="ParkdistancecontrolEnum"></a>
## Enum: ParkdistancecontrolEnum
Name | Value
---- | -----
Y | &quot;Y&quot;
N | &quot;N&quot;


<a name="TempomatEnum"></a>
## Enum: TempomatEnum
Name | Value
---- | -----
Y | &quot;Y&quot;
N | &quot;N&quot;


<a name="AbsEnum"></a>
## Enum: AbsEnum
Name | Value
---- | -----
Y | &quot;Y&quot;
N | &quot;N&quot;


<a name="AnhaengerkupplungEnum"></a>
## Enum: AnhaengerkupplungEnum
Name | Value
---- | -----
Y | &quot;Y&quot;
N | &quot;N&quot;


<a name="KlimaautomatikEnum"></a>
## Enum: KlimaautomatikEnum
Name | Value
---- | -----
Y | &quot;Y&quot;
N | &quot;N&quot;


<a name="LederausstattungEnum"></a>
## Enum: LederausstattungEnum
Name | Value
---- | -----
Y | &quot;Y&quot;
N | &quot;N&quot;


<a name="SeitenairbagsEnum"></a>
## Enum: SeitenairbagsEnum
Name | Value
---- | -----
Y | &quot;Y&quot;
N | &quot;N&quot;


<a name="UnfallfahrzeugEnum"></a>
## Enum: UnfallfahrzeugEnum
Name | Value
---- | -----
Y | &quot;Y&quot;
N | &quot;N&quot;


<a name="XenonscheinwerferEnum"></a>
## Enum: XenonscheinwerferEnum
Name | Value
---- | -----
Y | &quot;Y&quot;
N | &quot;N&quot;


<a name="ZombiEnum"></a>
## Enum: ZombiEnum
Name | Value
---- | -----
Y | &quot;Y&quot;
N | &quot;N&quot;


<a name="KopiertEnum"></a>
## Enum: KopiertEnum
Name | Value
---- | -----
Y | &quot;Y&quot;
N | &quot;N&quot;


<a name="QuelleEnum"></a>
## Enum: QuelleEnum
Name | Value
---- | -----
ELN | &quot;eln&quot;
DETAIL | &quot;detail&quot;
DEALER | &quot;dealer&quot;


<a name="DatenschutzOkEnum"></a>
## Enum: DatenschutzOkEnum
Name | Value
---- | -----
Y | &quot;Y&quot;
N | &quot;N&quot;



