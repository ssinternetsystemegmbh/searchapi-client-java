

# VehicleSelectionListVehicleRemoveDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealerId** | **Long** | The unique identifier of the dealer. | 
**listId** | **Long** | The unique identifier of the vehicle selection list. | 
**vehicles** | [**VehicleSusuuidList**](VehicleSusuuidList.md) |  | 



