

# VehicleDetailsRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vehicleIds** | **List&lt;String&gt;** |  |  [optional]



