

# ChangePasswordWithSessionIdResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**emailAddress** | **String** | Email Address the change password information was sent. | 
**dateDone** | **String** | Timestamp of the entry. | 
**doneBy** | **String** | Principal of the entry (eg. SusAdmin, PWForgotSite) | 
**message** | **String** | Further information. |  [optional]



