

# DealerCarparkInternSearchResultItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**susuid** | **String** |  |  [optional]
**dateParked** | **String** |  |  [optional]
**offerdealerId** | **Integer** |  |  [optional]
**offernumberOriginal** | **String** |  |  [optional]
**dateOccurrence** | **String** |  |  [optional]
**dateRecurrence** | **String** |  |  [optional]
**manufacturerId** | **Integer** |  |  [optional]
**manufacturer** | **String** |  |  [optional]
**seriestypeId** | **Integer** |  |  [optional]
**seriestype** | **String** |  |  [optional]
**versionOriginal** | **String** |  |  [optional]



