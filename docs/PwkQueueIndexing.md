

# PwkQueueIndexing

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**principal** | **String** |  |  [optional]
**distributorId** | **Integer** |  |  [optional]
**numResellers** | **Integer** |  |  [optional]
**cntReseller** | **Integer** |  |  [optional]
**numChunks** | **Integer** |  |  [optional]
**cntChunks** | **Integer** |  |  [optional]
**dateCreated** | [**DateTime**](DateTime.md) |  |  [optional]
**dateUpdated** | [**DateTime**](DateTime.md) |  |  [optional]
**indexingFinished** | **Boolean** |  |  [optional]
**mediaProcessingActive** | **Boolean** |  |  [optional]
**dateVehicleMediaIndexing** | [**DateTime**](DateTime.md) |  |  [optional]
**mtsDateTerminated** | [**DateTime**](DateTime.md) |  |  [optional]
**type** | [**PwkQueueIndexingType**](PwkQueueIndexingType.md) |  | 
**status** | [**PwkQueueIndexingStatus**](PwkQueueIndexingStatus.md) |  | 
**comment** | **String** |  |  [optional]



