

# VehicleInternResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vehicles** | [**List&lt;VehicleIntern&gt;**](VehicleIntern.md) |  |  [optional]



