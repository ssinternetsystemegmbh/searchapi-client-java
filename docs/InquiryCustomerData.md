

# InquiryCustomerData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**company** | **String** | The Customer Company if purchase_type &#x3D; &#39;geschaeftlich&#39; |  [optional]
**salutation** | [**SalutationEnum**](#SalutationEnum) | The CustomerSalutation as String, use Herr, Frau, Divers, Firma, Familie, keine, If nothing matches, &#39;keine&#39; is assumed |  [optional]
**name** | **String** | The Lastname as String limited by 50 digits [required] | 
**firstname** | **String** | The firstname as String limited by 50 digits |  [optional]
**street** | **String** | The Customer Resident Street and House number as String limited by 50 digits [required] | 
**postcode** | **String** | The Postcode as String, limited by 10 digits [required] | 
**city** | **String** | The City as String limited by 50 digits [required] | 
**country** | **String** | The Country as String, limited by 50 digits [required] | 
**telefonDaytime** | **String** | The Phone number at daytime as String, limited by 50 digits [required] | 
**telefonEvening** | **String** | The Phone number at evening as String, limited by 50 digits |  [optional]
**email** | **String** | The Customer Email Address, as String limited by 50 digits [required] | 
**fax** | **String** | The fax number as String, limited by 50 digits |  [optional]
**age** | **String** | The Customer age as String |  [optional]
**profession** | **String** | The Customer profession as String, limited by 50 digits |  [optional]
**purchaseType** | [**PurchaseTypeEnum**](#PurchaseTypeEnum) | The desired purchase Type as PurchasingTyp &#39;privat, geschaeftlich&#39; [required] |  [optional]
**privacyProtection** | **Boolean** | The Customer accept our privacy protection rules, true or false mandatory, only agreed Request will be stored and processed. | 



## Enum: SalutationEnum

Name | Value
---- | -----
HERR | &quot;Herr&quot;
FRAU | &quot;Frau&quot;
DIVERS | &quot;Divers&quot;
FIRMA | &quot;Firma&quot;
FAMILIE | &quot;Familie&quot;
KEINE | &quot;keine&quot;



## Enum: PurchaseTypeEnum

Name | Value
---- | -----
PRIVAT | &quot;privat&quot;
GESCHAEFTLICH | &quot;geschaeftlich&quot;



