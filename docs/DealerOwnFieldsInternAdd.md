

# DealerOwnFieldsInternAdd

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealerOwnFieldsInternAddItem** | [**DealerOwnFieldsInternAddItem**](DealerOwnFieldsInternAddItem.md) |  |  [optional]



