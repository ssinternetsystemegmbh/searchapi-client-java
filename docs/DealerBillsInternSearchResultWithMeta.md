

# DealerBillsInternSearchResultWithMeta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**MetaResponse**](MetaResponse.md) |  |  [optional]
**response** | [**DealerBillsInternSearchResult**](DealerBillsInternSearchResult.md) |  |  [optional]



