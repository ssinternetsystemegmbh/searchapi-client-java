

# DealerSearchResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hitsFound** | [**Object**](.md) |  |  [optional]
**hits** | [**List&lt;DealerSearchHit&gt;**](DealerSearchHit.md) |  |  [optional]



