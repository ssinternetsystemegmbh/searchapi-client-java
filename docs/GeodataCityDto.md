

# GeodataCityDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**zipcode** | **String** |  |  [optional]
**city** | **String** |  |  [optional]
**district** | **String** |  |  [optional]



