

# ModelResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**models** | [**List&lt;ModelDto&gt;**](ModelDto.md) |  |  [optional]



