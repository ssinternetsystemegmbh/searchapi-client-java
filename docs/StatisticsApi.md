# StatisticsApi

All URIs are relative to *https://develop-api.ssis.de:8443/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getMedienzuweisungTasksInfo**](StatisticsApi.md#getMedienzuweisungTasksInfo) | **GET** /admin/statistics/medienzuweisungtasks | Retrieves statistics for the Medienzuweisung Tasks.
[**getPwkIndexingQueueInfo**](StatisticsApi.md#getPwkIndexingQueueInfo) | **GET** /admin/statistics/pwkqueueindexing | Retrieves statistics for the PWK Queue Indexing.


<a name="getMedienzuweisungTasksInfo"></a>
# **getMedienzuweisungTasksInfo**
> MedienzuweisungTasksStatisticsDtoWithMeta getMedienzuweisungTasksInfo()

Retrieves statistics for the Medienzuweisung Tasks.

Retrieves statistics for the Medienzuweisung Tasks.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.StatisticsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    StatisticsApi apiInstance = new StatisticsApi(defaultClient);
    try {
      MedienzuweisungTasksStatisticsDtoWithMeta result = apiInstance.getMedienzuweisungTasksInfo();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling StatisticsApi#getMedienzuweisungTasksInfo");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**MedienzuweisungTasksStatisticsDtoWithMeta**](MedienzuweisungTasksStatisticsDtoWithMeta.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | Statistics of Medienzuweisung Tasks. |  -  |

<a name="getPwkIndexingQueueInfo"></a>
# **getPwkIndexingQueueInfo**
> PwkQueueIndexingStatisticsDtoWithMeta getPwkIndexingQueueInfo()

Retrieves statistics for the PWK Queue Indexing.

Retrieves statistics for the PWK Queue Indexing.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.StatisticsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    StatisticsApi apiInstance = new StatisticsApi(defaultClient);
    try {
      PwkQueueIndexingStatisticsDtoWithMeta result = apiInstance.getPwkIndexingQueueInfo();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling StatisticsApi#getPwkIndexingQueueInfo");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**PwkQueueIndexingStatisticsDtoWithMeta**](PwkQueueIndexingStatisticsDtoWithMeta.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | Statistics of PWK Queue Indexing. |  -  |

