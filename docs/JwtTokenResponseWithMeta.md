

# JwtTokenResponseWithMeta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**MetaResponse**](MetaResponse.md) |  |  [optional]
**response** | [**JWTTokenResponse**](JWTTokenResponse.md) |  |  [optional]



