

# UserNewPasswordResponseWithMeta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**MetaResponse**](MetaResponse.md) |  |  [optional]
**response** | [**UserNewPasswordResponse**](UserNewPasswordResponse.md) |  |  [optional]



