

# AuthUserListResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**users** | [**List&lt;AuthUserDto&gt;**](AuthUserDto.md) |  |  [optional]



