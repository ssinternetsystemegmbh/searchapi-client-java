

# DirectInquiry

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customerData** | [**CustomerInquiryData**](CustomerInquiryData.md) |  |  [optional]
**detailData** | [**List&lt;DirectInquiryDetail&gt;**](DirectInquiryDetail.md) |  |  [optional]



