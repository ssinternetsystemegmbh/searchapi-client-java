

# CountryIdentifier

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**licensePlateCode** | **String** |  |  [optional]
**isoCode** | **String** |  |  [optional]
**translation** | **String** |  |  [optional]



