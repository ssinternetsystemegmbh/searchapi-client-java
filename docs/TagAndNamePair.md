

# TagAndNamePair

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tag** | **String** | The tag of the item. |  [optional]
**name** | **String** | The name of the item. |  [optional]
**count** | **Long** | Quantity of the values of item. |  [optional]



