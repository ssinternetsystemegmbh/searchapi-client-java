# VehiclesInternApi

All URIs are relative to *https://develop-api.ssis.de:8443/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**detailsVehicleIntern**](VehiclesInternApi.md#detailsVehicleIntern) | **POST** /admin/vehiclesintern/{dealerId}/details | Get Vehicle details for internal.
[**searchVehicleIntern**](VehiclesInternApi.md#searchVehicleIntern) | **POST** /admin/vehiclesintern/{dealerId}/search | Search vehicles for internal.


<a name="detailsVehicleIntern"></a>
# **detailsVehicleIntern**
> VehicleInternResponseWithMeta detailsVehicleIntern(dealerId, vehicleDetailsRequest)

Get Vehicle details for internal.

Get Vehicle details for internal.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.VehiclesInternApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    VehiclesInternApi apiInstance = new VehiclesInternApi(defaultClient);
    Integer dealerId = 56; // Integer | 
    VehicleDetailsRequest vehicleDetailsRequest = new VehicleDetailsRequest(); // VehicleDetailsRequest | A list of susuuids
    try {
      VehicleInternResponseWithMeta result = apiInstance.detailsVehicleIntern(dealerId, vehicleDetailsRequest);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling VehiclesInternApi#detailsVehicleIntern");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dealerId** | **Integer**|  |
 **vehicleDetailsRequest** | [**VehicleDetailsRequest**](VehicleDetailsRequest.md)| A list of susuuids |

### Return type

[**VehicleInternResponseWithMeta**](VehicleInternResponseWithMeta.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json;charset=utf-8
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | An array of Vehicle details. |  -  |

<a name="searchVehicleIntern"></a>
# **searchVehicleIntern**
> VehicleInternSearchResultWithMeta searchVehicleIntern(dealerId, vehicleSearchIntern)

Search vehicles for internal.

Search vehicles for internal.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.VehiclesInternApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    VehiclesInternApi apiInstance = new VehiclesInternApi(defaultClient);
    Integer dealerId = 56; // Integer | 
    VehicleSearchIntern vehicleSearchIntern = new VehicleSearchIntern(); // VehicleSearchIntern | Vehicle search filters.
    try {
      VehicleInternSearchResultWithMeta result = apiInstance.searchVehicleIntern(dealerId, vehicleSearchIntern);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling VehiclesInternApi#searchVehicleIntern");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dealerId** | **Integer**|  |
 **vehicleSearchIntern** | [**VehicleSearchIntern**](VehicleSearchIntern.md)| Vehicle search filters. | [optional]

### Return type

[**VehicleInternSearchResultWithMeta**](VehicleInternSearchResultWithMeta.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json;charset=utf-8
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | An array of vehicleIds. |  -  |

