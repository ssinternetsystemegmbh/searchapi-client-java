

# Error

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rule** | **String** |  |  [optional]
**key** | **String** |  |  [optional]
**message** | **String** |  |  [optional]
**rejected** | **Boolean** |  |  [optional]
**fields** | **Map&lt;String, String&gt;** |  |  [optional]



