

# VehicleSusuuidList

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**susuuids** | **List&lt;String&gt;** | A list of SUSUUIDs uniquely identifying the associated vehicles. | 



