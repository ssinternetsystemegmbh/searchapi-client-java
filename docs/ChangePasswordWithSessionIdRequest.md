

# ChangePasswordWithSessionIdRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **String** | session id | 
**principal** | **String** | Principal of the request (eg. SusAdmin, PWForgotSite) | 
**password** | **String** | The new password. |  [optional]
**userMetadata** | **String** | The request user metadata incl. ip, browser etc. |  [optional]



