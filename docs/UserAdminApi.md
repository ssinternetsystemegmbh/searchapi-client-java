# UserAdminApi

All URIs are relative to *https://develop-api.ssis.de:8443/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteDealerAuthUser**](UserAdminApi.md#deleteDealerAuthUser) | **DELETE** /prevapi/admin/dealer/{dealerId}/user/{userId} | delete an dealer user.
[**getDealerAuthUser**](UserAdminApi.md#getDealerAuthUser) | **GET** /prevapi/admin/dealer/{dealerId}/user/{userId} | get an dealer user.
[**getDealerAuthUserRoles**](UserAdminApi.md#getDealerAuthUserRoles) | **GET** /prevapi/admin/dealer/user/roles | Get all possible dealer user roles.
[**getDealerAuthUsers**](UserAdminApi.md#getDealerAuthUsers) | **GET** /prevapi/admin/dealer/{dealerId}/user | get all auth users of one dealer.
[**saveDealerAuthUser**](UserAdminApi.md#saveDealerAuthUser) | **POST** /prevapi/admin/dealer/{dealerId}/user | save an dealer user.


<a name="deleteDealerAuthUser"></a>
# **deleteDealerAuthUser**
> CommonResponseWithMeta deleteDealerAuthUser(dealerId, userId)

delete an dealer user.

delete an dealer user.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.UserAdminApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    UserAdminApi apiInstance = new UserAdminApi(defaultClient);
    Long dealerId = 56L; // Long | 
    Long userId = 56L; // Long | 
    try {
      CommonResponseWithMeta result = apiInstance.deleteDealerAuthUser(dealerId, userId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling UserAdminApi#deleteDealerAuthUser");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dealerId** | **Long**|  |
 **userId** | **Long**|  |

### Return type

[**CommonResponseWithMeta**](CommonResponseWithMeta.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |
**401** | Username or password wrong. |  -  |
**0** | Other error responses. |  -  |

<a name="getDealerAuthUser"></a>
# **getDealerAuthUser**
> DealerAuthUserSingleResponseWithMeta getDealerAuthUser(dealerId, userId)

get an dealer user.

get an dealer user.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.UserAdminApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    UserAdminApi apiInstance = new UserAdminApi(defaultClient);
    Long dealerId = 56L; // Long | 
    Long userId = 56L; // Long | 
    try {
      DealerAuthUserSingleResponseWithMeta result = apiInstance.getDealerAuthUser(dealerId, userId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling UserAdminApi#getDealerAuthUser");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dealerId** | **Long**|  |
 **userId** | **Long**|  |

### Return type

[**DealerAuthUserSingleResponseWithMeta**](DealerAuthUserSingleResponseWithMeta.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |
**401** | Username or password wrong. |  -  |
**0** | Other error responses. |  -  |

<a name="getDealerAuthUserRoles"></a>
# **getDealerAuthUserRoles**
> DealerAuthRoleListResponseWithMeta getDealerAuthUserRoles()

Get all possible dealer user roles.

Get all possible dealer user roles.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.UserAdminApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    UserAdminApi apiInstance = new UserAdminApi(defaultClient);
    try {
      DealerAuthRoleListResponseWithMeta result = apiInstance.getDealerAuthUserRoles();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling UserAdminApi#getDealerAuthUserRoles");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**DealerAuthRoleListResponseWithMeta**](DealerAuthRoleListResponseWithMeta.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |
**401** | Username or password wrong. |  -  |
**0** | Other error responses. |  -  |

<a name="getDealerAuthUsers"></a>
# **getDealerAuthUsers**
> DealerAuthUserListResponseWithMeta getDealerAuthUsers(dealerId)

get all auth users of one dealer.

get all auth users of one dealer.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.UserAdminApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    UserAdminApi apiInstance = new UserAdminApi(defaultClient);
    Long dealerId = 56L; // Long | 
    try {
      DealerAuthUserListResponseWithMeta result = apiInstance.getDealerAuthUsers(dealerId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling UserAdminApi#getDealerAuthUsers");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dealerId** | **Long**|  |

### Return type

[**DealerAuthUserListResponseWithMeta**](DealerAuthUserListResponseWithMeta.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |
**401** | Username or password wrong. |  -  |
**0** | Other error responses. |  -  |

<a name="saveDealerAuthUser"></a>
# **saveDealerAuthUser**
> DealerAuthUserSaveResponseWithMeta saveDealerAuthUser(dealerId, dealerAuthUserRequest)

save an dealer user.

UserAdminApi.saveDealerAuthUser(DealerAuthUserRequest dealerAuthUserRequest)

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.UserAdminApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    UserAdminApi apiInstance = new UserAdminApi(defaultClient);
    Long dealerId = 56L; // Long | 
    DealerAuthUserRequest dealerAuthUserRequest = new DealerAuthUserRequest(); // DealerAuthUserRequest | User data
    try {
      DealerAuthUserSaveResponseWithMeta result = apiInstance.saveDealerAuthUser(dealerId, dealerAuthUserRequest);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling UserAdminApi#saveDealerAuthUser");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dealerId** | **Long**|  |
 **dealerAuthUserRequest** | [**DealerAuthUserRequest**](DealerAuthUserRequest.md)| User data |

### Return type

[**DealerAuthUserSaveResponseWithMeta**](DealerAuthUserSaveResponseWithMeta.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json;charset=utf-8
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |
**401** | Username or password wrong. |  -  |
**0** | Other error responses. |  -  |

