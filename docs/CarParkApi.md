# CarParkApi

All URIs are relative to *https://develop-api.ssis.de:8443/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**carpark**](CarParkApi.md#carpark) | **POST** /carpark/{dealerId}/sendemail | Send email with cars from carpark.


<a name="carpark"></a>
# **carpark**
> DefaultResponse carpark(dealerId, sendCarkparkEmail)

Send email with cars from carpark.

Send email with cars from carpark.

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.CarParkApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    CarParkApi apiInstance = new CarParkApi(defaultClient);
    Integer dealerId = 56; // Integer | 
    SendCarkparkEmail sendCarkparkEmail = new SendCarkparkEmail(); // SendCarkparkEmail | A list of susuuids
    try {
      DefaultResponse result = apiInstance.carpark(dealerId, sendCarkparkEmail);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling CarParkApi#carpark");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dealerId** | **Integer**|  |
 **sendCarkparkEmail** | [**SendCarkparkEmail**](SendCarkparkEmail.md)| A list of susuuids |

### Return type

[**DefaultResponse**](DefaultResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json;charset=utf-8
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | An array of Vehicle details. |  -  |

