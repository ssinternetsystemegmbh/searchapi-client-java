

# DealerBillsInternDetailsResultWithMeta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**MetaResponse**](MetaResponse.md) |  |  [optional]
**response** | [**DealerBillsInternDetailsResult**](DealerBillsInternDetailsResult.md) |  |  [optional]



