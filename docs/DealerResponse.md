

# DealerResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealers** | [**List&lt;Dealer&gt;**](Dealer.md) |  |  [optional]



