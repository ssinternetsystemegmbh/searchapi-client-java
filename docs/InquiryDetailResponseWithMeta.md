

# InquiryDetailResponseWithMeta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**MetaResponse**](MetaResponse.md) |  |  [optional]
**response** | [**InquiryDetailResponse**](InquiryDetailResponse.md) |  |  [optional]



