

# MakeResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**makes** | [**List&lt;MakeDto&gt;**](MakeDto.md) |  |  [optional]



