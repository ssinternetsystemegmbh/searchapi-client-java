

# CustomerInquiryData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**language** | **Integer** |  |  [optional]
**customerId** | **String** |  |  [optional]
**salutation** | [**SalutationEnum**](#SalutationEnum) |  |  [optional]
**name** | **String** | max. length 50 chars | 
**firstname** | **String** | max. length 50 chars |  [optional]
**street** | **String** |  |  [optional]
**postcode** | **String** |  |  [optional]
**city** | **String** |  |  [optional]
**country** | **String** |  |  [optional]
**telefonDaytime** | **String** |  |  [optional]
**telefonEvening** | **String** |  |  [optional]
**email** | **String** | max. length 50 chars | 
**fax** | **String** |  |  [optional]
**age** | **String** |  |  [optional]
**profession** | **String** |  |  [optional]
**purchaseType** | [**PurchaseTypeEnum**](#PurchaseTypeEnum) |  |  [optional]
**company** | **String** |  |  [optional]
**customerIdOld** | **String** |  |  [optional]
**copied** | **Boolean** |  |  [optional]
**privacyProtection** | **Boolean** | The Customer accept our privacy protection rules, true or false mandatory, only agreed Request will be stored and processed. | 
**dateCreated** | **String** |  |  [optional]
**dateModified** | **String** |  |  [optional]



## Enum: SalutationEnum

Name | Value
---- | -----
HERR | &quot;Herr&quot;
FRAU | &quot;Frau&quot;
DIVERS | &quot;Divers&quot;
FIRMA | &quot;Firma&quot;
FAMILIE | &quot;Familie&quot;
KEINE | &quot;keine&quot;



## Enum: PurchaseTypeEnum

Name | Value
---- | -----
PRIVAT | &quot;privat&quot;
GESCHAEFTLICH | &quot;geschaeftlich&quot;



