

# DealerVehicleManufacturerResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealerVehicleManufacturers** | [**List&lt;DealerVehicleManufacturer&gt;**](DealerVehicleManufacturer.md) | A list of dealers with their available vehicle manufacturers. |  [optional]



