

# ProductOptionSearch

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** |  |  [optional]
**value** | **String** |  |  [optional]
**dateValidFrom** | **String** |  |  [optional]



