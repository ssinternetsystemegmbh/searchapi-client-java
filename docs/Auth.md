
# Auth

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **String** |  |  [optional]
**password** | **String** |  |  [optional]
**roles** | **List&lt;String&gt;** |  |  [optional]



