

# Warranty

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**durationMonths** | **Integer** |  |  [optional]
**price** | [**BigDecimal**](BigDecimal.md) |  |  [optional]



