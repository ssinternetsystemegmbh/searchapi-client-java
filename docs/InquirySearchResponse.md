

# InquirySearchResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**inquiryId** | **Long** | The ID of the inquiry. |  [optional]
**inquiryType** | **String** | Inquiry type: &#39;Direkte Suchanfrage&#39;,&#39;Weiterleitung&#39;,&#39;Freie Suchanfrage&#39;,&#39;Unbekannt&#39; |  [optional]
**customerSalutation** | **String** | Salutation of the customer. |  [optional]
**customerSurname** | **String** | Lastname of the customer. |  [optional]
**customerForename** | **String** | Firstname of the customer. |  [optional]
**fullName** | **String** | Composite name of the customer. |  [optional]
**vehicleMake** | **String** | Make of the vehicle. |  [optional]
**vehicleModel** | **String** | Model of the vehicle. |  [optional]
**vehicleSeries** | **String** | Series of the vehicle. |  [optional]
**fullVehicle** | **String** | Composite of the vehicle. |  [optional]
**isProcessed** | **Boolean** | Inquiry is processed. |  [optional]
**isSeen** | **Boolean** | Inquiry is seen. |  [optional]
**isSent** | **Boolean** | Inquiry is sent. |  [optional]
**dateCreated** | **String** | Created date of the inquiry. |  [optional]



