

# VehicleInternSearchResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hitsFound** | **Long** |  |  [optional]
**aggregations** | [**Map&lt;String, List&lt;VehicleSearchTagObject&gt;&gt;**](List.md) |  |  [optional]
**hits** | [**List&lt;VehicleSearchHitIntern&gt;**](VehicleSearchHitIntern.md) |  |  [optional]



