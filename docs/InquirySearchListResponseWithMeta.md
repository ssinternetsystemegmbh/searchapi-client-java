

# InquirySearchListResponseWithMeta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**MetaResponse**](MetaResponse.md) |  |  [optional]
**response** | [**InquirySearchListResponse**](InquirySearchListResponse.md) |  |  [optional]



