

# VehicleResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vehicles** | [**List&lt;Vehicle&gt;**](Vehicle.md) |  |  [optional]



