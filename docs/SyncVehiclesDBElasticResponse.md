

# SyncVehiclesDBElasticResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numDBVehicles** | **Integer** |  |  [optional]
**numESVehicles** | **Long** |  |  [optional]
**numDBVehiclesNotInES** | **Integer** |  |  [optional]
**numESVehiclesNotInDB** | **Integer** |  |  [optional]
**vehicleInfos** | [**List&lt;VehicleInfo&gt;**](VehicleInfo.md) |  |  [optional]
**messages** | **List&lt;String&gt;** |  |  [optional]



