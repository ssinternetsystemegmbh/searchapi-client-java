

# DealerVehicleManufacturer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealerId** | **Integer** | The dealer ID. |  [optional]
**manufacturers** | [**List&lt;TagAndNamePair&gt;**](TagAndNamePair.md) | A list of vehicle manufacturers. |  [optional]



