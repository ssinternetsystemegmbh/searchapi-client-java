

# CronJobInternListResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cronJobNames** | **List&lt;String&gt;** |  |  [optional]



