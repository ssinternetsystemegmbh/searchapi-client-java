

# DealerInformationInternAdd

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tag** | **String** | Only one tag entry is allowed. |  [optional]
**header** | **String** |  |  [optional]
**content** | **String** |  |  [optional]



