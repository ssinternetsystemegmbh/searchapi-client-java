

# DirectInquiryIntern

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customerData** | [**InquiryCustomerData**](InquiryCustomerData.md) |  |  [optional]
**detailData** | [**List&lt;DirectInquiryDetail&gt;**](DirectInquiryDetail.md) |  |  [optional]



