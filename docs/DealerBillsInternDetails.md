

# DealerBillsInternDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**billIds** | **List&lt;Integer&gt;** | A list of bill_Ids |  [optional]



