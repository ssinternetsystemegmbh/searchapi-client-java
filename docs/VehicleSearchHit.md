

# VehicleSearchHit

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vehicleId** | **String** |  |  [optional]
**dateModified** | **String** |  |  [optional]
**vehicleType** | **String** |  |  [optional]



