

# Price

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**TypeEnum**](#TypeEnum) |  |  [optional]
**value** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**vatRate** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**vatIncluded** | **Boolean** |  |  [optional]
**currencyIso** | **String** |  |  [optional]
**currencySymbol** | **String** |  |  [optional]



## Enum: TypeEnum

Name | Value
---- | -----
RECOMMENDED | &quot;RECOMMENDED&quot;
ORIGINAL | &quot;ORIGINAL&quot;
TO_DEALER | &quot;TO_DEALER&quot;
TO_CONSUMER | &quot;TO_CONSUMER&quot;



