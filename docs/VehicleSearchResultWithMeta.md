

# VehicleSearchResultWithMeta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**MetaResponse**](MetaResponse.md) |  |  [optional]
**response** | [**VehicleSearchResult**](VehicleSearchResult.md) |  |  [optional]



