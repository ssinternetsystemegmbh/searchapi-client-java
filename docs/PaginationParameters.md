

# PaginationParameters

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**resultCount** | **Integer** |  |  [optional]
**resultOffset** | **Integer** |  |  [optional]
**sortType** | **String** |  |  [optional]
**sortOrderASC** | **Boolean** |  |  [optional]



