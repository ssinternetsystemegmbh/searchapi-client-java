

# DealerAuthRoleListResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**roles** | [**List&lt;DealerAuthRoleDto&gt;**](DealerAuthRoleDto.md) |  |  [optional]



