

# FuelType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tag** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**unit** | **String** |  |  [optional]
**cost** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**isConcrete** | **Boolean** |  |  [optional]
**concreteType** | [**FuelType**](FuelType.md) |  |  [optional]



