

# ClientRegistration

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**updateCount** | **Map&lt;String, Object&gt;** |  |  [optional]
**tokenEndpointAuthMethod** | **String** |  |  [optional]
**clientName** | **String** |  |  [optional]
**grantTypes** | **List&lt;String&gt;** |  |  [optional]
**applicationType** | **String** |  |  [optional]
**redirectUris** | **List&lt;String&gt;** |  |  [optional]
**clientUri** | **String** |  |  [optional]
**logoUri** | **String** |  |  [optional]
**resourceUris** | **List&lt;String&gt;** |  |  [optional]
**responseTypes** | **List&lt;String&gt;** |  |  [optional]
**contacts** | **List&lt;String&gt;** |  |  [optional]
**policyUri** | **String** |  |  [optional]
**tosUri** | **String** |  |  [optional]
**scope** | **String** |  |  [optional]



