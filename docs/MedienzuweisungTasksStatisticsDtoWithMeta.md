

# MedienzuweisungTasksStatisticsDtoWithMeta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**MetaResponse**](MetaResponse.md) |  |  [optional]
**response** | [**MedienzuweisungTasksStatisticsDto**](MedienzuweisungTasksStatisticsDto.md) |  |  [optional]



