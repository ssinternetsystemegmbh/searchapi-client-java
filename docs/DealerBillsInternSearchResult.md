

# DealerBillsInternSearchResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealerId** | **Integer** |  |  [optional]
**billCounter** | **Integer** |  |  [optional]
**items** | [**List&lt;DealerBillsInternSearchResultItem&gt;**](DealerBillsInternSearchResultItem.md) |  |  [optional]



