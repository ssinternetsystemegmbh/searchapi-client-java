

# PwkQueueIndexingDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** | Unique identifier of the indexing entry. |  [optional]
**principal** | **String** | Principal name associated with the entry. |  [optional]
**distributorId** | **Integer** | Identifier of the distributor associated with the entry. |  [optional]
**numResellers** | **Integer** | Number of resellers associated with the indexing. |  [optional]
**cntReseller** | **Integer** | Count of resellers processed. |  [optional]
**numChunks** | **Integer** | Number of chunks to be indexed. |  [optional]
**cntChunks** | **Integer** | Number of chunks processed. |  [optional]
**dateCreated** | [**DateTime**](DateTime.md) | Timestamp for when the entry was created. |  [optional]
**dateUpdated** | [**DateTime**](DateTime.md) | Timestamp for the last update of the entry. |  [optional]
**indexingFinished** | **Boolean** | Indicator whether indexing is finished. |  [optional]
**mediaProcessingActive** | **Boolean** | Indicator if media processing is active. |  [optional]
**dateVehicleMediaIndexing** | [**DateTime**](DateTime.md) | Timestamp for vehicle media indexing. |  [optional]
**mtsDateTerminated** | [**DateTime**](DateTime.md) | Timestamp when the task was terminated by the MTS. |  [optional]
**type** | **String** | Type of indexing for the entry. |  [optional]
**status** | **String** | Current status of the indexing entry. |  [optional]
**comment** | **String** | Optional comment or additional information. |  [optional]



