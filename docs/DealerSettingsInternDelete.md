

# DealerSettingsInternDelete

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List&lt;DealerSettingsInternDeleteItem&gt;**](DealerSettingsInternDeleteItem.md) | A list of settings_keys |  [optional]



