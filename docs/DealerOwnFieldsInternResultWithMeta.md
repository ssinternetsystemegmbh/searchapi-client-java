

# DealerOwnFieldsInternResultWithMeta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**MetaResponse**](MetaResponse.md) |  |  [optional]
**response** | [**DealerOwnFieldsInternResult**](DealerOwnFieldsInternResult.md) |  |  [optional]



