

# GeodataCityResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**geodataCityDtos** | [**List&lt;GeodataCityDto&gt;**](GeodataCityDto.md) |  |  [optional]



