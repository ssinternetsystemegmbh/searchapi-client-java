

# PwkQueueIndexingStatus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**status** | **String** |  |  [optional]
**description** | **String** |  |  [optional]



