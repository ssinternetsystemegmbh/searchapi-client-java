

# DealerAuthUserRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** | Set the user ID if you want to modify, else set it to null or 0. |  [optional]
**username** | **String** | The login username of the user. | 
**password** | **String** | The login password of the user. | 
**email** | **String** | The unique email address of the user. | 
**roles** | **List&lt;String&gt;** | The roles of the user. | 
**enabled** | **Boolean** | Set if the user is allowed to login. | 



