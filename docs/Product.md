

# Product

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tag** | **String** |  |  [optional]
**label** | **String** |  |  [optional]
**isStarted** | **Boolean** |  |  [optional]
**isCancelled** | **Boolean** |  |  [optional]
**isBlocked** | **Boolean** |  |  [optional]
**isTestphase** | **Boolean** |  |  [optional]
**productOptions** | [**List&lt;ProductOption&gt;**](ProductOption.md) |  |  [optional]



