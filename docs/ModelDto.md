

# ModelDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tag** | **String** | The unique TAG. |  [optional]
**value** | **String** | The model name. |  [optional]
**exotic** | **Boolean** | Defines whether a model is exotic. |  [optional]



