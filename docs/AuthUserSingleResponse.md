

# AuthUserSingleResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user** | [**AuthUserDto**](AuthUserDto.md) |  |  [optional]



