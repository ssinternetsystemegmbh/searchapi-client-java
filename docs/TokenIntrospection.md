

# TokenIntrospection

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active** | **Boolean** |  |  [optional]
**scope** | **String** |  |  [optional]
**clientId** | **String** |  |  [optional]
**username** | **String** |  |  [optional]
**tokenType** | **String** |  |  [optional]
**iat** | **Long** |  |  [optional]
**exp** | **Long** |  |  [optional]
**nbf** | **Long** |  |  [optional]
**sub** | **String** |  |  [optional]
**aud** | **List&lt;String&gt;** |  |  [optional]
**iss** | **String** |  |  [optional]
**jti** | **String** |  |  [optional]
**extensions** | **Map&lt;String, String&gt;** |  |  [optional]



