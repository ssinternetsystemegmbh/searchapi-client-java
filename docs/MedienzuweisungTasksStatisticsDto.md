

# MedienzuweisungTasksStatisticsDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numEntries** | **Integer** | The total number of media assignment tasks |  [optional]
**toOldestEntrySeconds** | **Long** | The duration in seconds to the oldest task entry |  [optional]
**oldestMedienzuweisungTask** | [**MedienzuweisungTasksDto**](MedienzuweisungTasksDto.md) |  |  [optional]



