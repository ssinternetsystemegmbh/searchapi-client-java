

# VehicleInternResponseWithMeta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**MetaResponse**](MetaResponse.md) |  |  [optional]
**response** | [**VehicleInternResponse**](VehicleInternResponse.md) |  |  [optional]



