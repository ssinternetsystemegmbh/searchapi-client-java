

# DealerAuthUserSaveResponseWithMeta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**MetaResponse**](MetaResponse.md) |  |  [optional]
**response** | [**DealerAuthUserSaveResponse**](DealerAuthUserSaveResponse.md) |  |  [optional]



