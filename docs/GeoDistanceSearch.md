

# GeoDistanceSearch

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**geolocation** | [**Geolocation**](Geolocation.md) |  |  [optional]
**zip** | **String** |  |  [optional]
**radius** | **Integer** |  |  [optional]
**calcDistance** | **Boolean** |  |  [optional]



