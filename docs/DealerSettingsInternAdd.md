

# DealerSettingsInternAdd

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**settingsKey** | **String** | Only one settings entry is allowed. |  [optional]
**settingsValue** | **String** | Blank settings value is also allowed. |  [optional]



