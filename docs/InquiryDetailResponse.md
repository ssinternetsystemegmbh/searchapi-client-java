

# InquiryDetailResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customer** | [**CustomerInquiryData**](CustomerInquiryData.md) |  |  [optional]
**anfrage** | [**Anfrage**](Anfrage.md) |  |  [optional]
**status** | [**InquiryStatus**](InquiryStatus.md) |  |  [optional]



