

# VehicleSelectionListDtoListResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vehicleSelectionListDtos** | [**List&lt;VehicleSelectionListDto&gt;**](VehicleSelectionListDto.md) | A list of VehicleSelectionListDto objects, each containing details of a specific vehicle selection list. | 



