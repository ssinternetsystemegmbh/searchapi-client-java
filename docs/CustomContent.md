

# CustomContent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** |  |  [optional]
**tag** | **String** |  |  [optional]
**header** | **String** |  |  [optional]
**content** | **String** |  |  [optional]



