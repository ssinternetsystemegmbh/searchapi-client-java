

# AccessTokenValidation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**initialValidationSuccessful** | **Boolean** |  |  [optional]
**clientId** | **String** |  |  [optional]
**clientIpAddress** | **String** |  |  [optional]
**clientSubject** | [**UserSubject**](UserSubject.md) |  |  [optional]
**tokenKey** | **String** |  |  [optional]
**tokenType** | **String** |  |  [optional]
**tokenGrantType** | **String** |  |  [optional]
**tokenIssuedAt** | **Long** |  |  [optional]
**tokenLifetime** | **Long** |  |  [optional]
**tokenIssuer** | **String** |  |  [optional]
**tokenSubject** | [**UserSubject**](UserSubject.md) |  |  [optional]
**tokenScopes** | [**List&lt;OAuthPermission&gt;**](OAuthPermission.md) |  |  [optional]
**audiences** | **List&lt;String&gt;** |  |  [optional]
**clientCodeVerifier** | **String** |  |  [optional]
**extraProps** | **Map&lt;String, String&gt;** |  |  [optional]
**clientConfidential** | **Boolean** |  |  [optional]



