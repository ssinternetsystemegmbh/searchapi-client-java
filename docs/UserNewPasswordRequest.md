

# UserNewPasswordRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **String** | username | 
**principal** | **String** | Principal of the request (eg. SusAdmin, PWForgotSite) | 
**userMetadata** | **String** | The request user metadata incl. ip, browser etc. |  [optional]



