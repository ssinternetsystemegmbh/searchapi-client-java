

# FreeInquiryVehicleData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hersteller** | **String** | The Manufacturer as String |  [optional]
**baureihe** | **String** | The model range as String |  [optional]
**modell** | **String** | The version as String |  [optional]
**typ** | **String** | The type as String |  [optional]
**ausfuehrung** | **String** | The Completion sa String |  [optional]
**motorart** | **String** | the Engine type as String |  [optional]
**hubraum** | **String** | the Engine size as String |  [optional]
**kwMin** | **Integer** | The minimum of kilowatt as Integer |  [optional]
**kwMax** | **Integer** | The maximum of kilowatt as Integer |  [optional]
**psMin** | **Integer** | The minimum of horsepower as Integer |  [optional]
**psMax** | **Integer** | The maximum of horsepower as Integer |  [optional]
**farbwunsch** | **String** | The desired Color as String |  [optional]
**gebraucht** | [**GebrauchtEnum**](#GebrauchtEnum) | Used Flag as Y(Yes)/N(No) Choice |  [optional]
**vonBaujahr** | **String** | from Build year as String |  [optional]
**bisBaujahr** | **String** | to Build year as String |  [optional]
**maxLaufleistung** | **String** | The max mileage as String |  [optional]
**schiebedach** | [**SchiebedachEnum**](#SchiebedachEnum) | Sunroof Flag as Y(Yes)/N(No) Choice |  [optional]
**zentralverriegelung** | [**ZentralverriegelungEnum**](#ZentralverriegelungEnum) | Central locking Flag as Y(Yes)/N(No) Choice |  [optional]
**elfensterheber** | [**ElfensterheberEnum**](#ElfensterheberEnum) | Electric windows Flag as Y(Yes)/N(No) Choice |  [optional]
**getriebeart** | **String** | Gearbox typ as String |  [optional]
**klimaanlage** | [**KlimaanlageEnum**](#KlimaanlageEnum) | air conditioner Flag as Y(Yes)/N(No) Choice |  [optional]
**esp** | [**EspEnum**](#EspEnum) | esp Flag as Y(Yes)/N(No) Choice |  [optional]
**sitzheizung** | [**SitzheizungEnum**](#SitzheizungEnum) | seat heater Flag as Y(Yes)/N(No) Choice |  [optional]
**radiomitcd** | [**RadiomitcdEnum**](#RadiomitcdEnum) | Radio with CD Flag as Y(Yes)/N(No) Choice |  [optional]
**abgaspartikelfilter** | [**AbgaspartikelfilterEnum**](#AbgaspartikelfilterEnum) | Exhaust Filter Flag as Y(Yes)/N(No) Choice |  [optional]
**allradantrieb** | [**AllradantriebEnum**](#AllradantriebEnum) | 4wd Flag as Y(Yes)/N(No) Choice |  [optional]
**leichtmetallfelgen** | [**LeichtmetallfelgenEnum**](#LeichtmetallfelgenEnum) | alloy wheels Flag as Y(Yes)/N(No) Choice |  [optional]
**navigationssystem** | [**NavigationssystemEnum**](#NavigationssystemEnum) | GPS Flag as Y(Yes)/N(No) Choice |  [optional]
**parkdistancecontrol** | [**ParkdistancecontrolEnum**](#ParkdistancecontrolEnum) | Parkdistancecontrol Flag as Y(Yes)/N(No) Choice |  [optional]
**tempomat** | [**TempomatEnum**](#TempomatEnum) | Cruise control Flag as Y(Yes)/N(No) Choice |  [optional]
**abs** | [**AbsEnum**](#AbsEnum) | ABS Flag as Y(Yes)/N(No) Choice |  [optional]
**anhaengerkupplung** | [**AnhaengerkupplungEnum**](#AnhaengerkupplungEnum) | coupling device Flag as Y(Yes)/N(No) Choice |  [optional]
**klimaautomatik** | [**KlimaautomatikEnum**](#KlimaautomatikEnum) | Klimatautomatik Flag as Y(Yes)/N(No) Choice |  [optional]
**lederausstattung** | [**LederausstattungEnum**](#LederausstattungEnum) | leather equipment Flag as Y(Yes)/N(No) Choice |  [optional]
**seitenairbags** | [**SeitenairbagsEnum**](#SeitenairbagsEnum) | side airbag Flag as Y(Yes)/N(No) Choice |  [optional]
**unfallfahrzeug** | [**UnfallfahrzeugEnum**](#UnfallfahrzeugEnum) | Crashed Vehicle Flag as Y(Yes)/N(No) Choice |  [optional]
**xenonscheinwerfer** | [**XenonscheinwerferEnum**](#XenonscheinwerferEnum) | Xenon Headlight Flag as Y(Yes)/N(No) Choice |  [optional]
**eufahrzeuge** | [**EufahrzeugeEnum**](#EufahrzeugeEnum) | EU Fahrzeuge J/N Choice Flag. |  [optional]
**kaufzeitraum** | **String** | Purchasing period |  [optional]
**verbrauchStadt** | **Double** | consumption city |  [optional]
**verbrauchAussen** | **Double** | consumption outside |  [optional]
**verbrauchGesamt** | **Double** | consumption total |  [optional]
**verbrauchStrom** | **Double** | consumption electricity |  [optional]
**co2Emission** | **Double** | CO2 Emission |  [optional]
**co2Effizienz** | **String** | CO2 Efficiency |  [optional]
**zombi** | [**ZombiEnum**](#ZombiEnum) | Zombi |  [optional]
**angebotsnr** | **String** | Offer number as String |  [optional]
**serienzubehoer** | **String** | The Serial Equipment as formated String |  [optional]
**aufpreiszubehoer** | **String** | The surcharge Equipment as formated String |  [optional]
**preis** | **Double** | The Price as Double |  [optional]
**preisZubehoer** | **Double** | The summarized Equipment Price as Double |  [optional]
**preisobergrenze** | **Double** | The max Price as Double |  [optional]
**versandStatus** | **String** | The delivery Status Flag as String |  [optional]
**bemerkung** | **String** | notice as String |  [optional]
**sonstiges** | **String** | miscellaneous as String |  [optional]
**quelle** | [**QuelleEnum**](#QuelleEnum) | Source as InquirySource - eln, detail, dealer |  [optional]
**waehrung** | **String** | Currency as String |  [optional]
**mwstfaktor** | **Double** | Sales Tax Faktor as Double |  [optional]
**datenschutzOk** | **Boolean** | Data protection Flag as Boolean |  [optional]



## Enum: GebrauchtEnum

Name | Value
---- | -----
J | &quot;J&quot;
N | &quot;N&quot;



## Enum: SchiebedachEnum

Name | Value
---- | -----
J | &quot;J&quot;
N | &quot;N&quot;



## Enum: ZentralverriegelungEnum

Name | Value
---- | -----
J | &quot;J&quot;
N | &quot;N&quot;



## Enum: ElfensterheberEnum

Name | Value
---- | -----
J | &quot;J&quot;
N | &quot;N&quot;



## Enum: KlimaanlageEnum

Name | Value
---- | -----
J | &quot;J&quot;
N | &quot;N&quot;



## Enum: EspEnum

Name | Value
---- | -----
J | &quot;J&quot;
N | &quot;N&quot;



## Enum: SitzheizungEnum

Name | Value
---- | -----
J | &quot;J&quot;
N | &quot;N&quot;



## Enum: RadiomitcdEnum

Name | Value
---- | -----
J | &quot;J&quot;
N | &quot;N&quot;



## Enum: AbgaspartikelfilterEnum

Name | Value
---- | -----
J | &quot;J&quot;
N | &quot;N&quot;



## Enum: AllradantriebEnum

Name | Value
---- | -----
J | &quot;J&quot;
N | &quot;N&quot;



## Enum: LeichtmetallfelgenEnum

Name | Value
---- | -----
J | &quot;J&quot;
N | &quot;N&quot;



## Enum: NavigationssystemEnum

Name | Value
---- | -----
J | &quot;J&quot;
N | &quot;N&quot;



## Enum: ParkdistancecontrolEnum

Name | Value
---- | -----
J | &quot;J&quot;
N | &quot;N&quot;



## Enum: TempomatEnum

Name | Value
---- | -----
J | &quot;J&quot;
N | &quot;N&quot;



## Enum: AbsEnum

Name | Value
---- | -----
J | &quot;J&quot;
N | &quot;N&quot;



## Enum: AnhaengerkupplungEnum

Name | Value
---- | -----
J | &quot;J&quot;
N | &quot;N&quot;



## Enum: KlimaautomatikEnum

Name | Value
---- | -----
J | &quot;J&quot;
N | &quot;N&quot;



## Enum: LederausstattungEnum

Name | Value
---- | -----
J | &quot;J&quot;
N | &quot;N&quot;



## Enum: SeitenairbagsEnum

Name | Value
---- | -----
J | &quot;J&quot;
N | &quot;N&quot;



## Enum: UnfallfahrzeugEnum

Name | Value
---- | -----
J | &quot;J&quot;
N | &quot;N&quot;



## Enum: XenonscheinwerferEnum

Name | Value
---- | -----
J | &quot;J&quot;
N | &quot;N&quot;



## Enum: EufahrzeugeEnum

Name | Value
---- | -----
J | &quot;J&quot;
N | &quot;N&quot;



## Enum: ZombiEnum

Name | Value
---- | -----
J | &quot;J&quot;
N | &quot;N&quot;



## Enum: QuelleEnum

Name | Value
---- | -----
ELN | &quot;eln&quot;
DETAIL | &quot;detail&quot;
DEALER | &quot;dealer&quot;



