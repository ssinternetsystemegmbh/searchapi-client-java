

# DealerOwnFieldsInternResultItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**thrId** | **Integer** | Dealer Id |  [optional]
**lsnId** | **Integer** | Language Id |  [optional]
**pdftextAktiv** | **Boolean** | Field &lt;B&gt;pdftext_aktiv&lt;/B&gt; will be changed only to \&quot;TRUE\&quot;, if this endpoint request field is \&quot;TRUE\&quot; .&lt;BR&gt;&lt;font color&#x3D;red&gt;Otherwise {by &lt;B&gt;missing field or a NULL value&lt;/B&gt; } it will be set to \&quot;FALSE\&quot;!&lt;/font&gt; |  [optional]
**pdftext** | **String** | Field &lt;B&gt;pdftext&lt;/B&gt; will be changed only, if this endpoint request field is given  &lt;font color&#x3D;red&gt;and the field value isn&#39;t EMPTY!&lt;/font&gt; |  [optional]
**pdftextVerbindlich** | **String** | Field &lt;B&gt;pdftextVerbindlich&lt;/B&gt; will be changed only, if this endpoint request field is given  &lt;font color&#x3D;red&gt;and the field value isn&#39;t EMPTY!&lt;/font&gt; |  [optional]
**shpFahrzeugangebote** | **String** | Field &lt;B&gt;shpFahrzeugangebote&lt;/B&gt; will be changed, if this endpoint request field is given.  &lt;font color&#x3D;darkgreen&gt;EMPTY value is also allowed&lt;/font&gt; |  [optional]
**shpMobile** | **String** | Field &lt;B&gt;shpMobile&lt;/B&gt; will be changed, if this endpoint request field is given.  &lt;font color&#x3D;darkgreen&gt;EMPTY value is also allowed&lt;/font&gt; |  [optional]
**shpAutoscout** | **String** | Field &lt;B&gt;shpAutoscout&lt;/B&gt; will be changed, if this endpoint request field is given.  &lt;font color&#x3D;darkgreen&gt;EMPTY value is also allowed&lt;/font&gt; |  [optional]
**suchmaskeUeber1Aktiv** | **Boolean** | Field &lt;B&gt;suchmaskeUeber1Aktiv&lt;/B&gt; will be set depends on user value of &lt;B&gt;suchmaskeUeber1&lt;/B&gt; |  [optional]
**suchmaskeUeber1** | **String** | Field &lt;B&gt;suchmaskeUeber1&lt;/B&gt; will be set only, if this endpoint request field is given.&lt;BR&gt;&lt;font color&#x3D;red&gt;An EMPTY field value is also allowed and this deactivated the field &lt;B&gt;suchmaskeUeber1Aktiv&lt;B&gt; &lt;/font&gt;&lt;BR&gt; |  [optional]
**suchmaskeUeber2Aktiv** | **Boolean** | Field &lt;B&gt;suchmaskeUeber2Aktiv&lt;/B&gt; will be set depends on user value of &lt;B&gt;suchmaskeUeber2&lt;/B&gt; |  [optional]
**suchmaskeUeber2** | **String** | Field &lt;B&gt;suchmaskeUeber2&lt;/B&gt; will be set only, if this endpoint request field is given.&lt;BR&gt;&lt;font color&#x3D;red&gt;An EMPTY field value is also allowed and this deactivated the field &lt;B&gt;suchmaskeUeber2Aktiv&lt;B&gt; &lt;/font&gt;&lt;BR&gt; |  [optional]
**suchmaskeUeber3Aktiv** | **Boolean** | Field &lt;B&gt;suchmaskeUeber3Aktiv&lt;/B&gt; will be set depends on user value of &lt;B&gt;suchmaskeUeber3&lt;/B&gt; |  [optional]
**suchmaskeUeber3** | **String** | Field &lt;B&gt;suchmaskeUeber3&lt;/B&gt; will be set only, if this endpoint request field is given.&lt;BR&gt;&lt;font color&#x3D;red&gt;An EMPTY field value is also allowed and this deactivated the field &lt;B&gt;suchmaskeUeber3Aktiv&lt;B&gt; &lt;/font&gt;&lt;BR&gt; |  [optional]
**detailseiteUeber1Aktiv** | **Boolean** | Field &lt;B&gt;detailseiteUeber1Aktiv&lt;/B&gt; will be set depends on user value of &lt;B&gt;detailseiteUeber1&lt;/B&gt; |  [optional]
**detailseiteUeber1** | **String** | Field &lt;B&gt;detailseiteUeber1&lt;/B&gt; will be set only, if this endpoint request field is given.&lt;BR&gt;&lt;font color&#x3D;red&gt;An EMPTY field value is also allowed and this deactivated the field &lt;B&gt;detailseiteUeber1Aktiv&lt;B&gt; &lt;/font&gt;&lt;BR&gt; |  [optional]
**detailseiteUeber2Aktiv** | **Boolean** | Field &lt;B&gt;detailseiteUeber2Aktiv&lt;/B&gt; will be set depends on user value of &lt;B&gt;detailseiteUeber2&lt;/B&gt; |  [optional]
**detailseiteUeber2** | **String** | Field &lt;B&gt;detailseiteUeber2&lt;/B&gt; will be set only, if this endpoint request field is given.&lt;BR&gt;&lt;font color&#x3D;red&gt;An EMPTY field value is also allowed and this deactivated the field &lt;B&gt;detailseiteUeber2Aktiv&lt;B&gt; &lt;/font&gt;&lt;BR&gt; |  [optional]
**detailseiteUeber1DruckAktiv** | **Boolean** | Field &lt;B&gt;detailseiteUeber1DruckAktiv&lt;/B&gt; will be set depends on user value of &lt;B&gt;detailseiteUeber1Druck&lt;/B&gt; |  [optional]
**detailseiteUeber1Druck** | **String** | Field &lt;B&gt;detailseiteUeber1Druck&lt;/B&gt; will be set only, if this endpoint request field is given.&lt;BR&gt;&lt;font color&#x3D;red&gt;An EMPTY field value is also allowed and this deactivated the field &lt;B&gt;detailseiteUeber1DruckAktiv&lt;B&gt; &lt;/font&gt;&lt;BR&gt; |  [optional]
**detailseiteUeber2DruckAktiv** | **Boolean** | Field &lt;B&gt;detailseiteUeber2DruckAktiv&lt;/B&gt; will be set depends on user value of &lt;B&gt;detailseiteUeber2Druck&lt;/B&gt; |  [optional]
**detailseiteUeber2Druck** | **String** | Field &lt;B&gt;detailseiteUeber2Druck&lt;/B&gt; will be set only, if this endpoint request field is given.&lt;BR&gt;&lt;font color&#x3D;red&gt;An EMPTY field value is also allowed and this deactivated the field &lt;B&gt;detailseiteUeber2DruckAktiv&lt;B&gt; &lt;/font&gt;&lt;BR&gt; |  [optional]
**bestellformularUeber1Aktiv** | **Boolean** | Field &lt;B&gt;bestellformularUeber1Aktiv&lt;/B&gt; will be set depends on user value of &lt;B&gt;bestellformularUeber1&lt;/B&gt; |  [optional]
**bestellformularUeber1** | **String** | Field &lt;B&gt;bestellformularUeber1&lt;/B&gt; will be set only, if this endpoint request field is given.&lt;BR&gt;&lt;font color&#x3D;red&gt;An EMPTY field value is also allowed and this deactivated the field &lt;B&gt;bestellformularUeber1Aktiv&lt;B&gt; &lt;/font&gt;&lt;BR&gt; |  [optional]
**bestellformularUeber2Aktiv** | **Boolean** | Field &lt;B&gt;bestellformularUeber2Aktiv&lt;/B&gt; will be set depends on user value of &lt;B&gt;bestellformularUeber2&lt;/B&gt; |  [optional]
**bestellformularUeber2** | **String** | Field &lt;B&gt;bestellformularUeber2&lt;/B&gt; will be set only, if this endpoint request field is given.&lt;BR&gt;&lt;font color&#x3D;red&gt;An EMPTY field value is also allowed and this deactivated the field &lt;B&gt;bestellformularUeber2Aktiv&lt;B&gt; &lt;/font&gt;&lt;BR&gt; |  [optional]



