

# MakeDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tag** | **String** | The unique TAG used e.g. for the models endpoint. |  [optional]
**value** | **String** | The make name. |  [optional]
**exotic** | **Boolean** | Defines whether a make is exotic. |  [optional]



