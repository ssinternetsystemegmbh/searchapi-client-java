

# DealerDetailsRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealerIds** | **List&lt;Integer&gt;** |  |  [optional]



