# GeodataApi

All URIs are relative to *https://develop-api.ssis.de:8443/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getGeodataCityByZipAndCity**](GeodataApi.md#getGeodataCityByZipAndCity) | **GET** /geodata/city | Get geodata of the passed zipcode and city. (For internal issues)


<a name="getGeodataCityByZipAndCity"></a>
# **getGeodataCityByZipAndCity**
> GeodataCityResponseWithMeta getGeodataCityByZipAndCity(zipcode, city)

Get geodata of the passed zipcode and city. (For internal issues)

Get geodata of the passed zipcode and city. (For internal issues)

### Example
```java
// Import classes:
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.handler.Configuration;
import de.ssis.api.client.swagger.handler.auth.*;
import de.ssis.api.client.swagger.handler.models.*;
import de.ssis.api.client.swagger.api.GeodataApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://develop-api.ssis.de:8443/api");
    
    // Configure HTTP basic authorization: basicAuth
    HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
    basicAuth.setUsername("YOUR USERNAME");
    basicAuth.setPassword("YOUR PASSWORD");

    GeodataApi apiInstance = new GeodataApi(defaultClient);
    String zipcode = "zipcode_example"; // String | 
    String city = "city_example"; // String | 
    try {
      GeodataCityResponseWithMeta result = apiInstance.getGeodataCityByZipAndCity(zipcode, city);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling GeodataApi#getGeodataCityByZipAndCity");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **zipcode** | **String**|  | [optional]
 **city** | **String**|  | [optional]

### Return type

[**GeodataCityResponseWithMeta**](GeodataCityResponseWithMeta.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | Returns a list of geodata. |  -  |

