# SSiS API Client JAVA

This is the search client for our API. The base was created with the openAPI (Swagger 3.0) generator. See [openAPI-Readme](README_OAPI.md).

To simplify the usage, we have created a wrapper class "[de.ssis.api.client.Client](docs/ssis/Client.md)".

Furthermore, the resource classes [DealersApi](docs/DealersApi.md) and [VehiclesApi](docs/VehiclesApi.md) are also wrapped by [DealersResource](docs/ssis/DealersResource.md) and [VehiclesResource](docs/ssis/VehiclesResource.md).

Here you find the configuration: 

You find some examples how to use the [Client](docs/ssis/Client.md) as JUnit tests [de.ssis.api.client.ClientTest](src/test/java/de/ssis/api/client/ClientTest.java).

## Documentation for Wrapper Classes

 - [Client](docs/ssis/Client.md)
 - [DealersResource](docs/ssis/DealersResource.md)
 - [VehiclesResource](docs/ssis/VehiclesResource.md)
 
 
 
 Our [ELN Search-API Documentation](https://api.ssis.de:8443/api/) you can find [here](https://api.ssis.de:8443/api/). 
 