package de.ssis.api.client;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.model.DealerDetailsRequest;
import de.ssis.api.client.swagger.model.DealerResponse;
import de.ssis.api.client.swagger.model.ModelSearch;
import de.ssis.api.client.swagger.model.PaginationParameters;
import de.ssis.api.client.swagger.model.TagSearch;
import de.ssis.api.client.swagger.model.TagSearchResult;
import de.ssis.api.client.swagger.model.Vehicle;
import de.ssis.api.client.swagger.model.VehicleDetailsRequest;
import de.ssis.api.client.swagger.model.VehicleResponse;
import de.ssis.api.client.swagger.model.VehicleSearch;
import de.ssis.api.client.swagger.model.VehicleSearchHit;
import de.ssis.api.client.swagger.model.VehicleSearchResult;

public class ClientTest {
	//@Test
	public void testGetClientVersion() {
		Client client = new Client();
		
		System.out.println("Client Version: " + client.getClientVersion());
	}
	
	//@Test
	public void testDealerDetails() {
		try {
			Client client = new Client();
			
			List<Integer> dealerIds = new ArrayList<>();
			dealerIds.add(0000);
			
			DealerDetailsRequest dealerDetailsRequest = client.getDealerDetailsRequest();
			dealerDetailsRequest.setDealerIds(dealerIds);
			
			DealerResponse dealerResponse = client.getDealerDetails(dealerDetailsRequest);
			
			System.out.println(dealerResponse.toString());
		} catch (ApiException e) {
			System.out.println(e.getResponseBody());
		}
	}
	
	//@Test
	public void testVehicleAggregationTags() throws Exception {
		try {
			Client client = new Client();
			
			List<String> aggregationTypes = new ArrayList<>();
			aggregationTypes.add(Client.AGG_TYPE_MANUFACTURER);
			
			TagSearch tagSearch = client.getVehicleTagSearch();
			tagSearch.setAggregationTypes(aggregationTypes);
			
			TagSearchResult tagSearchResult = client.getVehicleAggregationTags(tagSearch);
			
			System.out.println(tagSearchResult.toString());
		} catch (ApiException e) {
			System.out.println(e.getResponseBody());
		}
	}
	
	//@Test
	public void testVehicleAggregationTagsWithCount() throws ApiException {
		Client client = new Client();
		client.setPagParamResultCount(0);
		
		List<String> aggregationTypes = new ArrayList<>();
		aggregationTypes.add(Client.AGG_TYPE_MANUFACTURER);
		
		VehicleSearch vehicleSearch = client.getVehicleSearch();
		vehicleSearch.setAggregationTypes(aggregationTypes);
		
		VehicleSearchResult vehicleSearchResult = client.getVehicleSearchResult(vehicleSearch);
		
		System.out.println(vehicleSearchResult.toString());
	}
	
	@Test
	public void testVehicleSearch() throws ApiException {
		this.getVehicles();
	}
	
	private VehicleSearchResult getVehicles() throws ApiException {
		VehicleSearchResult vehicleSearchResult = null;
		try {
			Client client = new Client();
			
			client.setPagParamResultCount(5);
			
			ModelSearch modelSearch = new ModelSearch();
			modelSearch.setManufacturerTag("VOLVO");
			
			List<ModelSearch> lModelSearch = new ArrayList<>();
			lModelSearch.add(modelSearch);
			
			
			VehicleSearch vehicleSearch = client.getVehicleSearch();
			vehicleSearch.setModelSearch(lModelSearch);
			
			vehicleSearchResult = client.getVehicleSearchResult(vehicleSearch, 6844);
			
			System.out.println(vehicleSearchResult.toString());
		} catch (ApiException e) {
			System.out.println(e.getResponseBody().toString());
		}
		return vehicleSearchResult;
	}
	
	//@Test
	public void testVehicleDetails() throws ApiException {
		Client client = new Client();
		
		List<String> vehicleIds = new ArrayList<>();
		
		vehicleIds.add("fff67db88b8b4e5be991bfdcdcd937a1");
		VehicleDetailsRequest vehicleDetailsRequest = client.getVehicleDetailsRequest();
		vehicleDetailsRequest.setVehicleIds(vehicleIds);
		
		VehicleResponse vehicleResponse = client.getVehicleDetails(vehicleDetailsRequest);
		
		System.out.println(vehicleResponse.toString());
	}
	
	//@Test
	public void testMetadata() throws Exception {
		Client client = new Client();
		
		//** This block only needs to test the metatags response **/
		List<String> aggregationTypes = new ArrayList<>();
		aggregationTypes.add(Client.AGG_TYPE_MANUFACTURER);
		
		TagSearch tagSearch = client.getVehicleTagSearch();
		tagSearch.setAggregationTypes(aggregationTypes);
		
		TagSearchResult tagSearchResult = client.getVehicleAggregationTags(tagSearch);
		
		System.out.println(tagSearchResult.toString());
		//**********************************************/
		
		System.out.println(client.getLastMetadata());
	}
	
	//@Test
	public void testCntVehicleDetails() {
		try {
			int dealerId = 0;
			
			Client client = new Client();
			client.setDealerId(dealerId);
			
			VehicleSearch vehicleSearch = client.getVehicleSearch();
			PaginationParameters paginationParameters = new PaginationParameters();
			paginationParameters.setResultCount(100);
			
			List<String> vehicleIds = new ArrayList<>();
			
			VehicleSearchResult vehicleSearchResult = null;
			int resCounter = 0;
			
			do {
				paginationParameters.setResultOffset(resCounter);
		    	vehicleSearch.setPaginationParameters(paginationParameters);
		    	vehicleSearchResult = client.getVehicleSearchResult(vehicleSearch, dealerId);
		    	
		    	for (VehicleSearchHit veHit : vehicleSearchResult.getHits()) {
					vehicleIds.add(veHit.getVehicleId());
				}
		    	
		    	resCounter += vehicleSearchResult.getHits().size();
		    	System.out.println("resCounter:");
			    System.out.println(resCounter);
		    } while (resCounter < Float.parseFloat(vehicleSearchResult.getHitsFound().toString()));
			
		    System.out.println("Anzahl Vehicle IDs:");
		    System.out.println(vehicleIds.size());
		    System.out.println("Anzahl Vehicle IDs nach CUT:");
		    System.out.println(vehicleIds.size());
		    
		    VehicleDetailsRequest vehicleDetailsRequest = client.getVehicleDetailsRequest();
		    
		    int fromIndex = 0;
		    int toIndex = 50;
		    
		    VehicleResponse vehicleResponse = null;
		    int hitCounterDetails = 0;
		    
		    List<String> notFoundIds = new ArrayList<>(vehicleIds);
		    
		    while (toIndex <= vehicleIds.size() && fromIndex < toIndex) {
		    	vehicleDetailsRequest.setVehicleIds(vehicleIds.subList(fromIndex, toIndex));
		    	System.out.println("Detail Run from index " + fromIndex + " till index " + toIndex + ":");
			    System.out.println(vehicleIds.subList(fromIndex, toIndex).toString());
		    	vehicleResponse = client.getVehicleDetails(vehicleDetailsRequest);
		    	hitCounterDetails += vehicleResponse.getVehicles().size();
		    	
		    	for (Vehicle vehicle : vehicleResponse.getVehicles()) {
					notFoundIds.remove(vehicle.getSusuuid());
					
					if (vehicle.getPrices().size() == 0) {
						System.out.println("Kein Preis für vehicle " + vehicle.getSusuuid() + " gefunden.");
					}
				}
		    	
		    	fromIndex += 50;
		    	
		    	System.out.println("Anzahl verbleibend:");
			    System.out.println((vehicleIds.size() - hitCounterDetails));
			    
		    	if (vehicleIds.size() - hitCounterDetails < 50) {
		    		toIndex += (vehicleIds.size() - hitCounterDetails);
		    	} else {
		    		toIndex += 50;
		    	}
		    	
			};
			
		    System.out.println("Anzahl Vehicle Details:");
		    System.out.println(hitCounterDetails);
		    System.out.println("Nicht gefundene IDs:");
		    System.out.println(notFoundIds.toString());
		    System.out.println("META:");
		    System.out.println(client.getLastMetadata().toString());
		} catch (ApiException e) {
			System.out.println(e.getResponseBody().toString());
		}
	}
}
