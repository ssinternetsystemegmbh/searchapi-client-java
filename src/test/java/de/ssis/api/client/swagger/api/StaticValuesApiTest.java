/*
 * ELN Search-API DEV
 * <b>Build Time: 2025-03-06T14:15:01.313+0100</b><br><br>Host: null<br>IP: SuSApp/127.0.1.1
 *
 * The version of the OpenAPI document: 1.1.2559
 * Contact: technik@eln.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package de.ssis.api.client.swagger.api;

import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.model.MakeResponseWithMeta;
import de.ssis.api.client.swagger.model.ModelResponseWithMeta;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for StaticValuesApi
 */
@Ignore
public class StaticValuesApiTest {

    private final StaticValuesApi api = new StaticValuesApi();

    
    /**
     * Get makes.
     *
     * Get a list of vehicle makes.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getMakesTest() throws ApiException {
        MakeResponseWithMeta response = api.getMakes();

        // TODO: test validations
    }
    
    /**
     * Get models of the passed make.
     *
     * Get models of the passed make.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getModelsByMakeTest() throws ApiException {
        String make = null;
        ModelResponseWithMeta response = api.getModelsByMake(make);

        // TODO: test validations
    }
    
}
