package de.ssis.api.client.handler;

import java.util.List;
import java.util.Map;

import de.ssis.api.client.swagger.handler.ApiException;

public class ClientApiException extends ApiException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6101577286948397692L;

	public ClientApiException() {
		// TODO Auto-generated constructor stub
	}

	public ClientApiException(Throwable throwable) {
		super(throwable);
		// TODO Auto-generated constructor stub
	}

	public ClientApiException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ClientApiException(String message, Throwable throwable, int code, Map<String, List<String>> responseHeaders,
			String responseBody) {
		super(message, throwable, code, responseHeaders, responseBody);
		// TODO Auto-generated constructor stub
	}

	public ClientApiException(String message, int code, Map<String, List<String>> responseHeaders,
			String responseBody) {
		super(message, code, responseHeaders, responseBody);
		// TODO Auto-generated constructor stub
	}

	public ClientApiException(String message, Throwable throwable, int code,
			Map<String, List<String>> responseHeaders) {
		super(message, throwable, code, responseHeaders);
		// TODO Auto-generated constructor stub
	}

	public ClientApiException(int code, Map<String, List<String>> responseHeaders, String responseBody) {
		super(code, responseHeaders, responseBody);
		// TODO Auto-generated constructor stub
	}

	public ClientApiException(int code, String message) {
		super(code, message);
		// TODO Auto-generated constructor stub
	}

	public ClientApiException(int code, String message, Map<String, List<String>> responseHeaders,
			String responseBody) {
		super(code, message, responseHeaders, responseBody);
		// TODO Auto-generated constructor stub
	}

}
