/*
 * ELN Search-API DEV
 * <b>Build Time: 2025-03-06T14:15:01.313+0100</b><br><br>Host: null<br>IP: SuSApp/127.0.1.1
 *
 * The version of the OpenAPI document: 1.1.2559
 * Contact: technik@eln.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package de.ssis.api.client.swagger.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * Only one entry is allowed.
 */
@ApiModel(description = "Only one entry is allowed.")

public class DealerOwnFieldsInternAddItem {
  public static final String SERIALIZED_NAME_PDFTEXT_AKTIV = "pdftextAktiv";
  @SerializedName(SERIALIZED_NAME_PDFTEXT_AKTIV)
  private Boolean pdftextAktiv;

  public static final String SERIALIZED_NAME_PDFTEXT = "pdftext";
  @SerializedName(SERIALIZED_NAME_PDFTEXT)
  private String pdftext;

  public static final String SERIALIZED_NAME_PDFTEXT_VERBINDLICH = "pdftextVerbindlich";
  @SerializedName(SERIALIZED_NAME_PDFTEXT_VERBINDLICH)
  private String pdftextVerbindlich;

  public static final String SERIALIZED_NAME_SHP_FAHRZEUGANGEBOTE = "shpFahrzeugangebote";
  @SerializedName(SERIALIZED_NAME_SHP_FAHRZEUGANGEBOTE)
  private String shpFahrzeugangebote;

  public static final String SERIALIZED_NAME_SHP_MOBILE = "shpMobile";
  @SerializedName(SERIALIZED_NAME_SHP_MOBILE)
  private String shpMobile;

  public static final String SERIALIZED_NAME_SHP_AUTOSCOUT = "shpAutoscout";
  @SerializedName(SERIALIZED_NAME_SHP_AUTOSCOUT)
  private String shpAutoscout;

  public static final String SERIALIZED_NAME_SUCHMASKE_UEBER1 = "suchmaskeUeber1";
  @SerializedName(SERIALIZED_NAME_SUCHMASKE_UEBER1)
  private String suchmaskeUeber1;

  public static final String SERIALIZED_NAME_SUCHMASKE_UEBER2 = "suchmaskeUeber2";
  @SerializedName(SERIALIZED_NAME_SUCHMASKE_UEBER2)
  private String suchmaskeUeber2;

  public static final String SERIALIZED_NAME_SUCHMASKE_UEBER3 = "suchmaskeUeber3";
  @SerializedName(SERIALIZED_NAME_SUCHMASKE_UEBER3)
  private String suchmaskeUeber3;

  public static final String SERIALIZED_NAME_DETAILSEITE_UEBER1 = "detailseiteUeber1";
  @SerializedName(SERIALIZED_NAME_DETAILSEITE_UEBER1)
  private String detailseiteUeber1;

  public static final String SERIALIZED_NAME_DETAILSEITE_UEBER2 = "detailseiteUeber2";
  @SerializedName(SERIALIZED_NAME_DETAILSEITE_UEBER2)
  private String detailseiteUeber2;

  public static final String SERIALIZED_NAME_DETAILSEITE_UEBER1_DRUCK = "detailseiteUeber1Druck";
  @SerializedName(SERIALIZED_NAME_DETAILSEITE_UEBER1_DRUCK)
  private String detailseiteUeber1Druck;

  public static final String SERIALIZED_NAME_DETAILSEITE_UEBER2_DRUCK = "detailseiteUeber2Druck";
  @SerializedName(SERIALIZED_NAME_DETAILSEITE_UEBER2_DRUCK)
  private String detailseiteUeber2Druck;

  public static final String SERIALIZED_NAME_BESTELLFORMULAR_UEBER1 = "bestellformularUeber1";
  @SerializedName(SERIALIZED_NAME_BESTELLFORMULAR_UEBER1)
  private String bestellformularUeber1;

  public static final String SERIALIZED_NAME_BESTELLFORMULAR_UEBER2 = "bestellformularUeber2";
  @SerializedName(SERIALIZED_NAME_BESTELLFORMULAR_UEBER2)
  private String bestellformularUeber2;


  public DealerOwnFieldsInternAddItem pdftextAktiv(Boolean pdftextAktiv) {
    
    this.pdftextAktiv = pdftextAktiv;
    return this;
  }

   /**
   * Field &lt;B&gt;pdftext_aktiv&lt;/B&gt; will be changed only to \&quot;TRUE\&quot;, if this endpoint request field is \&quot;TRUE\&quot; .&lt;BR&gt;&lt;font color&#x3D;red&gt;Otherwise {by &lt;B&gt;missing field or a NULL value&lt;/B&gt; } it will be set to \&quot;FALSE\&quot;!&lt;/font&gt;
   * @return pdftextAktiv
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Field <B>pdftext_aktiv</B> will be changed only to \"TRUE\", if this endpoint request field is \"TRUE\" .<BR><font color=red>Otherwise {by <B>missing field or a NULL value</B> } it will be set to \"FALSE\"!</font>")

  public Boolean getPdftextAktiv() {
    return pdftextAktiv;
  }



  public void setPdftextAktiv(Boolean pdftextAktiv) {
    this.pdftextAktiv = pdftextAktiv;
  }


  public DealerOwnFieldsInternAddItem pdftext(String pdftext) {
    
    this.pdftext = pdftext;
    return this;
  }

   /**
   * Field &lt;B&gt;pdftext&lt;/B&gt; will be changed only, if this endpoint request field is given  &lt;font color&#x3D;red&gt;and the field value isn&#39;t EMPTY!&lt;/font&gt;
   * @return pdftext
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Field <B>pdftext</B> will be changed only, if this endpoint request field is given  <font color=red>and the field value isn't EMPTY!</font>")

  public String getPdftext() {
    return pdftext;
  }



  public void setPdftext(String pdftext) {
    this.pdftext = pdftext;
  }


  public DealerOwnFieldsInternAddItem pdftextVerbindlich(String pdftextVerbindlich) {
    
    this.pdftextVerbindlich = pdftextVerbindlich;
    return this;
  }

   /**
   * Field &lt;B&gt;pdftextVerbindlich&lt;/B&gt; will be changed only, if this endpoint request field is given  &lt;font color&#x3D;red&gt;and the field value isn&#39;t EMPTY!&lt;/font&gt;
   * @return pdftextVerbindlich
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Field <B>pdftextVerbindlich</B> will be changed only, if this endpoint request field is given  <font color=red>and the field value isn't EMPTY!</font>")

  public String getPdftextVerbindlich() {
    return pdftextVerbindlich;
  }



  public void setPdftextVerbindlich(String pdftextVerbindlich) {
    this.pdftextVerbindlich = pdftextVerbindlich;
  }


  public DealerOwnFieldsInternAddItem shpFahrzeugangebote(String shpFahrzeugangebote) {
    
    this.shpFahrzeugangebote = shpFahrzeugangebote;
    return this;
  }

   /**
   * Field &lt;B&gt;shpFahrzeugangebote&lt;/B&gt; will be changed, if this endpoint request field is given.  &lt;font color&#x3D;darkgreen&gt;EMPTY value is also allowed&lt;/font&gt;
   * @return shpFahrzeugangebote
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Field <B>shpFahrzeugangebote</B> will be changed, if this endpoint request field is given.  <font color=darkgreen>EMPTY value is also allowed</font>")

  public String getShpFahrzeugangebote() {
    return shpFahrzeugangebote;
  }



  public void setShpFahrzeugangebote(String shpFahrzeugangebote) {
    this.shpFahrzeugangebote = shpFahrzeugangebote;
  }


  public DealerOwnFieldsInternAddItem shpMobile(String shpMobile) {
    
    this.shpMobile = shpMobile;
    return this;
  }

   /**
   * Field &lt;B&gt;shpMobile&lt;/B&gt; will be changed, if this endpoint request field is given.  &lt;font color&#x3D;darkgreen&gt;EMPTY value is also allowed&lt;/font&gt;
   * @return shpMobile
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Field <B>shpMobile</B> will be changed, if this endpoint request field is given.  <font color=darkgreen>EMPTY value is also allowed</font>")

  public String getShpMobile() {
    return shpMobile;
  }



  public void setShpMobile(String shpMobile) {
    this.shpMobile = shpMobile;
  }


  public DealerOwnFieldsInternAddItem shpAutoscout(String shpAutoscout) {
    
    this.shpAutoscout = shpAutoscout;
    return this;
  }

   /**
   * Field &lt;B&gt;shpAutoscout&lt;/B&gt; will be changed, if this endpoint request field is given.  &lt;font color&#x3D;darkgreen&gt;EMPTY value is also allowed&lt;/font&gt;
   * @return shpAutoscout
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Field <B>shpAutoscout</B> will be changed, if this endpoint request field is given.  <font color=darkgreen>EMPTY value is also allowed</font>")

  public String getShpAutoscout() {
    return shpAutoscout;
  }



  public void setShpAutoscout(String shpAutoscout) {
    this.shpAutoscout = shpAutoscout;
  }


  public DealerOwnFieldsInternAddItem suchmaskeUeber1(String suchmaskeUeber1) {
    
    this.suchmaskeUeber1 = suchmaskeUeber1;
    return this;
  }

   /**
   * Field &lt;B&gt;suchmaskeUeber1&lt;/B&gt; will be set only, if this endpoint request field is given.&lt;BR&gt;&lt;font color&#x3D;red&gt;An EMPTY field value is also allowed and this deactivated the field &lt;B&gt;suchmaskeUeber1Aktiv&lt;B&gt; &lt;/font&gt;&lt;BR&gt;
   * @return suchmaskeUeber1
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Field <B>suchmaskeUeber1</B> will be set only, if this endpoint request field is given.<BR><font color=red>An EMPTY field value is also allowed and this deactivated the field <B>suchmaskeUeber1Aktiv<B> </font><BR>")

  public String getSuchmaskeUeber1() {
    return suchmaskeUeber1;
  }



  public void setSuchmaskeUeber1(String suchmaskeUeber1) {
    this.suchmaskeUeber1 = suchmaskeUeber1;
  }


  public DealerOwnFieldsInternAddItem suchmaskeUeber2(String suchmaskeUeber2) {
    
    this.suchmaskeUeber2 = suchmaskeUeber2;
    return this;
  }

   /**
   * Field &lt;B&gt;suchmaskeUeber2&lt;/B&gt; will be set only, if this endpoint request field is given.&lt;BR&gt;&lt;font color&#x3D;red&gt;An EMPTY field value is also allowed and this deactivated the field &lt;B&gt;suchmaskeUeber2Aktiv&lt;B&gt; &lt;/font&gt;&lt;BR&gt;
   * @return suchmaskeUeber2
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Field <B>suchmaskeUeber2</B> will be set only, if this endpoint request field is given.<BR><font color=red>An EMPTY field value is also allowed and this deactivated the field <B>suchmaskeUeber2Aktiv<B> </font><BR>")

  public String getSuchmaskeUeber2() {
    return suchmaskeUeber2;
  }



  public void setSuchmaskeUeber2(String suchmaskeUeber2) {
    this.suchmaskeUeber2 = suchmaskeUeber2;
  }


  public DealerOwnFieldsInternAddItem suchmaskeUeber3(String suchmaskeUeber3) {
    
    this.suchmaskeUeber3 = suchmaskeUeber3;
    return this;
  }

   /**
   * Field &lt;B&gt;suchmaskeUeber3&lt;/B&gt; will be set only, if this endpoint request field is given.&lt;BR&gt;&lt;font color&#x3D;red&gt;An EMPTY field value is also allowed and this deactivated the field &lt;B&gt;suchmaskeUeber3Aktiv&lt;B&gt; &lt;/font&gt;&lt;BR&gt;
   * @return suchmaskeUeber3
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Field <B>suchmaskeUeber3</B> will be set only, if this endpoint request field is given.<BR><font color=red>An EMPTY field value is also allowed and this deactivated the field <B>suchmaskeUeber3Aktiv<B> </font><BR>")

  public String getSuchmaskeUeber3() {
    return suchmaskeUeber3;
  }



  public void setSuchmaskeUeber3(String suchmaskeUeber3) {
    this.suchmaskeUeber3 = suchmaskeUeber3;
  }


  public DealerOwnFieldsInternAddItem detailseiteUeber1(String detailseiteUeber1) {
    
    this.detailseiteUeber1 = detailseiteUeber1;
    return this;
  }

   /**
   * Field &lt;B&gt;detailseiteUeber1&lt;/B&gt; will be set only, if this endpoint request field is given.&lt;BR&gt;&lt;font color&#x3D;red&gt;An EMPTY field value is also allowed and this deactivated the field &lt;B&gt;detailseiteUeber1Aktiv&lt;B&gt; &lt;/font&gt;&lt;BR&gt;
   * @return detailseiteUeber1
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Field <B>detailseiteUeber1</B> will be set only, if this endpoint request field is given.<BR><font color=red>An EMPTY field value is also allowed and this deactivated the field <B>detailseiteUeber1Aktiv<B> </font><BR>")

  public String getDetailseiteUeber1() {
    return detailseiteUeber1;
  }



  public void setDetailseiteUeber1(String detailseiteUeber1) {
    this.detailseiteUeber1 = detailseiteUeber1;
  }


  public DealerOwnFieldsInternAddItem detailseiteUeber2(String detailseiteUeber2) {
    
    this.detailseiteUeber2 = detailseiteUeber2;
    return this;
  }

   /**
   * Field &lt;B&gt;detailseiteUeber2&lt;/B&gt; will be set only, if this endpoint request field is given.&lt;BR&gt;&lt;font color&#x3D;red&gt;An EMPTY field value is also allowed and this deactivated the field &lt;B&gt;detailseiteUeber2Aktiv&lt;B&gt; &lt;/font&gt;&lt;BR&gt;
   * @return detailseiteUeber2
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Field <B>detailseiteUeber2</B> will be set only, if this endpoint request field is given.<BR><font color=red>An EMPTY field value is also allowed and this deactivated the field <B>detailseiteUeber2Aktiv<B> </font><BR>")

  public String getDetailseiteUeber2() {
    return detailseiteUeber2;
  }



  public void setDetailseiteUeber2(String detailseiteUeber2) {
    this.detailseiteUeber2 = detailseiteUeber2;
  }


  public DealerOwnFieldsInternAddItem detailseiteUeber1Druck(String detailseiteUeber1Druck) {
    
    this.detailseiteUeber1Druck = detailseiteUeber1Druck;
    return this;
  }

   /**
   * Field &lt;B&gt;detailseiteUeber1Druck&lt;/B&gt; will be set only, if this endpoint request field is given.&lt;BR&gt;&lt;font color&#x3D;red&gt;An EMPTY field value is also allowed and this deactivated the field &lt;B&gt;detailseiteUeber1DruckAktiv&lt;B&gt; &lt;/font&gt;&lt;BR&gt;
   * @return detailseiteUeber1Druck
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Field <B>detailseiteUeber1Druck</B> will be set only, if this endpoint request field is given.<BR><font color=red>An EMPTY field value is also allowed and this deactivated the field <B>detailseiteUeber1DruckAktiv<B> </font><BR>")

  public String getDetailseiteUeber1Druck() {
    return detailseiteUeber1Druck;
  }



  public void setDetailseiteUeber1Druck(String detailseiteUeber1Druck) {
    this.detailseiteUeber1Druck = detailseiteUeber1Druck;
  }


  public DealerOwnFieldsInternAddItem detailseiteUeber2Druck(String detailseiteUeber2Druck) {
    
    this.detailseiteUeber2Druck = detailseiteUeber2Druck;
    return this;
  }

   /**
   * Field &lt;B&gt;detailseiteUeber2Druck&lt;/B&gt; will be set only, if this endpoint request field is given.&lt;BR&gt;&lt;font color&#x3D;red&gt;An EMPTY field value is also allowed and this deactivated the field &lt;B&gt;detailseiteUeber2DruckAktiv&lt;B&gt; &lt;/font&gt;&lt;BR&gt;
   * @return detailseiteUeber2Druck
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Field <B>detailseiteUeber2Druck</B> will be set only, if this endpoint request field is given.<BR><font color=red>An EMPTY field value is also allowed and this deactivated the field <B>detailseiteUeber2DruckAktiv<B> </font><BR>")

  public String getDetailseiteUeber2Druck() {
    return detailseiteUeber2Druck;
  }



  public void setDetailseiteUeber2Druck(String detailseiteUeber2Druck) {
    this.detailseiteUeber2Druck = detailseiteUeber2Druck;
  }


  public DealerOwnFieldsInternAddItem bestellformularUeber1(String bestellformularUeber1) {
    
    this.bestellformularUeber1 = bestellformularUeber1;
    return this;
  }

   /**
   * Field &lt;B&gt;bestellformularUeber1&lt;/B&gt; will be set only, if this endpoint request field is given.&lt;BR&gt;&lt;font color&#x3D;red&gt;An EMPTY field value is also allowed and this deactivated the field &lt;B&gt;bestellformularUeber1Aktiv&lt;B&gt; &lt;/font&gt;&lt;BR&gt;
   * @return bestellformularUeber1
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Field <B>bestellformularUeber1</B> will be set only, if this endpoint request field is given.<BR><font color=red>An EMPTY field value is also allowed and this deactivated the field <B>bestellformularUeber1Aktiv<B> </font><BR>")

  public String getBestellformularUeber1() {
    return bestellformularUeber1;
  }



  public void setBestellformularUeber1(String bestellformularUeber1) {
    this.bestellformularUeber1 = bestellformularUeber1;
  }


  public DealerOwnFieldsInternAddItem bestellformularUeber2(String bestellformularUeber2) {
    
    this.bestellformularUeber2 = bestellformularUeber2;
    return this;
  }

   /**
   * Field &lt;B&gt;bestellformularUeber2&lt;/B&gt; will be set only, if this endpoint request field is given.&lt;BR&gt;&lt;font color&#x3D;red&gt;An EMPTY field value is also allowed and this deactivated the field &lt;B&gt;bestellformularUeber2Aktiv&lt;B&gt; &lt;/font&gt;&lt;BR&gt;
   * @return bestellformularUeber2
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Field <B>bestellformularUeber2</B> will be set only, if this endpoint request field is given.<BR><font color=red>An EMPTY field value is also allowed and this deactivated the field <B>bestellformularUeber2Aktiv<B> </font><BR>")

  public String getBestellformularUeber2() {
    return bestellformularUeber2;
  }



  public void setBestellformularUeber2(String bestellformularUeber2) {
    this.bestellformularUeber2 = bestellformularUeber2;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DealerOwnFieldsInternAddItem dealerOwnFieldsInternAddItem = (DealerOwnFieldsInternAddItem) o;
    return Objects.equals(this.pdftextAktiv, dealerOwnFieldsInternAddItem.pdftextAktiv) &&
        Objects.equals(this.pdftext, dealerOwnFieldsInternAddItem.pdftext) &&
        Objects.equals(this.pdftextVerbindlich, dealerOwnFieldsInternAddItem.pdftextVerbindlich) &&
        Objects.equals(this.shpFahrzeugangebote, dealerOwnFieldsInternAddItem.shpFahrzeugangebote) &&
        Objects.equals(this.shpMobile, dealerOwnFieldsInternAddItem.shpMobile) &&
        Objects.equals(this.shpAutoscout, dealerOwnFieldsInternAddItem.shpAutoscout) &&
        Objects.equals(this.suchmaskeUeber1, dealerOwnFieldsInternAddItem.suchmaskeUeber1) &&
        Objects.equals(this.suchmaskeUeber2, dealerOwnFieldsInternAddItem.suchmaskeUeber2) &&
        Objects.equals(this.suchmaskeUeber3, dealerOwnFieldsInternAddItem.suchmaskeUeber3) &&
        Objects.equals(this.detailseiteUeber1, dealerOwnFieldsInternAddItem.detailseiteUeber1) &&
        Objects.equals(this.detailseiteUeber2, dealerOwnFieldsInternAddItem.detailseiteUeber2) &&
        Objects.equals(this.detailseiteUeber1Druck, dealerOwnFieldsInternAddItem.detailseiteUeber1Druck) &&
        Objects.equals(this.detailseiteUeber2Druck, dealerOwnFieldsInternAddItem.detailseiteUeber2Druck) &&
        Objects.equals(this.bestellformularUeber1, dealerOwnFieldsInternAddItem.bestellformularUeber1) &&
        Objects.equals(this.bestellformularUeber2, dealerOwnFieldsInternAddItem.bestellformularUeber2);
  }

  @Override
  public int hashCode() {
    return Objects.hash(pdftextAktiv, pdftext, pdftextVerbindlich, shpFahrzeugangebote, shpMobile, shpAutoscout, suchmaskeUeber1, suchmaskeUeber2, suchmaskeUeber3, detailseiteUeber1, detailseiteUeber2, detailseiteUeber1Druck, detailseiteUeber2Druck, bestellformularUeber1, bestellformularUeber2);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DealerOwnFieldsInternAddItem {\n");
    sb.append("    pdftextAktiv: ").append(toIndentedString(pdftextAktiv)).append("\n");
    sb.append("    pdftext: ").append(toIndentedString(pdftext)).append("\n");
    sb.append("    pdftextVerbindlich: ").append(toIndentedString(pdftextVerbindlich)).append("\n");
    sb.append("    shpFahrzeugangebote: ").append(toIndentedString(shpFahrzeugangebote)).append("\n");
    sb.append("    shpMobile: ").append(toIndentedString(shpMobile)).append("\n");
    sb.append("    shpAutoscout: ").append(toIndentedString(shpAutoscout)).append("\n");
    sb.append("    suchmaskeUeber1: ").append(toIndentedString(suchmaskeUeber1)).append("\n");
    sb.append("    suchmaskeUeber2: ").append(toIndentedString(suchmaskeUeber2)).append("\n");
    sb.append("    suchmaskeUeber3: ").append(toIndentedString(suchmaskeUeber3)).append("\n");
    sb.append("    detailseiteUeber1: ").append(toIndentedString(detailseiteUeber1)).append("\n");
    sb.append("    detailseiteUeber2: ").append(toIndentedString(detailseiteUeber2)).append("\n");
    sb.append("    detailseiteUeber1Druck: ").append(toIndentedString(detailseiteUeber1Druck)).append("\n");
    sb.append("    detailseiteUeber2Druck: ").append(toIndentedString(detailseiteUeber2Druck)).append("\n");
    sb.append("    bestellformularUeber1: ").append(toIndentedString(bestellformularUeber1)).append("\n");
    sb.append("    bestellformularUeber2: ").append(toIndentedString(bestellformularUeber2)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

