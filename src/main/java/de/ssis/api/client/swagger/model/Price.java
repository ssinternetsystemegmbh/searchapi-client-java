/*
 * ELN Search-API DEV
 * <b>Build Time: 2025-03-06T14:15:01.313+0100</b><br><br>Host: null<br>IP: SuSApp/127.0.1.1
 *
 * The version of the OpenAPI document: 1.1.2559
 * Contact: technik@eln.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package de.ssis.api.client.swagger.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.math.BigDecimal;

/**
 * Price
 */

public class Price {
  /**
   * Gets or Sets type
   */
  @JsonAdapter(TypeEnum.Adapter.class)
  public enum TypeEnum {
    RECOMMENDED("RECOMMENDED"),
    
    ORIGINAL("ORIGINAL"),
    
    TO_DEALER("TO_DEALER"),
    
    TO_CONSUMER("TO_CONSUMER");

    private String value;

    TypeEnum(String value) {
      this.value = value;
    }

    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    public static TypeEnum fromValue(String value) {
      for (TypeEnum b : TypeEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }

    public static class Adapter extends TypeAdapter<TypeEnum> {
      @Override
      public void write(final JsonWriter jsonWriter, final TypeEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      @Override
      public TypeEnum read(final JsonReader jsonReader) throws IOException {
        String value =  jsonReader.nextString();
        return TypeEnum.fromValue(value);
      }
    }
  }

  public static final String SERIALIZED_NAME_TYPE = "type";
  @SerializedName(SERIALIZED_NAME_TYPE)
  private TypeEnum type;

  public static final String SERIALIZED_NAME_VALUE = "value";
  @SerializedName(SERIALIZED_NAME_VALUE)
  private BigDecimal value;

  public static final String SERIALIZED_NAME_VAT_RATE = "vatRate";
  @SerializedName(SERIALIZED_NAME_VAT_RATE)
  private BigDecimal vatRate;

  public static final String SERIALIZED_NAME_VAT_INCLUDED = "vatIncluded";
  @SerializedName(SERIALIZED_NAME_VAT_INCLUDED)
  private Boolean vatIncluded;

  public static final String SERIALIZED_NAME_CURRENCY_ISO = "currencyIso";
  @SerializedName(SERIALIZED_NAME_CURRENCY_ISO)
  private String currencyIso;

  public static final String SERIALIZED_NAME_CURRENCY_SYMBOL = "currencySymbol";
  @SerializedName(SERIALIZED_NAME_CURRENCY_SYMBOL)
  private String currencySymbol;


  public Price type(TypeEnum type) {
    
    this.type = type;
    return this;
  }

   /**
   * Get type
   * @return type
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public TypeEnum getType() {
    return type;
  }



  public void setType(TypeEnum type) {
    this.type = type;
  }


  public Price value(BigDecimal value) {
    
    this.value = value;
    return this;
  }

   /**
   * Get value
   * @return value
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public BigDecimal getValue() {
    return value;
  }



  public void setValue(BigDecimal value) {
    this.value = value;
  }


  public Price vatRate(BigDecimal vatRate) {
    
    this.vatRate = vatRate;
    return this;
  }

   /**
   * Get vatRate
   * @return vatRate
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public BigDecimal getVatRate() {
    return vatRate;
  }



  public void setVatRate(BigDecimal vatRate) {
    this.vatRate = vatRate;
  }


  public Price vatIncluded(Boolean vatIncluded) {
    
    this.vatIncluded = vatIncluded;
    return this;
  }

   /**
   * Get vatIncluded
   * @return vatIncluded
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public Boolean getVatIncluded() {
    return vatIncluded;
  }



  public void setVatIncluded(Boolean vatIncluded) {
    this.vatIncluded = vatIncluded;
  }


  public Price currencyIso(String currencyIso) {
    
    this.currencyIso = currencyIso;
    return this;
  }

   /**
   * Get currencyIso
   * @return currencyIso
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getCurrencyIso() {
    return currencyIso;
  }



  public void setCurrencyIso(String currencyIso) {
    this.currencyIso = currencyIso;
  }


  public Price currencySymbol(String currencySymbol) {
    
    this.currencySymbol = currencySymbol;
    return this;
  }

   /**
   * Get currencySymbol
   * @return currencySymbol
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getCurrencySymbol() {
    return currencySymbol;
  }



  public void setCurrencySymbol(String currencySymbol) {
    this.currencySymbol = currencySymbol;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Price price = (Price) o;
    return Objects.equals(this.type, price.type) &&
        Objects.equals(this.value, price.value) &&
        Objects.equals(this.vatRate, price.vatRate) &&
        Objects.equals(this.vatIncluded, price.vatIncluded) &&
        Objects.equals(this.currencyIso, price.currencyIso) &&
        Objects.equals(this.currencySymbol, price.currencySymbol);
  }

  @Override
  public int hashCode() {
    return Objects.hash(type, value, vatRate, vatIncluded, currencyIso, currencySymbol);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Price {\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    value: ").append(toIndentedString(value)).append("\n");
    sb.append("    vatRate: ").append(toIndentedString(vatRate)).append("\n");
    sb.append("    vatIncluded: ").append(toIndentedString(vatIncluded)).append("\n");
    sb.append("    currencyIso: ").append(toIndentedString(currencyIso)).append("\n");
    sb.append("    currencySymbol: ").append(toIndentedString(currencySymbol)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

