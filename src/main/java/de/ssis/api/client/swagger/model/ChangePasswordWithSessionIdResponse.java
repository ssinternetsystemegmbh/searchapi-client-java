/*
 * ELN Search-API DEV
 * <b>Build Time: 2025-03-06T14:15:01.313+0100</b><br><br>Host: null<br>IP: SuSApp/127.0.1.1
 *
 * The version of the OpenAPI document: 1.1.2559
 * Contact: technik@eln.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package de.ssis.api.client.swagger.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * Response of password change request.
 */
@ApiModel(description = "Response of password change request.")

public class ChangePasswordWithSessionIdResponse {
  public static final String SERIALIZED_NAME_EMAIL_ADDRESS = "emailAddress";
  @SerializedName(SERIALIZED_NAME_EMAIL_ADDRESS)
  private String emailAddress;

  public static final String SERIALIZED_NAME_DATE_DONE = "dateDone";
  @SerializedName(SERIALIZED_NAME_DATE_DONE)
  private String dateDone;

  public static final String SERIALIZED_NAME_DONE_BY = "doneBy";
  @SerializedName(SERIALIZED_NAME_DONE_BY)
  private String doneBy;

  public static final String SERIALIZED_NAME_MESSAGE = "message";
  @SerializedName(SERIALIZED_NAME_MESSAGE)
  private String message;


  public ChangePasswordWithSessionIdResponse emailAddress(String emailAddress) {
    
    this.emailAddress = emailAddress;
    return this;
  }

   /**
   * Email Address the change password information was sent.
   * @return emailAddress
  **/
  @ApiModelProperty(required = true, value = "Email Address the change password information was sent.")

  public String getEmailAddress() {
    return emailAddress;
  }



  public void setEmailAddress(String emailAddress) {
    this.emailAddress = emailAddress;
  }


  public ChangePasswordWithSessionIdResponse dateDone(String dateDone) {
    
    this.dateDone = dateDone;
    return this;
  }

   /**
   * Timestamp of the entry.
   * @return dateDone
  **/
  @ApiModelProperty(required = true, value = "Timestamp of the entry.")

  public String getDateDone() {
    return dateDone;
  }



  public void setDateDone(String dateDone) {
    this.dateDone = dateDone;
  }


  public ChangePasswordWithSessionIdResponse doneBy(String doneBy) {
    
    this.doneBy = doneBy;
    return this;
  }

   /**
   * Principal of the entry (eg. SusAdmin, PWForgotSite)
   * @return doneBy
  **/
  @ApiModelProperty(required = true, value = "Principal of the entry (eg. SusAdmin, PWForgotSite)")

  public String getDoneBy() {
    return doneBy;
  }



  public void setDoneBy(String doneBy) {
    this.doneBy = doneBy;
  }


  public ChangePasswordWithSessionIdResponse message(String message) {
    
    this.message = message;
    return this;
  }

   /**
   * Further information.
   * @return message
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Further information.")

  public String getMessage() {
    return message;
  }



  public void setMessage(String message) {
    this.message = message;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ChangePasswordWithSessionIdResponse changePasswordWithSessionIdResponse = (ChangePasswordWithSessionIdResponse) o;
    return Objects.equals(this.emailAddress, changePasswordWithSessionIdResponse.emailAddress) &&
        Objects.equals(this.dateDone, changePasswordWithSessionIdResponse.dateDone) &&
        Objects.equals(this.doneBy, changePasswordWithSessionIdResponse.doneBy) &&
        Objects.equals(this.message, changePasswordWithSessionIdResponse.message);
  }

  @Override
  public int hashCode() {
    return Objects.hash(emailAddress, dateDone, doneBy, message);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ChangePasswordWithSessionIdResponse {\n");
    sb.append("    emailAddress: ").append(toIndentedString(emailAddress)).append("\n");
    sb.append("    dateDone: ").append(toIndentedString(dateDone)).append("\n");
    sb.append("    doneBy: ").append(toIndentedString(doneBy)).append("\n");
    sb.append("    message: ").append(toIndentedString(message)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

