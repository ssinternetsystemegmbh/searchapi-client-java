/*
 * ELN Search-API DEV
 * <b>Build Time: 2025-03-06T14:15:01.313+0100</b><br><br>Host: null<br>IP: SuSApp/127.0.1.1
 *
 * The version of the OpenAPI document: 1.1.2559
 * Contact: technik@eln.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package de.ssis.api.client.swagger.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * A container for a list of unique identifiers (SUSUUIDs) associated with vehicles.
 */
@ApiModel(description = "A container for a list of unique identifiers (SUSUUIDs) associated with vehicles.")

public class VehicleSusuuidList {
  public static final String SERIALIZED_NAME_SUSUUIDS = "susuuids";
  @SerializedName(SERIALIZED_NAME_SUSUUIDS)
  private List<String> susuuids = new ArrayList<String>();


  public VehicleSusuuidList susuuids(List<String> susuuids) {
    
    this.susuuids = susuuids;
    return this;
  }

  public VehicleSusuuidList addSusuuidsItem(String susuuidsItem) {
    this.susuuids.add(susuuidsItem);
    return this;
  }

   /**
   * A list of SUSUUIDs uniquely identifying the associated vehicles.
   * @return susuuids
  **/
  @ApiModelProperty(required = true, value = "A list of SUSUUIDs uniquely identifying the associated vehicles.")

  public List<String> getSusuuids() {
    return susuuids;
  }



  public void setSusuuids(List<String> susuuids) {
    this.susuuids = susuuids;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VehicleSusuuidList vehicleSusuuidList = (VehicleSusuuidList) o;
    return Objects.equals(this.susuuids, vehicleSusuuidList.susuuids);
  }

  @Override
  public int hashCode() {
    return Objects.hash(susuuids);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class VehicleSusuuidList {\n");
    sb.append("    susuuids: ").append(toIndentedString(susuuids)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

