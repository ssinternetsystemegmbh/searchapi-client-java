/*
 * ELN Search-API DEV
 * <b>Build Time: 2025-03-06T14:15:01.313+0100</b><br><br>Host: null<br>IP: SuSApp/127.0.1.1
 *
 * The version of the OpenAPI document: 1.1.2559
 * Contact: technik@eln.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package de.ssis.api.client.swagger.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * DTO for a request of vehicle manufacturers from a dealer.
 */
@ApiModel(description = "DTO for a request of vehicle manufacturers from a dealer.")

public class DealerVehicleManufacturerRequest {
  public static final String SERIALIZED_NAME_DEALER_IDS = "dealerIds";
  @SerializedName(SERIALIZED_NAME_DEALER_IDS)
  private List<Integer> dealerIds = null;


  public DealerVehicleManufacturerRequest dealerIds(List<Integer> dealerIds) {
    
    this.dealerIds = dealerIds;
    return this;
  }

  public DealerVehicleManufacturerRequest addDealerIdsItem(Integer dealerIdsItem) {
    if (this.dealerIds == null) {
      this.dealerIds = new ArrayList<Integer>();
    }
    this.dealerIds.add(dealerIdsItem);
    return this;
  }

   /**
   * A list of dealer IDs.
   * @return dealerIds
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "A list of dealer IDs.")

  public List<Integer> getDealerIds() {
    return dealerIds;
  }



  public void setDealerIds(List<Integer> dealerIds) {
    this.dealerIds = dealerIds;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DealerVehicleManufacturerRequest dealerVehicleManufacturerRequest = (DealerVehicleManufacturerRequest) o;
    return Objects.equals(this.dealerIds, dealerVehicleManufacturerRequest.dealerIds);
  }

  @Override
  public int hashCode() {
    return Objects.hash(dealerIds);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DealerVehicleManufacturerRequest {\n");
    sb.append("    dealerIds: ").append(toIndentedString(dealerIds)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

