/*
 * ELN Search-API DEV
 * <b>Build Time: 2025-03-06T14:15:01.313+0100</b><br><br>Host: null<br>IP: SuSApp/127.0.1.1
 *
 * The version of the OpenAPI document: 1.1.2559
 * Contact: technik@eln.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package de.ssis.api.client.swagger.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import de.ssis.api.client.swagger.model.VehicleIntern;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * VehicleInternResponse
 */

public class VehicleInternResponse {
  public static final String SERIALIZED_NAME_VEHICLES = "vehicles";
  @SerializedName(SERIALIZED_NAME_VEHICLES)
  private List<VehicleIntern> vehicles = null;


  public VehicleInternResponse vehicles(List<VehicleIntern> vehicles) {
    
    this.vehicles = vehicles;
    return this;
  }

  public VehicleInternResponse addVehiclesItem(VehicleIntern vehiclesItem) {
    if (this.vehicles == null) {
      this.vehicles = new ArrayList<VehicleIntern>();
    }
    this.vehicles.add(vehiclesItem);
    return this;
  }

   /**
   * Get vehicles
   * @return vehicles
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public List<VehicleIntern> getVehicles() {
    return vehicles;
  }



  public void setVehicles(List<VehicleIntern> vehicles) {
    this.vehicles = vehicles;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VehicleInternResponse vehicleInternResponse = (VehicleInternResponse) o;
    return Objects.equals(this.vehicles, vehicleInternResponse.vehicles);
  }

  @Override
  public int hashCode() {
    return Objects.hash(vehicles);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class VehicleInternResponse {\n");
    sb.append("    vehicles: ").append(toIndentedString(vehicles)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

