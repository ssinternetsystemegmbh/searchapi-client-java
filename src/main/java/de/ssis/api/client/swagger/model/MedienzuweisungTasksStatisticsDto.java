/*
 * ELN Search-API DEV
 * <b>Build Time: 2025-03-06T14:15:01.313+0100</b><br><br>Host: null<br>IP: SuSApp/127.0.1.1
 *
 * The version of the OpenAPI document: 1.1.2559
 * Contact: technik@eln.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package de.ssis.api.client.swagger.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import de.ssis.api.client.swagger.model.MedienzuweisungTasksDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * Statistics for media assignment tasks.
 */
@ApiModel(description = "Statistics for media assignment tasks.")

public class MedienzuweisungTasksStatisticsDto {
  public static final String SERIALIZED_NAME_NUM_ENTRIES = "numEntries";
  @SerializedName(SERIALIZED_NAME_NUM_ENTRIES)
  private Integer numEntries;

  public static final String SERIALIZED_NAME_TO_OLDEST_ENTRY_SECONDS = "toOldestEntrySeconds";
  @SerializedName(SERIALIZED_NAME_TO_OLDEST_ENTRY_SECONDS)
  private Long toOldestEntrySeconds;

  public static final String SERIALIZED_NAME_OLDEST_MEDIENZUWEISUNG_TASK = "oldestMedienzuweisungTask";
  @SerializedName(SERIALIZED_NAME_OLDEST_MEDIENZUWEISUNG_TASK)
  private MedienzuweisungTasksDto oldestMedienzuweisungTask;


  public MedienzuweisungTasksStatisticsDto numEntries(Integer numEntries) {
    
    this.numEntries = numEntries;
    return this;
  }

   /**
   * The total number of media assignment tasks
   * @return numEntries
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "The total number of media assignment tasks")

  public Integer getNumEntries() {
    return numEntries;
  }



  public void setNumEntries(Integer numEntries) {
    this.numEntries = numEntries;
  }


  public MedienzuweisungTasksStatisticsDto toOldestEntrySeconds(Long toOldestEntrySeconds) {
    
    this.toOldestEntrySeconds = toOldestEntrySeconds;
    return this;
  }

   /**
   * The duration in seconds to the oldest task entry
   * @return toOldestEntrySeconds
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "The duration in seconds to the oldest task entry")

  public Long getToOldestEntrySeconds() {
    return toOldestEntrySeconds;
  }



  public void setToOldestEntrySeconds(Long toOldestEntrySeconds) {
    this.toOldestEntrySeconds = toOldestEntrySeconds;
  }


  public MedienzuweisungTasksStatisticsDto oldestMedienzuweisungTask(MedienzuweisungTasksDto oldestMedienzuweisungTask) {
    
    this.oldestMedienzuweisungTask = oldestMedienzuweisungTask;
    return this;
  }

   /**
   * Get oldestMedienzuweisungTask
   * @return oldestMedienzuweisungTask
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public MedienzuweisungTasksDto getOldestMedienzuweisungTask() {
    return oldestMedienzuweisungTask;
  }



  public void setOldestMedienzuweisungTask(MedienzuweisungTasksDto oldestMedienzuweisungTask) {
    this.oldestMedienzuweisungTask = oldestMedienzuweisungTask;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MedienzuweisungTasksStatisticsDto medienzuweisungTasksStatisticsDto = (MedienzuweisungTasksStatisticsDto) o;
    return Objects.equals(this.numEntries, medienzuweisungTasksStatisticsDto.numEntries) &&
        Objects.equals(this.toOldestEntrySeconds, medienzuweisungTasksStatisticsDto.toOldestEntrySeconds) &&
        Objects.equals(this.oldestMedienzuweisungTask, medienzuweisungTasksStatisticsDto.oldestMedienzuweisungTask);
  }

  @Override
  public int hashCode() {
    return Objects.hash(numEntries, toOldestEntrySeconds, oldestMedienzuweisungTask);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class MedienzuweisungTasksStatisticsDto {\n");
    sb.append("    numEntries: ").append(toIndentedString(numEntries)).append("\n");
    sb.append("    toOldestEntrySeconds: ").append(toIndentedString(toOldestEntrySeconds)).append("\n");
    sb.append("    oldestMedienzuweisungTask: ").append(toIndentedString(oldestMedienzuweisungTask)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

