/*
 * ELN Search-API DEV
 * <b>Build Time: 2025-03-06T14:15:01.313+0100</b><br><br>Host: null<br>IP: SuSApp/127.0.1.1
 *
 * The version of the OpenAPI document: 1.1.2559
 * Contact: technik@eln.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package de.ssis.api.client.swagger.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * This search is used for ELN internal.
 */
@ApiModel(description = "This search is used for ELN internal.")

public class DealerBillsInternSearch {
  public static final String SERIALIZED_NAME_BILL_YEAR = "bill_year";
  @SerializedName(SERIALIZED_NAME_BILL_YEAR)
  private String billYear;


  public DealerBillsInternSearch billYear(String billYear) {
    
    this.billYear = billYear;
    return this;
  }

   /**
   * Leave empty for all bills.
   * @return billYear
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Leave empty for all bills.")

  public String getBillYear() {
    return billYear;
  }



  public void setBillYear(String billYear) {
    this.billYear = billYear;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DealerBillsInternSearch dealerBillsInternSearch = (DealerBillsInternSearch) o;
    return Objects.equals(this.billYear, dealerBillsInternSearch.billYear);
  }

  @Override
  public int hashCode() {
    return Objects.hash(billYear);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DealerBillsInternSearch {\n");
    sb.append("    billYear: ").append(toIndentedString(billYear)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

