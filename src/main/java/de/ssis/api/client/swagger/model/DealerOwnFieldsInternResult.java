/*
 * ELN Search-API DEV
 * <b>Build Time: 2025-03-06T14:15:01.313+0100</b><br><br>Host: null<br>IP: SuSApp/127.0.1.1
 *
 * The version of the OpenAPI document: 1.1.2559
 * Contact: technik@eln.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package de.ssis.api.client.swagger.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import de.ssis.api.client.swagger.model.DealerOwnFieldsInternResultItem;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * Returns the final stored values in table &#39;tblhaendlerEigeneFelder&#39; for the given dealerId
 */
@ApiModel(description = "Returns the final stored values in table 'tblhaendlerEigeneFelder' for the given dealerId")

public class DealerOwnFieldsInternResult {
  public static final String SERIALIZED_NAME_SUCCESS = "success";
  @SerializedName(SERIALIZED_NAME_SUCCESS)
  private Boolean success;

  public static final String SERIALIZED_NAME_MESSAGE = "message";
  @SerializedName(SERIALIZED_NAME_MESSAGE)
  private String message;

  public static final String SERIALIZED_NAME_DEALER_OWN_FIELDS_INTERN_RESULT_ITEM = "dealerOwnFieldsInternResultItem";
  @SerializedName(SERIALIZED_NAME_DEALER_OWN_FIELDS_INTERN_RESULT_ITEM)
  private DealerOwnFieldsInternResultItem dealerOwnFieldsInternResultItem;


  public DealerOwnFieldsInternResult success(Boolean success) {
    
    this.success = success;
    return this;
  }

   /**
   * Get success
   * @return success
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public Boolean getSuccess() {
    return success;
  }



  public void setSuccess(Boolean success) {
    this.success = success;
  }


  public DealerOwnFieldsInternResult message(String message) {
    
    this.message = message;
    return this;
  }

   /**
   * Get message
   * @return message
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getMessage() {
    return message;
  }



  public void setMessage(String message) {
    this.message = message;
  }


  public DealerOwnFieldsInternResult dealerOwnFieldsInternResultItem(DealerOwnFieldsInternResultItem dealerOwnFieldsInternResultItem) {
    
    this.dealerOwnFieldsInternResultItem = dealerOwnFieldsInternResultItem;
    return this;
  }

   /**
   * Get dealerOwnFieldsInternResultItem
   * @return dealerOwnFieldsInternResultItem
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public DealerOwnFieldsInternResultItem getDealerOwnFieldsInternResultItem() {
    return dealerOwnFieldsInternResultItem;
  }



  public void setDealerOwnFieldsInternResultItem(DealerOwnFieldsInternResultItem dealerOwnFieldsInternResultItem) {
    this.dealerOwnFieldsInternResultItem = dealerOwnFieldsInternResultItem;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DealerOwnFieldsInternResult dealerOwnFieldsInternResult = (DealerOwnFieldsInternResult) o;
    return Objects.equals(this.success, dealerOwnFieldsInternResult.success) &&
        Objects.equals(this.message, dealerOwnFieldsInternResult.message) &&
        Objects.equals(this.dealerOwnFieldsInternResultItem, dealerOwnFieldsInternResult.dealerOwnFieldsInternResultItem);
  }

  @Override
  public int hashCode() {
    return Objects.hash(success, message, dealerOwnFieldsInternResultItem);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DealerOwnFieldsInternResult {\n");
    sb.append("    success: ").append(toIndentedString(success)).append("\n");
    sb.append("    message: ").append(toIndentedString(message)).append("\n");
    sb.append("    dealerOwnFieldsInternResultItem: ").append(toIndentedString(dealerOwnFieldsInternResultItem)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

