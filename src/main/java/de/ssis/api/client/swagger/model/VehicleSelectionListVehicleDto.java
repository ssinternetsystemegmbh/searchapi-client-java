/*
 * ELN Search-API DEV
 * <b>Build Time: 2025-03-06T14:15:01.313+0100</b><br><br>Host: null<br>IP: SuSApp/127.0.1.1
 *
 * The version of the OpenAPI document: 1.1.2559
 * Contact: technik@eln.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package de.ssis.api.client.swagger.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import org.joda.time.DateTime;

/**
 * This class represents a vehicle in the selection list with associated metadata and timestamps.
 */
@ApiModel(description = "This class represents a vehicle in the selection list with associated metadata and timestamps.")

public class VehicleSelectionListVehicleDto {
  public static final String SERIALIZED_NAME_SUSUUID = "susuuid";
  @SerializedName(SERIALIZED_NAME_SUSUUID)
  private String susuuid;

  public static final String SERIALIZED_NAME_META_DATA = "metaData";
  @SerializedName(SERIALIZED_NAME_META_DATA)
  private String metaData;

  public static final String SERIALIZED_NAME_DATE_CREATED = "dateCreated";
  @SerializedName(SERIALIZED_NAME_DATE_CREATED)
  private DateTime dateCreated;


  public VehicleSelectionListVehicleDto susuuid(String susuuid) {
    
    this.susuuid = susuuid;
    return this;
  }

   /**
   * Unique identifier for the vehicle.
   * @return susuuid
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Unique identifier for the vehicle.")

  public String getSusuuid() {
    return susuuid;
  }



  public void setSusuuid(String susuuid) {
    this.susuuid = susuuid;
  }


  public VehicleSelectionListVehicleDto metaData(String metaData) {
    
    this.metaData = metaData;
    return this;
  }

   /**
   * Metadata containing additional information about the vehicle.
   * @return metaData
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Metadata containing additional information about the vehicle.")

  public String getMetaData() {
    return metaData;
  }



  public void setMetaData(String metaData) {
    this.metaData = metaData;
  }


  public VehicleSelectionListVehicleDto dateCreated(DateTime dateCreated) {
    
    this.dateCreated = dateCreated;
    return this;
  }

   /**
   * Timestamp indicating when the vehicle was added to the selection list.
   * @return dateCreated
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Timestamp indicating when the vehicle was added to the selection list.")

  public DateTime getDateCreated() {
    return dateCreated;
  }



  public void setDateCreated(DateTime dateCreated) {
    this.dateCreated = dateCreated;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VehicleSelectionListVehicleDto vehicleSelectionListVehicleDto = (VehicleSelectionListVehicleDto) o;
    return Objects.equals(this.susuuid, vehicleSelectionListVehicleDto.susuuid) &&
        Objects.equals(this.metaData, vehicleSelectionListVehicleDto.metaData) &&
        Objects.equals(this.dateCreated, vehicleSelectionListVehicleDto.dateCreated);
  }

  @Override
  public int hashCode() {
    return Objects.hash(susuuid, metaData, dateCreated);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class VehicleSelectionListVehicleDto {\n");
    sb.append("    susuuid: ").append(toIndentedString(susuuid)).append("\n");
    sb.append("    metaData: ").append(toIndentedString(metaData)).append("\n");
    sb.append("    dateCreated: ").append(toIndentedString(dateCreated)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

