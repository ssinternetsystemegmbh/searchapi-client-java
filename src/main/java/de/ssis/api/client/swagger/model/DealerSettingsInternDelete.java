/*
 * ELN Search-API DEV
 * <b>Build Time: 2025-03-06T14:15:01.313+0100</b><br><br>Host: null<br>IP: SuSApp/127.0.1.1
 *
 * The version of the OpenAPI document: 1.1.2559
 * Contact: technik@eln.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package de.ssis.api.client.swagger.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import de.ssis.api.client.swagger.model.DealerSettingsInternDeleteItem;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * This request is used for ELN internal.
 */
@ApiModel(description = "This request is used for ELN internal.")

public class DealerSettingsInternDelete {
  public static final String SERIALIZED_NAME_ITEMS = "items";
  @SerializedName(SERIALIZED_NAME_ITEMS)
  private List<DealerSettingsInternDeleteItem> items = null;


  public DealerSettingsInternDelete items(List<DealerSettingsInternDeleteItem> items) {
    
    this.items = items;
    return this;
  }

  public DealerSettingsInternDelete addItemsItem(DealerSettingsInternDeleteItem itemsItem) {
    if (this.items == null) {
      this.items = new ArrayList<DealerSettingsInternDeleteItem>();
    }
    this.items.add(itemsItem);
    return this;
  }

   /**
   * A list of settings_keys
   * @return items
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "A list of settings_keys")

  public List<DealerSettingsInternDeleteItem> getItems() {
    return items;
  }



  public void setItems(List<DealerSettingsInternDeleteItem> items) {
    this.items = items;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DealerSettingsInternDelete dealerSettingsInternDelete = (DealerSettingsInternDelete) o;
    return Objects.equals(this.items, dealerSettingsInternDelete.items);
  }

  @Override
  public int hashCode() {
    return Objects.hash(items);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DealerSettingsInternDelete {\n");
    sb.append("    items: ").append(toIndentedString(items)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

