/*
 * ELN Search-API DEV
 * Host: null<br>IP: SuSApp/127.0.1.1
 *
 * The version of the OpenAPI document: 1.1.2492
 * Contact: technik@ssis.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package de.ssis.api.client.swagger.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * Tblhaendlerdaten
 */

public class Tblhaendlerdaten {
  public static final String SERIALIZED_NAME_FIRMA = "firma";
  @SerializedName(SERIALIZED_NAME_FIRMA)
  private String firma;

  public static final String SERIALIZED_NAME_FIRMENZUSATZ = "firmenzusatz";
  @SerializedName(SERIALIZED_NAME_FIRMENZUSATZ)
  private String firmenzusatz;

  public static final String SERIALIZED_NAME_STRASSE = "strasse";
  @SerializedName(SERIALIZED_NAME_STRASSE)
  private String strasse;

  public static final String SERIALIZED_NAME_LHD_ID = "lhd_id";
  @SerializedName(SERIALIZED_NAME_LHD_ID)
  private Integer lhdId;

  public static final String SERIALIZED_NAME_PLZ = "plz";
  @SerializedName(SERIALIZED_NAME_PLZ)
  private String plz;

  public static final String SERIALIZED_NAME_ORT = "ort";
  @SerializedName(SERIALIZED_NAME_ORT)
  private String ort;

  public static final String SERIALIZED_NAME_ANSPRECHPARTNER1 = "ansprechpartner1";
  @SerializedName(SERIALIZED_NAME_ANSPRECHPARTNER1)
  private String ansprechpartner1;

  public static final String SERIALIZED_NAME_ANSPRECHPARTNER2 = "ansprechpartner2";
  @SerializedName(SERIALIZED_NAME_ANSPRECHPARTNER2)
  private String ansprechpartner2;

  public static final String SERIALIZED_NAME_TELEFON1 = "telefon1";
  @SerializedName(SERIALIZED_NAME_TELEFON1)
  private String telefon1;

  public static final String SERIALIZED_NAME_TELEFON2 = "telefon2";
  @SerializedName(SERIALIZED_NAME_TELEFON2)
  private String telefon2;

  public static final String SERIALIZED_NAME_FAX = "fax";
  @SerializedName(SERIALIZED_NAME_FAX)
  private String fax;

  public static final String SERIALIZED_NAME_MOBIL = "mobil";
  @SerializedName(SERIALIZED_NAME_MOBIL)
  private String mobil;

  public static final String SERIALIZED_NAME_EMAIL1 = "email1";
  @SerializedName(SERIALIZED_NAME_EMAIL1)
  private String email1;

  public static final String SERIALIZED_NAME_EMAIL2 = "email2";
  @SerializedName(SERIALIZED_NAME_EMAIL2)
  private String email2;

  public static final String SERIALIZED_NAME_EMAILTECHNIC = "emailtechnic";
  @SerializedName(SERIALIZED_NAME_EMAILTECHNIC)
  private String emailtechnic;

  public static final String SERIALIZED_NAME_EMAILBILLING = "emailbilling";
  @SerializedName(SERIALIZED_NAME_EMAILBILLING)
  private String emailbilling;

  public static final String SERIALIZED_NAME_HOMEPAGE = "homepage";
  @SerializedName(SERIALIZED_NAME_HOMEPAGE)
  private String homepage;

  public static final String SERIALIZED_NAME_USTID = "ustid";
  @SerializedName(SERIALIZED_NAME_USTID)
  private String ustid;

  public static final String SERIALIZED_NAME_HRB = "hrb";
  @SerializedName(SERIALIZED_NAME_HRB)
  private String hrb;

  public static final String SERIALIZED_NAME_GESELLSCHAFTSFORM = "gesellschaftsform";
  @SerializedName(SERIALIZED_NAME_GESELLSCHAFTSFORM)
  private String gesellschaftsform;

  public static final String SERIALIZED_NAME_GESCHAEFTSFUEHRER = "geschaeftsfuehrer";
  @SerializedName(SERIALIZED_NAME_GESCHAEFTSFUEHRER)
  private String geschaeftsfuehrer;

  public static final String SERIALIZED_NAME_REGISTERGERICHT = "registergericht";
  @SerializedName(SERIALIZED_NAME_REGISTERGERICHT)
  private String registergericht;

  public static final String SERIALIZED_NAME_REGISTERNUMMER = "registernummer";
  @SerializedName(SERIALIZED_NAME_REGISTERNUMMER)
  private String registernummer;

  public static final String SERIALIZED_NAME_EUIDENTNUMMER = "euidentnummer";
  @SerializedName(SERIALIZED_NAME_EUIDENTNUMMER)
  private String euidentnummer;

  public static final String SERIALIZED_NAME_VVREGISTER = "vvregister";
  @SerializedName(SERIALIZED_NAME_VVREGISTER)
  private String vvregister;

  public static final String SERIALIZED_NAME_SCHIEDSSTELLE = "schiedsstelle";
  @SerializedName(SERIALIZED_NAME_SCHIEDSSTELLE)
  private String schiedsstelle;

  public static final String SERIALIZED_NAME_BANK = "bank";
  @SerializedName(SERIALIZED_NAME_BANK)
  private String bank;

  public static final String SERIALIZED_NAME_IBAN = "iban";
  @SerializedName(SERIALIZED_NAME_IBAN)
  private String iban;

  public static final String SERIALIZED_NAME_BIC = "bic";
  @SerializedName(SERIALIZED_NAME_BIC)
  private String bic;


  public Tblhaendlerdaten firma(String firma) {
    
    this.firma = firma;
    return this;
  }

   /**
   * Firma wird nur übernommen, wenn eine Händerneuanlage(INSERT) erfolgt. Zurzeit ist nur das UPDATE erlaubt und daher wird dieses Feld ignoriert.
   * @return firma
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Firma wird nur übernommen, wenn eine Händerneuanlage(INSERT) erfolgt. Zurzeit ist nur das UPDATE erlaubt und daher wird dieses Feld ignoriert.")

  public String getFirma() {
    return firma;
  }



  public void setFirma(String firma) {
    this.firma = firma;
  }


  public Tblhaendlerdaten firmenzusatz(String firmenzusatz) {
    
    this.firmenzusatz = firmenzusatz;
    return this;
  }

   /**
   * Get firmenzusatz
   * @return firmenzusatz
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getFirmenzusatz() {
    return firmenzusatz;
  }



  public void setFirmenzusatz(String firmenzusatz) {
    this.firmenzusatz = firmenzusatz;
  }


  public Tblhaendlerdaten strasse(String strasse) {
    
    this.strasse = strasse;
    return this;
  }

   /**
   * Get strasse
   * @return strasse
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getStrasse() {
    return strasse;
  }



  public void setStrasse(String strasse) {
    this.strasse = strasse;
  }


  public Tblhaendlerdaten lhdId(Integer lhdId) {
    
    this.lhdId = lhdId;
    return this;
  }

   /**
   * ID des Herkunktsland
   * @return lhdId
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "ID des Herkunktsland")

  public Integer getLhdId() {
    return lhdId;
  }



  public void setLhdId(Integer lhdId) {
    this.lhdId = lhdId;
  }


  public Tblhaendlerdaten plz(String plz) {
    
    this.plz = plz;
    return this;
  }

   /**
   * Get plz
   * @return plz
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getPlz() {
    return plz;
  }



  public void setPlz(String plz) {
    this.plz = plz;
  }


  public Tblhaendlerdaten ort(String ort) {
    
    this.ort = ort;
    return this;
  }

   /**
   * Get ort
   * @return ort
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getOrt() {
    return ort;
  }



  public void setOrt(String ort) {
    this.ort = ort;
  }


  public Tblhaendlerdaten ansprechpartner1(String ansprechpartner1) {
    
    this.ansprechpartner1 = ansprechpartner1;
    return this;
  }

   /**
   * Get ansprechpartner1
   * @return ansprechpartner1
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getAnsprechpartner1() {
    return ansprechpartner1;
  }



  public void setAnsprechpartner1(String ansprechpartner1) {
    this.ansprechpartner1 = ansprechpartner1;
  }


  public Tblhaendlerdaten ansprechpartner2(String ansprechpartner2) {
    
    this.ansprechpartner2 = ansprechpartner2;
    return this;
  }

   /**
   * Get ansprechpartner2
   * @return ansprechpartner2
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getAnsprechpartner2() {
    return ansprechpartner2;
  }



  public void setAnsprechpartner2(String ansprechpartner2) {
    this.ansprechpartner2 = ansprechpartner2;
  }


  public Tblhaendlerdaten telefon1(String telefon1) {
    
    this.telefon1 = telefon1;
    return this;
  }

   /**
   * Get telefon1
   * @return telefon1
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getTelefon1() {
    return telefon1;
  }



  public void setTelefon1(String telefon1) {
    this.telefon1 = telefon1;
  }


  public Tblhaendlerdaten telefon2(String telefon2) {
    
    this.telefon2 = telefon2;
    return this;
  }

   /**
   * Get telefon2
   * @return telefon2
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getTelefon2() {
    return telefon2;
  }



  public void setTelefon2(String telefon2) {
    this.telefon2 = telefon2;
  }


  public Tblhaendlerdaten fax(String fax) {
    
    this.fax = fax;
    return this;
  }

   /**
   * Get fax
   * @return fax
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getFax() {
    return fax;
  }



  public void setFax(String fax) {
    this.fax = fax;
  }


  public Tblhaendlerdaten mobil(String mobil) {
    
    this.mobil = mobil;
    return this;
  }

   /**
   * Get mobil
   * @return mobil
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getMobil() {
    return mobil;
  }



  public void setMobil(String mobil) {
    this.mobil = mobil;
  }


  public Tblhaendlerdaten email1(String email1) {
    
    this.email1 = email1;
    return this;
  }

   /**
   * Get email1
   * @return email1
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getEmail1() {
    return email1;
  }



  public void setEmail1(String email1) {
    this.email1 = email1;
  }


  public Tblhaendlerdaten email2(String email2) {
    
    this.email2 = email2;
    return this;
  }

   /**
   * Get email2
   * @return email2
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getEmail2() {
    return email2;
  }



  public void setEmail2(String email2) {
    this.email2 = email2;
  }


  public Tblhaendlerdaten emailtechnic(String emailtechnic) {
    
    this.emailtechnic = emailtechnic;
    return this;
  }

   /**
   * Technik E-Mail
   * @return emailtechnic
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Technik E-Mail")

  public String getEmailtechnic() {
    return emailtechnic;
  }



  public void setEmailtechnic(String emailtechnic) {
    this.emailtechnic = emailtechnic;
  }


  public Tblhaendlerdaten emailbilling(String emailbilling) {
    
    this.emailbilling = emailbilling;
    return this;
  }

   /**
   * Rechnungs E-Mail
   * @return emailbilling
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Rechnungs E-Mail")

  public String getEmailbilling() {
    return emailbilling;
  }



  public void setEmailbilling(String emailbilling) {
    this.emailbilling = emailbilling;
  }


  public Tblhaendlerdaten homepage(String homepage) {
    
    this.homepage = homepage;
    return this;
  }

   /**
   * Get homepage
   * @return homepage
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getHomepage() {
    return homepage;
  }



  public void setHomepage(String homepage) {
    this.homepage = homepage;
  }


  public Tblhaendlerdaten ustid(String ustid) {
    
    this.ustid = ustid;
    return this;
  }

   /**
   * Get ustid
   * @return ustid
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getUstid() {
    return ustid;
  }



  public void setUstid(String ustid) {
    this.ustid = ustid;
  }


  public Tblhaendlerdaten hrb(String hrb) {
    
    this.hrb = hrb;
    return this;
  }

   /**
   * Get hrb
   * @return hrb
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getHrb() {
    return hrb;
  }



  public void setHrb(String hrb) {
    this.hrb = hrb;
  }


  public Tblhaendlerdaten gesellschaftsform(String gesellschaftsform) {
    
    this.gesellschaftsform = gesellschaftsform;
    return this;
  }

   /**
   * Get gesellschaftsform
   * @return gesellschaftsform
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getGesellschaftsform() {
    return gesellschaftsform;
  }



  public void setGesellschaftsform(String gesellschaftsform) {
    this.gesellschaftsform = gesellschaftsform;
  }


  public Tblhaendlerdaten geschaeftsfuehrer(String geschaeftsfuehrer) {
    
    this.geschaeftsfuehrer = geschaeftsfuehrer;
    return this;
  }

   /**
   * Get geschaeftsfuehrer
   * @return geschaeftsfuehrer
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getGeschaeftsfuehrer() {
    return geschaeftsfuehrer;
  }



  public void setGeschaeftsfuehrer(String geschaeftsfuehrer) {
    this.geschaeftsfuehrer = geschaeftsfuehrer;
  }


  public Tblhaendlerdaten registergericht(String registergericht) {
    
    this.registergericht = registergericht;
    return this;
  }

   /**
   * Get registergericht
   * @return registergericht
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getRegistergericht() {
    return registergericht;
  }



  public void setRegistergericht(String registergericht) {
    this.registergericht = registergericht;
  }


  public Tblhaendlerdaten registernummer(String registernummer) {
    
    this.registernummer = registernummer;
    return this;
  }

   /**
   * Get registernummer
   * @return registernummer
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getRegisternummer() {
    return registernummer;
  }



  public void setRegisternummer(String registernummer) {
    this.registernummer = registernummer;
  }


  public Tblhaendlerdaten euidentnummer(String euidentnummer) {
    
    this.euidentnummer = euidentnummer;
    return this;
  }

   /**
   * Get euidentnummer
   * @return euidentnummer
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getEuidentnummer() {
    return euidentnummer;
  }



  public void setEuidentnummer(String euidentnummer) {
    this.euidentnummer = euidentnummer;
  }


  public Tblhaendlerdaten vvregister(String vvregister) {
    
    this.vvregister = vvregister;
    return this;
  }

   /**
   * Get vvregister
   * @return vvregister
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getVvregister() {
    return vvregister;
  }



  public void setVvregister(String vvregister) {
    this.vvregister = vvregister;
  }


  public Tblhaendlerdaten schiedsstelle(String schiedsstelle) {
    
    this.schiedsstelle = schiedsstelle;
    return this;
  }

   /**
   * Get schiedsstelle
   * @return schiedsstelle
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getSchiedsstelle() {
    return schiedsstelle;
  }



  public void setSchiedsstelle(String schiedsstelle) {
    this.schiedsstelle = schiedsstelle;
  }


  public Tblhaendlerdaten bank(String bank) {
    
    this.bank = bank;
    return this;
  }

   /**
   * Get bank
   * @return bank
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getBank() {
    return bank;
  }



  public void setBank(String bank) {
    this.bank = bank;
  }


  public Tblhaendlerdaten iban(String iban) {
    
    this.iban = iban;
    return this;
  }

   /**
   * Get iban
   * @return iban
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getIban() {
    return iban;
  }



  public void setIban(String iban) {
    this.iban = iban;
  }


  public Tblhaendlerdaten bic(String bic) {
    
    this.bic = bic;
    return this;
  }

   /**
   * Get bic
   * @return bic
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getBic() {
    return bic;
  }



  public void setBic(String bic) {
    this.bic = bic;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Tblhaendlerdaten tblhaendlerdaten = (Tblhaendlerdaten) o;
    return Objects.equals(this.firma, tblhaendlerdaten.firma) &&
        Objects.equals(this.firmenzusatz, tblhaendlerdaten.firmenzusatz) &&
        Objects.equals(this.strasse, tblhaendlerdaten.strasse) &&
        Objects.equals(this.lhdId, tblhaendlerdaten.lhdId) &&
        Objects.equals(this.plz, tblhaendlerdaten.plz) &&
        Objects.equals(this.ort, tblhaendlerdaten.ort) &&
        Objects.equals(this.ansprechpartner1, tblhaendlerdaten.ansprechpartner1) &&
        Objects.equals(this.ansprechpartner2, tblhaendlerdaten.ansprechpartner2) &&
        Objects.equals(this.telefon1, tblhaendlerdaten.telefon1) &&
        Objects.equals(this.telefon2, tblhaendlerdaten.telefon2) &&
        Objects.equals(this.fax, tblhaendlerdaten.fax) &&
        Objects.equals(this.mobil, tblhaendlerdaten.mobil) &&
        Objects.equals(this.email1, tblhaendlerdaten.email1) &&
        Objects.equals(this.email2, tblhaendlerdaten.email2) &&
        Objects.equals(this.emailtechnic, tblhaendlerdaten.emailtechnic) &&
        Objects.equals(this.emailbilling, tblhaendlerdaten.emailbilling) &&
        Objects.equals(this.homepage, tblhaendlerdaten.homepage) &&
        Objects.equals(this.ustid, tblhaendlerdaten.ustid) &&
        Objects.equals(this.hrb, tblhaendlerdaten.hrb) &&
        Objects.equals(this.gesellschaftsform, tblhaendlerdaten.gesellschaftsform) &&
        Objects.equals(this.geschaeftsfuehrer, tblhaendlerdaten.geschaeftsfuehrer) &&
        Objects.equals(this.registergericht, tblhaendlerdaten.registergericht) &&
        Objects.equals(this.registernummer, tblhaendlerdaten.registernummer) &&
        Objects.equals(this.euidentnummer, tblhaendlerdaten.euidentnummer) &&
        Objects.equals(this.vvregister, tblhaendlerdaten.vvregister) &&
        Objects.equals(this.schiedsstelle, tblhaendlerdaten.schiedsstelle) &&
        Objects.equals(this.bank, tblhaendlerdaten.bank) &&
        Objects.equals(this.iban, tblhaendlerdaten.iban) &&
        Objects.equals(this.bic, tblhaendlerdaten.bic);
  }

  @Override
  public int hashCode() {
    return Objects.hash(firma, firmenzusatz, strasse, lhdId, plz, ort, ansprechpartner1, ansprechpartner2, telefon1, telefon2, fax, mobil, email1, email2, emailtechnic, emailbilling, homepage, ustid, hrb, gesellschaftsform, geschaeftsfuehrer, registergericht, registernummer, euidentnummer, vvregister, schiedsstelle, bank, iban, bic);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Tblhaendlerdaten {\n");
    sb.append("    firma: ").append(toIndentedString(firma)).append("\n");
    sb.append("    firmenzusatz: ").append(toIndentedString(firmenzusatz)).append("\n");
    sb.append("    strasse: ").append(toIndentedString(strasse)).append("\n");
    sb.append("    lhdId: ").append(toIndentedString(lhdId)).append("\n");
    sb.append("    plz: ").append(toIndentedString(plz)).append("\n");
    sb.append("    ort: ").append(toIndentedString(ort)).append("\n");
    sb.append("    ansprechpartner1: ").append(toIndentedString(ansprechpartner1)).append("\n");
    sb.append("    ansprechpartner2: ").append(toIndentedString(ansprechpartner2)).append("\n");
    sb.append("    telefon1: ").append(toIndentedString(telefon1)).append("\n");
    sb.append("    telefon2: ").append(toIndentedString(telefon2)).append("\n");
    sb.append("    fax: ").append(toIndentedString(fax)).append("\n");
    sb.append("    mobil: ").append(toIndentedString(mobil)).append("\n");
    sb.append("    email1: ").append(toIndentedString(email1)).append("\n");
    sb.append("    email2: ").append(toIndentedString(email2)).append("\n");
    sb.append("    emailtechnic: ").append(toIndentedString(emailtechnic)).append("\n");
    sb.append("    emailbilling: ").append(toIndentedString(emailbilling)).append("\n");
    sb.append("    homepage: ").append(toIndentedString(homepage)).append("\n");
    sb.append("    ustid: ").append(toIndentedString(ustid)).append("\n");
    sb.append("    hrb: ").append(toIndentedString(hrb)).append("\n");
    sb.append("    gesellschaftsform: ").append(toIndentedString(gesellschaftsform)).append("\n");
    sb.append("    geschaeftsfuehrer: ").append(toIndentedString(geschaeftsfuehrer)).append("\n");
    sb.append("    registergericht: ").append(toIndentedString(registergericht)).append("\n");
    sb.append("    registernummer: ").append(toIndentedString(registernummer)).append("\n");
    sb.append("    euidentnummer: ").append(toIndentedString(euidentnummer)).append("\n");
    sb.append("    vvregister: ").append(toIndentedString(vvregister)).append("\n");
    sb.append("    schiedsstelle: ").append(toIndentedString(schiedsstelle)).append("\n");
    sb.append("    bank: ").append(toIndentedString(bank)).append("\n");
    sb.append("    iban: ").append(toIndentedString(iban)).append("\n");
    sb.append("    bic: ").append(toIndentedString(bic)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

