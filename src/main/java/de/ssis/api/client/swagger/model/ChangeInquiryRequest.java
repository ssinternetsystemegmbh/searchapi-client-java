/*
 * ELN Search-API DEV
 * <b>Build Time: 2025-03-06T14:15:01.313+0100</b><br><br>Host: null<br>IP: SuSApp/127.0.1.1
 *
 * The version of the OpenAPI document: 1.1.2559
 * Contact: technik@eln.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package de.ssis.api.client.swagger.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * ChangeInquiryRequest
 */

public class ChangeInquiryRequest {
  public static final String SERIALIZED_NAME_INQUIRY_ID = "inquiryId";
  @SerializedName(SERIALIZED_NAME_INQUIRY_ID)
  private Integer inquiryId;

  public static final String SERIALIZED_NAME_PROCESSED = "processed";
  @SerializedName(SERIALIZED_NAME_PROCESSED)
  private Boolean processed;

  public static final String SERIALIZED_NAME_SEEN = "seen";
  @SerializedName(SERIALIZED_NAME_SEEN)
  private Boolean seen;

  public static final String SERIALIZED_NAME_SENT = "sent";
  @SerializedName(SERIALIZED_NAME_SENT)
  private Boolean sent;


  public ChangeInquiryRequest inquiryId(Integer inquiryId) {
    
    this.inquiryId = inquiryId;
    return this;
  }

   /**
   * Get inquiryId
   * @return inquiryId
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public Integer getInquiryId() {
    return inquiryId;
  }



  public void setInquiryId(Integer inquiryId) {
    this.inquiryId = inquiryId;
  }


  public ChangeInquiryRequest processed(Boolean processed) {
    
    this.processed = processed;
    return this;
  }

   /**
   * Get processed
   * @return processed
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public Boolean getProcessed() {
    return processed;
  }



  public void setProcessed(Boolean processed) {
    this.processed = processed;
  }


  public ChangeInquiryRequest seen(Boolean seen) {
    
    this.seen = seen;
    return this;
  }

   /**
   * Get seen
   * @return seen
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public Boolean getSeen() {
    return seen;
  }



  public void setSeen(Boolean seen) {
    this.seen = seen;
  }


  public ChangeInquiryRequest sent(Boolean sent) {
    
    this.sent = sent;
    return this;
  }

   /**
   * Get sent
   * @return sent
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public Boolean getSent() {
    return sent;
  }



  public void setSent(Boolean sent) {
    this.sent = sent;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ChangeInquiryRequest changeInquiryRequest = (ChangeInquiryRequest) o;
    return Objects.equals(this.inquiryId, changeInquiryRequest.inquiryId) &&
        Objects.equals(this.processed, changeInquiryRequest.processed) &&
        Objects.equals(this.seen, changeInquiryRequest.seen) &&
        Objects.equals(this.sent, changeInquiryRequest.sent);
  }

  @Override
  public int hashCode() {
    return Objects.hash(inquiryId, processed, seen, sent);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ChangeInquiryRequest {\n");
    sb.append("    inquiryId: ").append(toIndentedString(inquiryId)).append("\n");
    sb.append("    processed: ").append(toIndentedString(processed)).append("\n");
    sb.append("    seen: ").append(toIndentedString(seen)).append("\n");
    sb.append("    sent: ").append(toIndentedString(sent)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

