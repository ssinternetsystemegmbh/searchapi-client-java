/*
 * ELN Search-API DEV
 * <b>Build Time: 2025-03-06T14:15:01.313+0100</b><br><br>Host: null<br>IP: SuSApp/127.0.1.1
 *
 * The version of the OpenAPI document: 1.1.2559
 * Contact: technik@eln.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package de.ssis.api.client.swagger.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import org.joda.time.DateTime;

/**
 * Outlet Whitelist
 */
@ApiModel(description = "Outlet Whitelist")

public class OutletWhitelistDto {
  public static final String SERIALIZED_NAME_ADDRESS = "address";
  @SerializedName(SERIALIZED_NAME_ADDRESS)
  private String address;

  public static final String SERIALIZED_NAME_DEALER_ID = "dealerId";
  @SerializedName(SERIALIZED_NAME_DEALER_ID)
  private Long dealerId;

  public static final String SERIALIZED_NAME_COMPANY = "company";
  @SerializedName(SERIALIZED_NAME_COMPANY)
  private String company;

  public static final String SERIALIZED_NAME_HOMEPAGE = "homepage";
  @SerializedName(SERIALIZED_NAME_HOMEPAGE)
  private String homepage;

  public static final String SERIALIZED_NAME_ACTIVE = "active";
  @SerializedName(SERIALIZED_NAME_ACTIVE)
  private Boolean active;

  public static final String SERIALIZED_NAME_DATE_MODIFIED = "dateModified";
  @SerializedName(SERIALIZED_NAME_DATE_MODIFIED)
  private DateTime dateModified;

  public static final String SERIALIZED_NAME_DATE_CREATED = "dateCreated";
  @SerializedName(SERIALIZED_NAME_DATE_CREATED)
  private DateTime dateCreated;


  public OutletWhitelistDto address(String address) {
    
    this.address = address;
    return this;
  }

   /**
   * Email-Address
   * @return address
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Email-Address")

  public String getAddress() {
    return address;
  }



  public void setAddress(String address) {
    this.address = address;
  }


  public OutletWhitelistDto dealerId(Long dealerId) {
    
    this.dealerId = dealerId;
    return this;
  }

   /**
   * Dealer-ID
   * @return dealerId
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Dealer-ID")

  public Long getDealerId() {
    return dealerId;
  }



  public void setDealerId(Long dealerId) {
    this.dealerId = dealerId;
  }


  public OutletWhitelistDto company(String company) {
    
    this.company = company;
    return this;
  }

   /**
   * Company
   * @return company
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Company")

  public String getCompany() {
    return company;
  }



  public void setCompany(String company) {
    this.company = company;
  }


  public OutletWhitelistDto homepage(String homepage) {
    
    this.homepage = homepage;
    return this;
  }

   /**
   * Homepage
   * @return homepage
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Homepage")

  public String getHomepage() {
    return homepage;
  }



  public void setHomepage(String homepage) {
    this.homepage = homepage;
  }


  public OutletWhitelistDto active(Boolean active) {
    
    this.active = active;
    return this;
  }

   /**
   * active
   * @return active
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "active")

  public Boolean getActive() {
    return active;
  }



  public void setActive(Boolean active) {
    this.active = active;
  }


  public OutletWhitelistDto dateModified(DateTime dateModified) {
    
    this.dateModified = dateModified;
    return this;
  }

   /**
   * Date of modification.
   * @return dateModified
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Date of modification.")

  public DateTime getDateModified() {
    return dateModified;
  }



  public void setDateModified(DateTime dateModified) {
    this.dateModified = dateModified;
  }


  public OutletWhitelistDto dateCreated(DateTime dateCreated) {
    
    this.dateCreated = dateCreated;
    return this;
  }

   /**
   * Date of creation.
   * @return dateCreated
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Date of creation.")

  public DateTime getDateCreated() {
    return dateCreated;
  }



  public void setDateCreated(DateTime dateCreated) {
    this.dateCreated = dateCreated;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OutletWhitelistDto outletWhitelistDto = (OutletWhitelistDto) o;
    return Objects.equals(this.address, outletWhitelistDto.address) &&
        Objects.equals(this.dealerId, outletWhitelistDto.dealerId) &&
        Objects.equals(this.company, outletWhitelistDto.company) &&
        Objects.equals(this.homepage, outletWhitelistDto.homepage) &&
        Objects.equals(this.active, outletWhitelistDto.active) &&
        Objects.equals(this.dateModified, outletWhitelistDto.dateModified) &&
        Objects.equals(this.dateCreated, outletWhitelistDto.dateCreated);
  }

  @Override
  public int hashCode() {
    return Objects.hash(address, dealerId, company, homepage, active, dateModified, dateCreated);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OutletWhitelistDto {\n");
    sb.append("    address: ").append(toIndentedString(address)).append("\n");
    sb.append("    dealerId: ").append(toIndentedString(dealerId)).append("\n");
    sb.append("    company: ").append(toIndentedString(company)).append("\n");
    sb.append("    homepage: ").append(toIndentedString(homepage)).append("\n");
    sb.append("    active: ").append(toIndentedString(active)).append("\n");
    sb.append("    dateModified: ").append(toIndentedString(dateModified)).append("\n");
    sb.append("    dateCreated: ").append(toIndentedString(dateCreated)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

