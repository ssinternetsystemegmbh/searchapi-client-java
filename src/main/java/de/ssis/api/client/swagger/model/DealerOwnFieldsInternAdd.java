/*
 * ELN Search-API DEV
 * <b>Build Time: 2025-03-06T14:15:01.313+0100</b><br><br>Host: null<br>IP: SuSApp/127.0.1.1
 *
 * The version of the OpenAPI document: 1.1.2559
 * Contact: technik@eln.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package de.ssis.api.client.swagger.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import de.ssis.api.client.swagger.model.DealerOwnFieldsInternAddItem;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * This request is used for ELN internal.
 */
@ApiModel(description = "This request is used for ELN internal.")

public class DealerOwnFieldsInternAdd {
  public static final String SERIALIZED_NAME_DEALER_OWN_FIELDS_INTERN_ADD_ITEM = "dealerOwnFieldsInternAddItem";
  @SerializedName(SERIALIZED_NAME_DEALER_OWN_FIELDS_INTERN_ADD_ITEM)
  private DealerOwnFieldsInternAddItem dealerOwnFieldsInternAddItem;


  public DealerOwnFieldsInternAdd dealerOwnFieldsInternAddItem(DealerOwnFieldsInternAddItem dealerOwnFieldsInternAddItem) {
    
    this.dealerOwnFieldsInternAddItem = dealerOwnFieldsInternAddItem;
    return this;
  }

   /**
   * Get dealerOwnFieldsInternAddItem
   * @return dealerOwnFieldsInternAddItem
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public DealerOwnFieldsInternAddItem getDealerOwnFieldsInternAddItem() {
    return dealerOwnFieldsInternAddItem;
  }



  public void setDealerOwnFieldsInternAddItem(DealerOwnFieldsInternAddItem dealerOwnFieldsInternAddItem) {
    this.dealerOwnFieldsInternAddItem = dealerOwnFieldsInternAddItem;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DealerOwnFieldsInternAdd dealerOwnFieldsInternAdd = (DealerOwnFieldsInternAdd) o;
    return Objects.equals(this.dealerOwnFieldsInternAddItem, dealerOwnFieldsInternAdd.dealerOwnFieldsInternAddItem);
  }

  @Override
  public int hashCode() {
    return Objects.hash(dealerOwnFieldsInternAddItem);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DealerOwnFieldsInternAdd {\n");
    sb.append("    dealerOwnFieldsInternAddItem: ").append(toIndentedString(dealerOwnFieldsInternAddItem)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

