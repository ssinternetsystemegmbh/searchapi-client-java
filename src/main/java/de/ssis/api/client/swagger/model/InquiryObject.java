/*
 * ELN Search-API DEV
 * <b>Build Time: 2025-03-06T14:15:01.313+0100</b><br><br>Host: null<br>IP: SuSApp/127.0.1.1
 *
 * The version of the OpenAPI document: 1.1.2559
 * Contact: technik@eln.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package de.ssis.api.client.swagger.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import de.ssis.api.client.swagger.model.Anfrage;
import de.ssis.api.client.swagger.model.CustomerInquiryData;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * InquiryObject
 */

public class InquiryObject {
  public static final String SERIALIZED_NAME_CUSTOMER = "customer";
  @SerializedName(SERIALIZED_NAME_CUSTOMER)
  private CustomerInquiryData customer;

  public static final String SERIALIZED_NAME_ANFRAGE = "anfrage";
  @SerializedName(SERIALIZED_NAME_ANFRAGE)
  private Anfrage anfrage;


  public InquiryObject customer(CustomerInquiryData customer) {
    
    this.customer = customer;
    return this;
  }

   /**
   * Get customer
   * @return customer
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public CustomerInquiryData getCustomer() {
    return customer;
  }



  public void setCustomer(CustomerInquiryData customer) {
    this.customer = customer;
  }


  public InquiryObject anfrage(Anfrage anfrage) {
    
    this.anfrage = anfrage;
    return this;
  }

   /**
   * Get anfrage
   * @return anfrage
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public Anfrage getAnfrage() {
    return anfrage;
  }



  public void setAnfrage(Anfrage anfrage) {
    this.anfrage = anfrage;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    InquiryObject inquiryObject = (InquiryObject) o;
    return Objects.equals(this.customer, inquiryObject.customer) &&
        Objects.equals(this.anfrage, inquiryObject.anfrage);
  }

  @Override
  public int hashCode() {
    return Objects.hash(customer, anfrage);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class InquiryObject {\n");
    sb.append("    customer: ").append(toIndentedString(customer)).append("\n");
    sb.append("    anfrage: ").append(toIndentedString(anfrage)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

