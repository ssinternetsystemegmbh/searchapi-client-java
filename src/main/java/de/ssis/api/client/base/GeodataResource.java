package de.ssis.api.client.base;

import de.ssis.api.client.swagger.api.GeodataApi;
import de.ssis.api.client.swagger.handler.ApiClient;

/**
 * The GeodataResource extended the Swagger-generated GeodataApi class
 * This class is intended for future client features of the SSiS
 * 
 * @copyright (c) S&S Internet Systeme GmbH
 * @author Martin Soisch <technik@ssis.de>
 * @since 2023-03-06
 * 
 */
public class GeodataResource extends GeodataApi {

	public GeodataResource() {
		super();
	}

	public GeodataResource(ApiClient apiClient) {
		super(apiClient);
	}

}
