package de.ssis.api.client.base;

import de.ssis.api.client.swagger.api.VehiclesApi;
import de.ssis.api.client.swagger.handler.ApiClient;

/**
 * The VehiclesResource extended the Swagger-generated VehiclesApi class
 * This class is intended for future client features of the SSiS
 * 
 * @copyright (c) S&S Internet Systeme GmbH
 * @author Martin Soisch <technik@ssis.de>
 * @since 2018-12-04
 * 
 */
public class VehiclesResource extends VehiclesApi {

	public VehiclesResource() {
		super();
	}

	public VehiclesResource(ApiClient apiClient) {
		super(apiClient);
	}

}
