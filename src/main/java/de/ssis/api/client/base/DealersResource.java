package de.ssis.api.client.base;

import de.ssis.api.client.swagger.api.DealersApi;
import de.ssis.api.client.swagger.handler.ApiClient;

/**
 * The DealersResource extended the Swagger-generated DealersApi class
 * This class is intended for future client features of the SSiS
 * 
 * @copyright (c) S&S Internet Systeme GmbH
 * @author Martin Soisch <technik@ssis.de>
 * @since 2018-12-03
 * 
 */
public class DealersResource extends DealersApi {

	public DealersResource() {
		super();
	}

	public DealersResource(ApiClient apiClient) {
		super(apiClient);
	}

}
