package de.ssis.api.client.base;

import de.ssis.api.client.swagger.api.StaticValuesApi;
import de.ssis.api.client.swagger.handler.ApiClient;

/**
 * The StaticValuesResource extended the Swagger-generated StaticValuesApi class
 * This class is intended for future client features of the SSiS
 * 
 * @copyright (c) S&S Internet Systeme GmbH
 * @author Martin Soisch <technik@ssis.de>
 * @since 2023-02-17
 * 
 */
public class StaticValuesResource extends StaticValuesApi {

	public StaticValuesResource() {
		super();
	}

	public StaticValuesResource(ApiClient apiClient) {
		super(apiClient);
	}

}
