package de.ssis.api.client;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import de.ssis.api.client.base.DealersResource;
import de.ssis.api.client.base.VehiclesResource;
import de.ssis.api.client.swagger.handler.ApiClient;
import de.ssis.api.client.swagger.handler.ApiException;
import de.ssis.api.client.swagger.model.DealerDetailsRequest;
import de.ssis.api.client.swagger.model.DealerResponse;
import de.ssis.api.client.swagger.model.DealerResponseWithMeta;
import de.ssis.api.client.swagger.model.MetaResponse;
import de.ssis.api.client.swagger.model.PaginationParameters;
import de.ssis.api.client.swagger.model.TagSearch;
import de.ssis.api.client.swagger.model.TagSearchResult;
import de.ssis.api.client.swagger.model.TagSearchResultWithMeta;
import de.ssis.api.client.swagger.model.VehicleDetailsRequest;
import de.ssis.api.client.swagger.model.VehicleResponse;
import de.ssis.api.client.swagger.model.VehicleResponseWithMeta;
import de.ssis.api.client.swagger.model.VehicleSearch;
import de.ssis.api.client.swagger.model.VehicleSearchResult;
import de.ssis.api.client.swagger.model.VehicleSearchResultWithMeta;

/**
 * The Client class wrapped the Swagger-generated api classes
 * This class is intended to use the api client easier 
 * and for future client features of the SSiS
 * 
 * @copyright (c) S&S Internet Systeme GmbH
 * @author Martin Soisch <technik@ssis.de>
 * @since 2018-12-03
 *
 */
public class Client {
	public static final String AGG_TYPE_MANUFACTURER = "MANUFACTURER";
	public static final String AGG_TYPE_SERIES = "SERIES";
	public static final String AGG_TYPE_ORDERSTATUS = "ORDERSTATUS";
	public static final String AGG_TYPE_BODYTYPE = "BODYTYPE";
	public static final String AGG_TYPE_BODYTYPEGROUP = "BODYTYPEGROUP";
	public static final String AGG_TYPE_FUELTYPEPRIMARY = "FUELTYPEPRIMARY";
	public static final String AGG_TYPE_DEALERID_VEHICLEOWNER = "DEALERID_VEHICLEOWNER";
	public static final String AGG_TYPE_COLORTAG = "COLORTAG";
	public static final String AGG_TYPE_UPHOLSTERYTYPETAG = "UPHOLSTERYTYPETAG";
	public static final String AGG_TYPE_PROPULSIONTYPETAG = "PROPULSIONTYPETAG";
	public static final String AGG_TYPE_POLLUTIONBADGETAG = "POLLUTIONBADGETAG";
	public static final String AGG_TYPE_EURONORMTAG = "EURONORMTAG";
	public static final String AGG_TYPE_TRANSMISSIONTYPE = "TRANSMISSIONTYPE";
	
	protected String clientVersion = "1.1.348";
	
	protected Properties props;
	
	protected int dealerId;
	
	protected ApiClient apiClient;
	protected DealersResource dealersResource;
	protected VehiclesResource vehiclesResource;
	
	protected Map<String, String> pagParams;
	
	protected PaginationParameters paginationParameters;
	
	protected MetaResponse lastMetadata;
	
	public Client() {
		this.props = this.getProperties();
		this.init();
	}
	
	public Client(String username, String password, Integer dealerId) {
		this.props = new Properties();
		this.props.setProperty("host", "https://api.ssis.de:8443/api");
		this.props.setProperty("temp_folder_path", "/var/tmp/elnapiclient/");
		this.props.setProperty("username", username);
		this.props.setProperty("password", password);
		this.props.setProperty("dealerid", String.valueOf(dealerId));
		this.init();
	}
	
	final private void init() {
		this.pagParams = new HashMap<String, String>();
		this.pagParams.put("result_count", "10");
		this.pagParams.put("result_offset", "0");
		this.pagParams.put("sort_type", "id");
		this.pagParams.put("sort_order_asc", "true");
		
		this.paginationParameters = new PaginationParameters();
		this.paginationParameters.setResultCount(Integer.parseInt(this.pagParams.get("result_count").trim()));
		this.paginationParameters.setResultOffset(Integer.parseInt(this.pagParams.get("result_offset").trim()));
		this.paginationParameters.setSortType(this.pagParams.get("sort_type"));
		this.paginationParameters.setSortOrderASC(Boolean.parseBoolean(this.pagParams.get("sort_order_asc").trim()));
		
		this.dealerId = Integer.parseInt(this.props.getProperty("dealerid").trim());
		
		this.apiClient = new ApiClient();
		this.apiClient.setBasePath(this.props.getProperty("host"));
		this.apiClient.setUsername(this.props.getProperty("username"));
		this.apiClient.setPassword(this.props.getProperty("password"));
		this.apiClient.setTempFolderPath(this.props.getProperty("temp_folder_path"));
		this.apiClient.setVerifyingSsl(false);
		this.dealersResource = new DealersResource(this.apiClient);
		this.vehiclesResource = new VehiclesResource(this.apiClient);
	}
	
	/**
	 * Returns the API Client Version
	 * 
	 * @return String
	 */
	public String getClientVersion() {
		return this.clientVersion;
	}
	
	/**
	 * Returns the API Url
	 * 
	 * @return String
	 */
	public String getHost() {
		return this.apiClient.getBasePath();
	}
	
	/**
	 * Sets the API Url
	 * 
	 * @param host
	 * @return Client
	 */
	public Client setHost(String host) {
		this.apiClient.setBasePath(host);
		
		return this;
	}
	
	/**
	 * Returns the Dealer ID
	 * 
	 * @return int
	 */
	public int getDealerId() {
		return this.dealerId;
	}
	
	/**
	 * Sets the Dealer ID
	 * 
	 * @param dealerId
	 * @return Client
	 */
	public Client setDealerId(int dealerId) {
		this.dealerId = dealerId;
		
		return this;
	}
	
	/**
	 * Returns the Pagination Parameters
	 * 
	 * @return Map<String, String>
	 */
	public Map<String, String> getPagParams() {
		return this.pagParams;
	}
	
	/**
	 * Set the Pagination Parameters Object
	 * 
	 * @param pagParams
	 * @return Client
	 */
	public Client setPaginationParameters(Map<String, String> pagParams) {
		this.pagParams = pagParams;
		
		this.paginationParameters.setResultCount(Integer.parseInt(this.pagParams.get("result_count")));
		this.paginationParameters.setResultOffset(Integer.parseInt(this.pagParams.get("result_offset")));
		this.paginationParameters.setSortType(this.pagParams.get("sort_type"));
		this.paginationParameters.setSortOrderASC(Boolean.parseBoolean(this.pagParams.get("sort_order_asc")));
		
		return this;
	}
	
	/**
	 * Returns the Pagination Parameters Object
	 * 
	 * @return PaginationParameters
	 */
	public PaginationParameters getPaginationParameters() {
		return this.paginationParameters;
	}
	
	/**
	 * Set the number of return values
	 * 
	 * @param cnt
	 * @return Client
	 */
	public Client setPagParamResultCount(int cnt) {
		this.paginationParameters.setResultCount(cnt);
		
		return this;
	}
	
	/**
	 * Set the offset of the return values
	 * 
	 * @param cnt
	 * @return Client
	 */
	public Client setPagParamResultOffset(int cnt) {
		this.paginationParameters.setResultOffset(cnt);
		
		return this;
	}
	
	/**
	 * Set the fieldname for sort (default: id)
	 * 
	 * @param sortType
	 * @return
	 */
	public Client setPagParamSortType(String sortType) {
		this.paginationParameters.setSortType(sortType);
		
		return this;
	}
	
	/**
	 * Set the sort direction (default: ascending)
	 * 
	 * @param sortOrderAsc
	 * @return Client
	 */
	public Client setPagParamSortOrderAsc(boolean sortOrderAsc) {
		this.paginationParameters.setSortOrderASC(sortOrderAsc);
		
		return this;
	}
	
	/**
	 * Returns a DealerDetailsRequest Object
	 * used for getDealerDetails()
	 * 
	 * @return DealerDetailsRequest
	 */
	public DealerDetailsRequest getDealerDetailsRequest() {
		DealerDetailsRequest dealerDetailsRequest = new DealerDetailsRequest();
		
		return dealerDetailsRequest;
	}
	
	/**
	 * Returns a TagSearch Object
	 * used for getVehicleAggregationTags()
	 * 
	 * @return TagSearch
	 */
	public TagSearch getVehicleTagSearch() {
		TagSearch tagSearch = new TagSearch();
		
		return tagSearch;
	}
	
	/**
	 * Returns a VehicleSearch Object
	 * used for getVehicleSearchResult()
	 * 
	 * @return VehicleSearch
	 */
	public VehicleSearch getVehicleSearch() {
		VehicleSearch vehicleSearch = new VehicleSearch();
		
		return vehicleSearch;
	}
	
	/**
	 * Returns a VehicleDetailsRequest Object
	 * used for getVehicleDetails()
	 * 
	 * @return VehicleDetailsRequest
	 */
	public VehicleDetailsRequest getVehicleDetailsRequest() {
		VehicleDetailsRequest vehicleDetailsRequest = new VehicleDetailsRequest();
		
		return vehicleDetailsRequest;
	}
	
	
	/**
	 * Returns a DealerResponse Object with data from API
	 * 
	 * @param dealerDetailsRequest
	 * 
	 * @return DealerResponse
	 * @throws ApiException
	 */
	public DealerResponse getDealerDetails(DealerDetailsRequest dealerDetailsRequest) throws ApiException {
		DealerResponseWithMeta dealerResponseWithMeta = this.dealersResource.getDealerDetails(dealerDetailsRequest);
		
		this.lastMetadata = dealerResponseWithMeta.getMeta();
		
		return dealerResponseWithMeta.getResponse();
	}
	
	/**
	 * Returns a TagSearchResult Object with data from API
	 * 
	 * @param tagSearch
	 * 
	 * @return TagSearchResult
	 * @throws ApiException
	 */
	public TagSearchResult getVehicleAggregationTags(TagSearch tagSearch) throws ApiException {
		return this.getVehicleAggregationTags(tagSearch, this.dealerId);
	}
	
	/**
	 * Returns a TagSearchResult Object with data from API
	 * 
	 * @param tagSearch
	 * @param dealerId
	 * 
	 * @return TagSearchResult
	 * @throws ApiException
	 */
	public TagSearchResult getVehicleAggregationTags(TagSearch tagSearch, int dealerId) throws ApiException {
		TagSearchResultWithMeta tagSearchResultWithMeta = this.vehiclesResource.getVehicleAggregationTags(dealerId, tagSearch);
		
		this.lastMetadata = tagSearchResultWithMeta.getMeta();
		
		TagSearchResult tagSearchResult = tagSearchResultWithMeta.getResponse();
		
		return tagSearchResult;
	}
	
	
	/**
	 * Returns a VehicleSearchResult Object with data from API
	 * 
	 * @param vehicleSearch
	 * 
	 * @return VehicleSearchResult
	 * @throws ApiException
	 */
	public VehicleSearchResult getVehicleSearchResult(VehicleSearch vehicleSearch) throws ApiException {
		return this.getVehicleSearchResult(vehicleSearch, this.dealerId);
	}
	
	/**
	 * Returns a VehicleSearchResult Object with data from API
	 * 
	 * @param vehicleSearch
	 * @param dealerId
	 * 
	 * @return VehicleSearchResult
	 * @throws ApiException
	 */
	public VehicleSearchResult getVehicleSearchResult(VehicleSearch vehicleSearch, int dealerId) throws ApiException {
		if (vehicleSearch.getPaginationParameters() == null) {
			vehicleSearch.setPaginationParameters(this.paginationParameters);
		}
		
		VehicleSearchResultWithMeta vehicleSearchResultWithMeta = this.vehiclesResource.searchDealerVehicles(dealerId, vehicleSearch);
		
		this.lastMetadata = vehicleSearchResultWithMeta.getMeta();
		
		return vehicleSearchResultWithMeta.getResponse();
	}
	
	/**
	 * Returns a VehicleResponse Object with data from API
	 * 
	 * @param vehicleDetailsRequest
	 * 
	 * @return VehicleResponse
	 * @throws ApiException
	 */
	public VehicleResponse getVehicleDetails(VehicleDetailsRequest vehicleDetailsRequest) throws ApiException {
		return this.getVehicleDetails(vehicleDetailsRequest, this.dealerId);
	}
	
	/**
	 * Returns a VehicleResponse Object with data from API
	 * 
	 * @param vehicleDetailsRequest
	 * @param dealerId
	 * 
	 * @return VehicleResponse
	 * @throws ApiException
	 */
	public VehicleResponse getVehicleDetails(VehicleDetailsRequest vehicleDetailsRequest, int dealerId) throws ApiException {
		VehicleResponseWithMeta vehicleResponseWithMeta = this.vehiclesResource.getVehicleDetails(dealerId, vehicleDetailsRequest);
		
		this.lastMetadata = vehicleResponseWithMeta.getMeta();
		
		return vehicleResponseWithMeta.getResponse();
	}
	
	/**
	 * Returns a MetaResponse Object from last request
	 *
	 * @return MetaResponse
	 * @throws ApiException
	 */
	public MetaResponse getLastMetadata() throws ApiException {
		MetaResponse metaResponse = this.lastMetadata;
		
		if (metaResponse == null) {
			throw new ApiException("To get the last metadata, you must run a search first.", 999, null, null);
		}
		
		return metaResponse;
	}
	
	/**
	 * 
	 * @return Properties
	 */
	private Properties getProperties() {
		InputStream inputStream = null;
		Properties prop = new Properties();
		try {
			
			String propFileName = "config.properties";
 
			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
			
			if (inputStream != null) {
				
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}
			
			inputStream.close();
		} catch (Exception e) {
			System.out.println("Exception: " + e);
		}
		
		return prop;
	}
}
